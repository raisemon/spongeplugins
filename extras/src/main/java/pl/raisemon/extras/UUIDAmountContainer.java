package pl.raisemon.extras;
import lombok.Data;

import java.util.UUID;

@Data
public class UUIDAmountContainer {
    private UUID uuid;
    private int amount;

    public UUIDAmountContainer(UUID uuid, int n) {
        this.uuid = uuid;
        this.amount = n;
    }
}
