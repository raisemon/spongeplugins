package pl.raisemon.extras.chatteams;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class ChatTeam {
    private final UUID owner;
    @Getter
    private Set<UUID> users = new HashSet<>();

    protected ChatTeam(UUID owner) {
        this.owner = owner;
    }

    public boolean hasMember(UUID uuid) {
        return users.contains(uuid) || uuid.equals(owner);
    }

    protected void addMember(UUID uuid) {
        users.add(uuid);
    }

    public boolean isOwner(UUID uuid) {
        return owner == uuid;
    }

    protected void removeMember(UUID user) {
        users.remove(user);
    }
}
