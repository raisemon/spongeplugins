package pl.raisemon.extras.chatteams;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import pl.raisemon.extras.ExtrasPlugin;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public class ChatTeamManager {
    private Set<ChatTeam> teams = new HashSet<>();

    public Optional<ChatTeam> createTeam(Player player){
        return createTeam(player.getUniqueId());
    }

    private Optional<ChatTeam> createTeam(UUID uuid) {
        if(getTeam(uuid).isPresent()){
            return Optional.empty();
        }
        ChatTeam team = new ChatTeam(uuid);
        Sponge.getServer().getPlayer(uuid).ifPresent(ExtrasPlugin.getInstance()::updateChatTabs);
        teams.add(team);
        return Optional.of(team);
    }

    public void addToTeam(ChatTeam team, Player player){
        addToTeam(team, player.getUniqueId());
        ExtrasPlugin.getInstance().updateChatTabs(player);
    }

    public void addToTeam(ChatTeam team, UUID uuid){
        if(getTeam(uuid).isPresent()){
            return;
        }
        team.addMember(uuid);
    }

    public void addToTeam(Player player, Player invited) {
        final ChatTeam team = getOwnedTeam(player).orElseGet(() -> createTeam(player).orElse(null));
        team.addMember(invited.getUniqueId());
        ExtrasPlugin.getInstance().updateChatTabs(invited);
    }

    public Optional<ChatTeam> getOwnedTeam(Player player){
        return getTeam(player.getUniqueId());
    }

    public Optional<ChatTeam> getOwnedTeam(UUID uuid){
        return teams.stream().filter(t -> t.isOwner(uuid)).findFirst();
    }

    public Optional<ChatTeam> getTeam(Player player){
        return getTeam(player.getUniqueId());
    }

    public Optional<ChatTeam> getTeam(UUID uuid){
        return teams.stream().filter(t -> t.hasMember(uuid)).findFirst();
    }

    public void load() {
    }

    public void removeMember(ChatTeam team, UUID user) {
        team.removeMember(user);
        Sponge.getServer().getPlayer(user).ifPresent(ExtrasPlugin.getInstance()::updateChatTabs);
    }
}
