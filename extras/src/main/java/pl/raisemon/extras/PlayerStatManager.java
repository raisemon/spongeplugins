package pl.raisemon.extras;

import lombok.Getter;
import org.spongepowered.api.Sponge;
import pl.raisemon.api.utils.DatabaseUtil;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

public class PlayerStatManager {
    @Getter
    private List<UUIDAmountContainer> top = new CopyOnWriteArrayList<>();
    private PlayerStatType type;

    public PlayerStatManager(PlayerStatType type) {
        this.type = type;
        DatabaseUtil.execute("CREATE TABLE IF NOT EXISTS `" + type.getTable_name() + "` (" +
                "`id` INT NOT NULL AUTO_INCREMENT," +
                "`uuid` varchar(32) NOT NULL UNIQUE," +
                "`n` INT NOT NULL," +
                "PRIMARY KEY (`id`)" +
                ");");

        Sponge.getScheduler().createTaskBuilder().async().interval(1, TimeUnit.MINUTES).delay(2, TimeUnit.MINUTES).execute(() -> {
            top.sort((o1, o2) -> o2.getAmount() - o1.getAmount());
        }).submit(ExtrasPlugin.getInstance());
    }

    public void load() {
        DatabaseUtil.query("SELECT `uuid`,`n` FROM `" + type.getTable_name() + "`;", results -> {
            try {
                while (results.next()) {
                    top.add(new UUIDAmountContainer(UUID.fromString(results.getString("uuid").replaceAll(
                            "(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})",
                            "$1-$2-$3-$4-$5")), results.getInt("n")));
                }
                top.sort((o1, o2) -> o2.getAmount() - o1.getAmount());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void increment(UUID uuid1) {
        DatabaseUtil.execute("INSERT INTO " + type.getTable_name() + " (`uuid`, `n`) " +
                "VALUES ('" + uuid1.toString().replace("-", "") + "', 1) ON DUPLICATE KEY UPDATE `n`=`n`+1;");
        for (UUIDAmountContainer entry : top) {
            if (entry.getUuid().equals(uuid1)) {
                entry.setAmount(entry.getAmount() + 1);
                return;
            }
        }
        top.add(new UUIDAmountContainer(uuid1, 1));
    }

    public void clear() {
        DatabaseUtil.execute("DELETE FROM `" + type.getTable_name() + "` WHERE 1=1;");
        this.top.clear();
    }

    public int getPosition(UUID uniqueId) {
        for (int i = 0; i < top.size(); i++) {
            if (top.get(i).getUuid().equals(uniqueId)) {
                return i;
            }
        }
        return -1;
    }
}
