package pl.raisemon.extras;

import com.flowpowered.math.vector.Vector3d;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.Configurable;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ExpPlacesConfig implements Configurable {
    @Getter
    private Map<String, LocRot> expplaces;
    private CommentedConfigurationNode node;

    @Override
    public String getConfigName() {
        return "ExpPlaces";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {

    }

    @Override
    public void load(CommentedConfigurationNode node) {
        this.node = node;
        expplaces = new HashMap<>();
        for (Map.Entry<Object, ? extends CommentedConfigurationNode> e : node.getNode("expplaces").getChildrenMap().entrySet()) {
            Optional<World> world = Sponge.getServer().getWorld(e.getValue().getNode("world").getString());
            if(!world.isPresent()){
                System.out.println("WORLD NOT FOUND");
                return;
            }
            expplaces.put((String) e.getKey(),
                    new LocRot(new Location<World>(world.get(),
                            e.getValue().getNode("x").getDouble(),
                            e.getValue().getNode("y").getDouble(),
                            e.getValue().getNode("z").getDouble()),
                            new Vector3d(
                                    e.getValue().getNode("pitch").getDouble(),
                                    e.getValue().getNode("yaw").getDouble(),
                                    e.getValue().getNode("roll").getDouble())
                            )
                    );
        }
    }

    public void set(String name, Location<World> location, Vector3d headRotation){
        node.getNode("expplaces", name, "world").setValue(location.getExtent().getName());
        node.getNode("expplaces", name, "x").setValue(location.getX());
        node.getNode("expplaces", name, "y").setValue(location.getY());
        node.getNode("expplaces", name, "z").setValue(location.getZ());
        node.getNode("expplaces", name, "pitch").setValue(headRotation.getX());
        node.getNode("expplaces", name, "yaw").setValue(headRotation.getY());
        node.getNode("expplaces", name, "roll").setValue(headRotation.getZ());
        expplaces.put(name, new LocRot(location, headRotation));
        ApiPlugin.getInstance().saveConfig(this);
    }

    public void del(String name){
        expplaces.remove(name);
        node.getNode("expplaces", name).setValue(null);
        ApiPlugin.getInstance().saveConfig(this);
    }

    @Data
    @AllArgsConstructor
    public class LocRot {
        private Location<World> loc;
        private Vector3d rot;
    }
}
