package pl.raisemon.extras;

import com.google.common.reflect.TypeToken;
import com.pixelmonmod.pixelmon.config.PixelmonItems;
import lombok.Getter;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import pl.raisemon.api.Configurable;
import pl.raisemon.extras.data.ShapedMultiRecipe;
import pl.raisemon.mod.RegistrationHandler;

import java.util.*;

public class CraftingConfig implements Configurable {
    @Getter
    private List<ShapedMultiRecipe> craftings;

    @Override
    public String getConfigName() {
        return "Crafting";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {
        Map<String, ItemType> items = new HashMap<>();
        items.put("S", ItemTypes.PRISMARINE_SHARD);
        items.put("T", (ItemType) PixelmonItems.thunderStoneShard);
        items.put("W", (ItemType) PixelmonItems.waterStoneShard);
        items.put("R", (ItemType) PixelmonItems.ruby);
        try {
            node.getNode("craftings").setValue(new TypeToken<List<ShapedMultiRecipe>>() {},
                    Collections.singletonList(new ShapedMultiRecipe("DEPTH_STRIDER_STONE_CRAFTING", "STS", "WRW", "STS", items, (ItemType) RegistrationHandler.DEPTH_STRIDER_STONE, Arrays.asList(4, 1, 4, 1, 1, 1, 4, 1, 4))));
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(CommentedConfigurationNode node) {
        try {
            this.craftings = node.getNode("craftings").getList(new TypeToken<ShapedMultiRecipe>() {});
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }
}
