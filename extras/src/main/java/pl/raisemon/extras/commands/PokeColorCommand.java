package pl.raisemon.extras.commands;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import net.minecraft.entity.player.EntityPlayerMP;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;

import java.util.*;

public class PokeColorCommand extends BasePlayerCommand {

    private static Map<String, String> colors = new HashMap<>();
    static{
        colors.put("czarny", "§0");
        colors.put("ciemnoniebieski", "§1");
        colors.put("ciemnozielony", "§2");
        colors.put("ciemnoturkusowy", "§3");
        colors.put("ciemnoczerwony", "§4");
        colors.put("ciemnofioletowy", "§5");
        colors.put("zloty", "§6");
        colors.put("szary", "§7");
        colors.put("ciemnoszary", "§8");
        colors.put("niebieski", "§9");
        colors.put("zielony", "§a");
        colors.put("turkusowy", "§b");
        colors.put("czerwony", "§c");
        colors.put("fioletowy", "§d");
        colors.put("zolty", "§e");
        colors.put("bialy", "§f");
        colors.put("tecza", "§t");
    }

    public PokeColorCommand(Object plugin) {
        super(plugin, "PokeKolor");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        int slot = args.<Integer>getOne("slot").get()-1;
        if(slot < 0 || slot > 5) {
            MessageUtils.sendMessage(player, "PokeKolor", "extras.commands.pokeColorBadSlot");
            return CommandResult.success();
        }
        String color = args.<String>getOne("kolor").get();
        if(color.equals("§t")){
            if(!player.hasPermission("pl.raisemon.pokecolorrainbow")){
                throw new CommandException(Text.of(TextColors.RED, "Nie posiadasz permisji do tego koloru"));
            }
        }
        final Pokemon pokemon = Pixelmon.storageManager.getParty((EntityPlayerMP) player).get(slot);
        if(pokemon == null) {
            MessageUtils.sendMessage(player, "PokeKolor", "extras.commands.pokeColorBadSlot");
            return CommandResult.success();
        }
        String nickname = pokemon.getDisplayName();
        while(nickname.charAt(0) == '§'){
            nickname = nickname.substring(2);
        }
        pokemon.setNickname(color + nickname);
        MessageUtils.sendMessage(player, "PokeKolor", "extras.commands.pokeColorSuccess");
        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("pokekolor");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.pokecolor";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.integer(Text.of("slot")),
                GenericArguments.choices(Text.of("kolor"), colors)
        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }
}
