package pl.raisemon.extras.commands;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.extras.ExtrasPlugin;

import java.util.*;

public class RemoveEvsCommand extends BasePlayerCommand {

    private static final Map<String, String> pixelEvs = new HashMap<>();

    static {
        pixelEvs.put("hp", "evHP");
        pixelEvs.put("atk", "evAttack");
        pixelEvs.put("def", "evDefence");
        pixelEvs.put("spatk", "evSpecialAttack");
        pixelEvs.put("spdef", "evSpecialDefence");
        pixelEvs.put("spd", "evSpeed");
    }

    public RemoveEvsCommand(ExtrasPlugin extrasPlugin) {
        super(extrasPlugin, "UsunEVs");
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("usunevs");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.commands.removeevs";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.optional(GenericArguments.integer(Text.of("slot"))),
                GenericArguments.optional(GenericArguments.choices(Text.of("evs"), () -> {
                    List<String> l = new ArrayList<>(pixelEvs.keySet());
                    l.add("all");
                    return l;
                }, s->s))
        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }
    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {

        if(!args.hasAny("evs") ||
                !args.hasAny("slot") ||
                (!pixelEvs.containsKey(args.<String>getOne("evs").get()) &&
                        !args.<String>getOne("evs").get().equalsIgnoreCase("all"))){
            MessageUtils.sendMessage(player, "UsunEVs", "commands.removeEvsUsage");
            return CommandResult.empty();
        }
        String evs = args.<String>getOne("evs").get();
        int slot = args.<Integer>getOne("slot").get();
        if(slot<1 || slot>6){
            MessageUtils.sendMessage(player, "UsunEVs", "commands.  pokemonSlotOutOfRange=\"&cMusisz podac slot! (1-6)\"\n" +
                    "  removeEvsAll=\"&7Usunieto &3WSZYSTKIE &7ev's!\"\n" +
                    "  removeEvsSingle=\"&7Usunieto ev's &3%s &7z wybranego pokemona!\"");
            return CommandResult.empty();
        }
        if(evs.equalsIgnoreCase("all")){
            for (Map.Entry<String, String> stringStringEntry : pixelEvs.entrySet()) {
                Sponge.getCommandManager().process(Sponge.getServer().getConsole(), "pokeedit "+player.getName()+" "+ slot +" "+stringStringEntry.getValue()+":0");
            }
            MessageUtils.sendMessage(player, "UsunEVs", "commands.removeEvsAll");
        }else{
            Sponge.getCommandManager().process(Sponge.getServer().getConsole(), "pokeedit "+player.getName()+" "+ slot +" "+pixelEvs.get(evs)+":0");
            MessageUtils.sendMessage(player, "UsunEVs", "commands.removeEvsSingle",evs.toUpperCase());
        }
        return CommandResult.success();
    }
}
