package pl.raisemon.extras.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.extras.data.MessagesCommand;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class MessageCommandImpl extends BaseCommand {
    private ExtrasPlugin extrasPlugin;
    private MessagesCommand messagesCommand;

    public MessageCommandImpl(ExtrasPlugin extrasPlugin, MessagesCommand messagesCommand) {
        super(extrasPlugin);
        this.extrasPlugin = extrasPlugin;
        this.messagesCommand = messagesCommand;
    }

    @Override
    public List<String> getAliases() {
        return messagesCommand.getAliases();
    }

    @Override
    public String getPermission() {
        return messagesCommand.getPermission();
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        if(messagesCommand.getChildren() == null){
            return null;
        }
        return messagesCommand.getChildren().stream().map(m -> new MessageCommandImpl(extrasPlugin, m)).collect(Collectors.toSet());
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        for (String messageLine : messagesCommand.getMessageLines()) {
            src.sendMessage(TextSerializers.FORMATTING_CODE.deserialize(messageLine));
        }
        return CommandResult.success();
    }
}
