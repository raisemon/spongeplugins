package pl.raisemon.extras.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.extras.ExpPlacesConfig;
import pl.raisemon.extras.ExtrasPlugin;

import java.util.*;
import java.util.function.Function;

public class ExpPlaceCommand extends BasePlayerCommand{
    public ExpPlaceCommand(ExtrasPlugin extrasPlugin) {
        super(ExtrasPlugin.getInstance(), "EXP");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        Optional<String> name = args.getOne("name");
        if(!player.hasPermission("pl.raisemon.expplace." + name.get())){
            MessageUtils.sendMessage(player, "EXP", "expplaces.nopermission");
            return CommandResult.success();
        }
        ExpPlacesConfig.LocRot locRot = ExtrasPlugin.getInstance().getExpPlaceConfig().getExpplaces().get(name.get());
        player.setLocationAndRotation(locRot.getLoc(), locRot.getRot());
        MessageUtils.sendMessage(player, "EXP", "expplaces.teleported");
        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("expowisko");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.choices(Text.of("name"), () -> ExtrasPlugin.getInstance().getExpPlaceConfig().getExpplaces().keySet(), (Function<String, String>) s -> s)
        };
    }

    @Override
    public Collection<BaseCommand> getChildren() {
        return Arrays.asList(new Create(), new Delete());
    }

    private class Create extends BasePlayerCommand {
        public Create() {
            super(ExtrasPlugin.getInstance(), "EXP");
        }

        @Override
        public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
            Optional<String> name = args.getOne("name");
            String s = name.get();
            ExtrasPlugin.getInstance().getExpPlaceConfig().set(s, player.getLocation(), player.getHeadRotation());
            player.sendMessage(Text.of(TextColors.GREEN, "Dodano"));
            return CommandResult.success();
        }

        @Override
        public List<String> getAliases() {
            return Collections.singletonList("create");
        }

        @Override
        public String getPermission() {
            return "pl.raisemon.admin";
        }

        @Override
        public CommandElement[] getArguments() {
            return new CommandElement[]{
                    GenericArguments.string(Text.of("name"))
            };
        }

        @Override
        public Collection<BaseCommand> getChildren() {
            return null;
        }
    }
    private class Delete extends BasePlayerCommand {
        public Delete() {
            super(ExtrasPlugin.getInstance(), "EXP");
        }

        @Override
        public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
            Optional<String> name = args.getOne("name");
            String s = name.get();
            ExtrasPlugin.getInstance().getExpPlaceConfig().del(s);
            player.sendMessage(Text.of(TextColors.RED, "Skasowano"));
            return CommandResult.success();
        }

        @Override
        public List<String> getAliases() {
            return Collections.singletonList("delete");
        }

        @Override
        public String getPermission() {
            return "pl.raisemon.admin";
        }

        @Override
        public CommandElement[] getArguments() {
            return new CommandElement[]{
                    GenericArguments.choices(Text.of("name"), () -> ExtrasPlugin.getInstance().getExpPlaceConfig().getExpplaces().keySet(), (Function<String, String>) s -> s)
            };
        }

        @Override
        public Collection<BaseCommand> getChildren() {
            return null;
        }
    }
}
