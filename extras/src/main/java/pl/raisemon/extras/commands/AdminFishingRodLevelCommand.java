package pl.raisemon.extras.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.extras.ExtrasPlugin;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class AdminFishingRodLevelCommand extends BasePlayerCommand {
    public AdminFishingRodLevelCommand(ExtrasPlugin extrasPlugin) {
        super(extrasPlugin, "AD");
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("adminfishinglevel");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.admin.fishingrod";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.integer(Text.of("points"))
        };
    }

    @Override
    public Collection<BaseCommand> getChildren() {
        return null;
    }


    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        int points = args.<Integer>getOne("points").get();
        Optional<ItemStack> itemInHand = player.getItemInHand(HandTypes.MAIN_HAND);
        if(!itemInHand.isPresent() || !itemInHand.get().getType().getId().startsWith("raisemonmod:fishingrod_")){
            player.sendMessage(Text.of(TextColors.RED, "Musisz trzymac wedke w reku"));
            return CommandResult.success();
        }
        DataContainer dataContainer = itemInHand.get().toContainer();
        dataContainer.set(DataQuery.of("UnsafeData", "RFishing", "points"), points);
        player.setItemInHand(HandTypes.MAIN_HAND, ItemStack.builder().fromContainer(dataContainer).build());
        return CommandResult.success();
    }
}
