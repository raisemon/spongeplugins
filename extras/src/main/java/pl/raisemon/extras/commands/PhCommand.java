package pl.raisemon.extras.commands;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.entity.living.player.Player;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.extras.ExtrasPlugin;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class PhCommand extends BasePlayerCommand {
    public PhCommand(ExtrasPlugin extrasPlugin) {
        super(extrasPlugin, "PH");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        Sponge.getCommandManager().process(Sponge.getServer().getConsole(), "pokeheal " + player.getName());
        MessageUtils.sendMessage(player, "PH", "extras.commands.pokeHeal");
        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("ph");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.commands.ph";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Collection<BaseCommand> getChildren() {
        return null;
    }
}
