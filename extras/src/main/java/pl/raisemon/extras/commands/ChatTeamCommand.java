package pl.raisemon.extras.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.extras.chatteams.ChatTeam;

import java.util.*;

public class ChatTeamCommand extends BasePlayerCommand {
    private ExtrasPlugin extrasPlugin;

    public ChatTeamCommand(ExtrasPlugin extrasPlugin) {
        super(extrasPlugin, "CHAT");
        this.extrasPlugin = extrasPlugin;
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {

        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("team");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.chatteams";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public List<BaseCommand> getChildren() {
        return Arrays.asList(
                new AddMember(extrasPlugin),
                new ListMembers(extrasPlugin),
                new RemoveMember(extrasPlugin)
        );
    }

    private class AddMember extends BasePlayerCommand{
        public AddMember(Object plugin) {
            super(plugin, "CHAT");
        }

        @Override
        public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
            final Player invited = args.<Player>getOne("player").get();
            invited.sendMessage(Text.of(
                    TextSerializers.FORMATTING_CODE.deserialize(String.format(ApiPlugin.getInstance().getMessagePrefix(), "CHAT")),
                    TextActions.executeCallback(commandSource -> {
                        if(extrasPlugin.getChatTeamsManager().getTeam(player).isPresent()){
                            MessageUtils.sendMessage(commandSource, "CHAT", "extras.chatteams.alreadyinteam");
                            return;
                        }
                        extrasPlugin.getChatTeamsManager().addToTeam(player, (Player) commandSource);
                    }), MessageUtils.translate("extras.chatteams.invite", player.getName())));
            MessageUtils.sendMessage(player, "CHAT", "extras.chatteams.invitesent");
            return CommandResult.success();
        }

        @Override
        public List<String> getAliases() {
            return Collections.singletonList("zapros");
        }

        @Override
        public String getPermission() {
            return "pl.raisemon.chatteams.invite";
        }

        @Override
        public CommandElement[] getArguments() {
            return new CommandElement[]{
                    GenericArguments.player(Text.of("player"))
            };
        }

        @Override
        public Collection<BaseCommand> getChildren() {
            return null;
        }
    }

    private class RemoveMember extends BasePlayerCommand{
        public RemoveMember(Object plugin) {
            super(plugin, "CHAT");
        }

        @Override
        public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
            final Optional<ChatTeam> team = extrasPlugin.getChatTeamsManager().getTeam(player);
            if(!team.isPresent() || !team.get().isOwner(player.getUniqueId())){
                MessageUtils.sendMessage(player, "CHAT", "extras.chatteams.onlyforowners");
                return CommandResult.success();
            }
            extrasPlugin.getChatTeamsManager().removeMember(team.get(), args.<User>getOne("user").get().getUniqueId());
            MessageUtils.sendMessage(player, "CHAT", "extras.chatteams.removedmember");

            return CommandResult.success();
        }

        @Override
        public List<String> getAliases() {
            return Collections.singletonList("wywal");
        }

        @Override
        public String getPermission() {
            return "pl.raisemon.chatteams.kick";
        }

        @Override
        public CommandElement[] getArguments() {
            return new CommandElement[]{
                    GenericArguments.user(Text.of("user"))
            };
        }

        @Override
        public Collection<BaseCommand> getChildren() {
            return null;
        }
    }

    private class ListMembers extends BasePlayerCommand{
        public ListMembers(Object plugin) {
            super(plugin, "CHAT");
        }

        @Override
        public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {

            return CommandResult.success();
        }

        @Override
        public List<String> getAliases() {
            return Collections.singletonList("lista");
        }

        @Override
        public String getPermission() {
            return "pl.raisemon.chatteams.list";
        }

        @Override
        public CommandElement[] getArguments() {
            return new CommandElement[0];
        }

        @Override
        public Collection<BaseCommand> getChildren() {
            return null;
        }
    }

}
