package pl.raisemon.extras.commands;

import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.extras.ExtrasPlugin;

import java.util.*;
import java.util.function.BiConsumer;

public class RandomTpBlockCommand extends BasePlayerCommand {
    public RandomTpBlockCommand(ExtrasPlugin extrasPlugin) {
        super(extrasPlugin, "RTP");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        Location<World> sub = player.getLocation().sub(0, 1, 0);
        if(sub.getBlockType() != BlockTypes.BARRIER){
            player.sendMessage(Text.of(TextColors.RED, "Musisz stac na barrier blocku"));
            return CommandResult.success();
        }
        List<Vector3i> vector3is = ExtrasPlugin.getInstance().getRandomTpBlocks().computeIfAbsent(player.getWorld().getName(), k -> new ArrayList<>());
        if(args.hasAny("all")){
            List<Vector3i> found = new ArrayList<>();
            found.add(sub.getBlockPosition());
            find(found, sub);
            vector3is.addAll(found);
            ExtrasPlugin.getInstance().saveRandomTpBlocks();
            player.sendMessage(Text.of(TextColors.GREEN, "Dodano " + found.size() + " blokow"));
        }else{
            vector3is.add(sub.getBlockPosition());
            ExtrasPlugin.getInstance().saveRandomTpBlocks();
            player.sendMessage(Text.of(TextColors.GREEN, "Dodano blok"));
        }
        return CommandResult.success();
    }

    private void find(List<Vector3i> found, Location<World> sub) {
        for (Direction direction : new Direction[]{Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST, Direction.NORTHEAST, Direction.NORTHWEST, Direction.SOUTHEAST, Direction.SOUTHWEST}) {
            Location<World> blockRelative = sub.getBlockRelative(direction);
            if(blockRelative.getBlockType() == BlockTypes.BARRIER && !found.contains(blockRelative.getBlockPosition())){
                found.add(blockRelative.getBlockPosition());
                find(found, blockRelative);
            }
        }
    }


    @Override
    public List<String> getAliases() {
        return Collections.singletonList("setuprtp");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.admin.setuprtp";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.optional(GenericArguments.bool(Text.of("all")))
        };
    }

    @Override
    public Collection<BaseCommand> getChildren() {
        return new HashSet<>(Arrays.asList(new Remove(ExtrasPlugin.getInstance(), "RTP")));
    }

    private class Remove extends BasePlayerCommand {
        public Remove(Object plugin, String tag) {
            super(plugin, tag);
        }

        @Override
        public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {

            ExtrasPlugin.getInstance().getRandomTpBlocks().forEach(new BiConsumer<String, List<Vector3i>>() {
                @Override
                public void accept(String s, List<Vector3i> vector3is) {
                    for (Vector3i vector3i : vector3is) {
                        player.sendMessage(Text.of(TextColors.GOLD, TextActions.executeCallback(commandSource -> {
                            ExtrasPlugin.getInstance().getRandomTpBlocks().get(s).remove(vector3i);
                            ExtrasPlugin.getInstance().saveRandomTpBlocks();
                            player.sendMessage(Text.of(TextColors.GREEN, "Skasowano blok"));
                        }), "Kliknij aby skasowac  X: " + vector3i.getX() + " Y: " + vector3i.getY() + " Z: " + vector3i.getZ()));

                    }
                }
            });
            return CommandResult.success();
        }

        @Override
        public List<String> getAliases() {
            return Collections.singletonList("usun");
        }

        @Override
        public String getPermission() {
            return "pl.raisemon.admin.setuprtp";
        }

        @Override
        public CommandElement[] getArguments() {
            return new CommandElement[0];
        }

        @Override
        public Collection<BaseCommand> getChildren() {
            return null;
        }
    }
}
