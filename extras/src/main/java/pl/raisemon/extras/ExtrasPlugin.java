package pl.raisemon.extras;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.spawning.SpawnInfo;
import com.pixelmonmod.pixelmon.api.spawning.SpawnSet;
import com.pixelmonmod.pixelmon.api.spawning.archetypes.entities.pokemon.SpawnInfoPokemon;
import com.pixelmonmod.pixelmon.spawning.PixelmonSpawning;
import lombok.Getter;
import net.minecraft.entity.player.EntityPlayerMP;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.Configurable;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.extras.chatteams.ChatTeamManager;
import pl.raisemon.extras.commands.*;
import pl.raisemon.extras.data.MessagesCommand;
import pl.raisemon.extras.listeners.*;
import pl.raisemon.mod.chat.ChatChannel;
import pl.raisemon.mod.networking.RaisemonPacketHandler;
import pl.raisemon.mod.networking.SCChatChannelsPacket;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;

@Plugin(
        id = "raisemon_extras",
        name = "RaisemonExtras",
        version = "1.0",
        authors = {"XopyIP"},
        dependencies = {
                @Dependency(id = "raisemon_api")
        }
)
public class ExtrasPlugin implements Configurable {
    @Inject
    private Game game;
    private List<CommandMapping> registredCommands = new ArrayList<>();
    private List<MessagesCommand> messageCommands;
    @Getter
    private ChatTeamManager chatTeamsManager;
    @Getter
    private static ExtrasPlugin instance;
    @Getter
    private Map<String, List<Vector3i>> randomTpBlocks;
    private CommentedConfigurationNode node;
    @Getter
    private CraftingConfig craftingConfig;
    @Getter
    private List<ItemType> blockedCraftings;

    private Text remove10secondsMessage = TextSerializers.FORMATTING_CODE.deserialize("&aItemki z ziemi zostana usuniete za 10 sekund!");
    private Text removePoke10secondsMessage = TextSerializers.FORMATTING_CODE.deserialize("&aDzikie pokemony zostana usuniete za 10 sekund!");
    private Text itemsRemoved = TextSerializers.FORMATTING_CODE.deserialize("&aItemki zostaly usuniete!");
    private Text pokeRemoved = TextSerializers.FORMATTING_CODE.deserialize("&aPokemony zostaly usuniete!");
    private Map<PlayerStatType, PlayerStatManager> managers = new HashMap<>();
    @Getter
    private ExpPlacesConfig expPlaceConfig;


    @Listener
    public void onInit(GamePreInitializationEvent event) {
        instance = this;
    }

    @Listener
    public void onInit(GameInitializationEvent event) {

        ApiPlugin.getInstance().loadConfig(this);
        ApiPlugin.getInstance().loadConfig(this.craftingConfig = new CraftingConfig());

        this.craftingConfig.getCraftings().forEach(crafting -> Sponge.getRegistry().getCraftingRecipeRegistry().register(crafting.getRecipe()));


        for (PlayerStatType value : PlayerStatType.values()) {
            final PlayerStatManager playerStatManager = new PlayerStatManager(value);
            playerStatManager.load();
            managers.put(value, playerStatManager);
        }

    }

    @Listener
    public void onStart(GameStartedServerEvent event) {
        ApiPlugin.getInstance().loadConfig(this.expPlaceConfig = new ExpPlacesConfig());

        this.chatTeamsManager = new ChatTeamManager();
        this.chatTeamsManager.load();
        game.getEventManager().registerListeners(this, new ChatChannelListener());
        game.getEventManager().registerListeners(this, new PlayerJoinListener(this));
        game.getEventManager().registerListeners(this, new ChatPokemonListener(this));
        game.getEventManager().registerListeners(this, new MoveRandomTpListener(this));

        game.getEventManager().registerListeners(this, new MultiCraftingListener(this));
        game.getEventManager().registerListeners(this, new BookPackListener(this));
        game.getEventManager().registerListeners(this, new LeverDelayListener());
        game.getEventManager().registerListeners(this, new BlockCraftingListener());
        game.getEventManager().registerListeners(this, new BlockNetherEndListener());
        game.getEventManager().registerListeners(this, new PokeChestListener());
        game.getEventManager().registerListeners(this, new BlockBreakListener());
        game.getEventManager().registerListeners(this, new FishingListener());
        game.getEventManager().registerListeners(this, new BLockShulkerListener());
        game.getEventManager().registerListeners(this, new ChickenSpawnListener());
        game.getEventManager().registerListeners(this, new PokeGiftListener());

        ApiPlugin.getInstance().registerCommand(new PokeColorCommand(this));
        ApiPlugin.getInstance().registerCommand(new ChatTeamCommand(this));
        ApiPlugin.getInstance().registerCommand(new AdminFishingRodLevelCommand(this));
        ApiPlugin.getInstance().registerCommand(new RandomTpBlockCommand(this));
        ApiPlugin.getInstance().registerCommand(new ExpPlaceCommand(this));
        ApiPlugin.getInstance().registerCommand(new PhCommand(this));
        ApiPlugin.getInstance().registerCommand(new RemoveEvsCommand(this));
        randomTpBlocks = new HashMap<>();
        CommentedConfigurationNode node = this.node.getNode("random-tp-blocks");
        node.getChildrenMap().forEach((BiConsumer<Object, CommentedConfigurationNode>) (o, commentedConfigurationNode) -> {
            try {
                randomTpBlocks.put((String) o, new ArrayList<>(commentedConfigurationNode.getList(TypeToken.of(Vector3i.class))));
            } catch (ObjectMappingException e) {
                e.printStackTrace();
            }
        });


        Pixelmon.EVENT_BUS.register(new PixelmonReceivedListener());
        Pixelmon.EVENT_BUS.register(new PixelmonKilledListener());
        Pixelmon.EVENT_BUS.register(new PokemonSpawnListener());
        Pixelmon.EVENT_BUS.register(new ShopKeeperListener());

        for (SpawnSet spawnInfos : PixelmonSpawning.standard) {
            for (SpawnInfo spawnInfo : spawnInfos.spawnInfos) {
                if(spawnInfo instanceof SpawnInfoPokemon){
                    if(((SpawnInfoPokemon) spawnInfo).heldItems != null) {
                        System.out.println("clearing " + ((SpawnInfoPokemon) spawnInfo).heldItems.size() + " held items");
                        ((SpawnInfoPokemon) spawnInfo).heldItems.clear();
                    }
                }
            }
        }

        game.getScheduler().createTaskBuilder().execute(() -> {
            game.getServer().getOnlinePlayers().forEach(player ->
                    MessageUtils.sendMessage(player, "AntyLag", remove10secondsMessage));

            game.getScheduler().createTaskBuilder().delay(10, TimeUnit.SECONDS).execute(() -> {
                game.getServer().getWorlds().forEach(world -> world.getEntities().stream().filter(entity -> entity instanceof Item).map(entity -> (Item) entity).forEach(Entity::remove));
                game.getServer().getOnlinePlayers().forEach(player ->
                        MessageUtils.sendMessage(player, "AntyLag", itemsRemoved));
            }).submit(ExtrasPlugin.this);
        }).delay(2, TimeUnit.MINUTES).interval(10, TimeUnit.MINUTES).name("ClearItem").submit(this);
        game.getScheduler().createTaskBuilder().execute(() -> {
            game.getServer().getOnlinePlayers().forEach(player ->
                    MessageUtils.sendMessage(player, "AntyLag", removePoke10secondsMessage));

            game.getScheduler().createTaskBuilder().delay(10, TimeUnit.SECONDS).execute(() -> {
                game.getCommandManager().process(game.getServer().getConsole(), "pokekill");
                game.getServer().getOnlinePlayers().forEach(player ->
                        MessageUtils.sendMessage(player, "AntyLag", pokeRemoved));

            }).submit(ExtrasPlugin.this);
        }).delay(2, TimeUnit.MINUTES).interval(10, TimeUnit.MINUTES).name("ClearPoke").submit(this);

    }

    @Override
    public String getConfigName() {
        return "Extras";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {
        try {
            node.getNode("MessageCommands").setValue(new TypeToken<List<MessagesCommand>>() {
            }, Arrays.asList(
                    new MessagesCommand(
                            Collections.singletonList("regulamin"),
                            "pl.raisemon.regulamin",
                            Arrays.asList("&aTestowa komenda", "&bLinia numer 2", "&cLinia nr 4", "&eNie bylo linii numer 3"),
                            Collections.emptyList())
            ));
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(CommentedConfigurationNode node) {
        this.node = node;
        registredCommands.forEach(cm -> Sponge.getCommandManager().removeMapping(cm));
        try {
            messageCommands = node.getNode("MessageCommands").getList(TypeToken.of(MessagesCommand.class));
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
        messageCommands.forEach(messagesCommand -> {
            Optional<CommandMapping> commandMapping = ApiPlugin.getInstance().registerCommand(new MessageCommandImpl(ExtrasPlugin.this, messagesCommand));
            commandMapping.ifPresent(cm -> registredCommands.add(cm));
        });
        try {
            blockedCraftings = node.getNode("BlockedCraftings").getList(TypeToken.of(ItemType.class));
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ConfigurationOptions getConfigurationOptions() {
        final ConfigurationOptions defaults = ConfigurationOptions.defaults();
        defaults.acceptsType(MessagesCommand.class);
        return defaults;
    }

    public void updateChatTabs(Player player) {
        final ArrayList<ChatChannel> channels = new ArrayList<>();
        channels.add(new ChatChannel('C', "Komendy"));
        channels.add(new ChatChannel('G', "Globalny"));
        channels.add(new ChatChannel('H', "Handel"));
        if (player.hasPermission("pl.raisemon.adminchatchannel")) {
            channels.add(new ChatChannel('A', "Administracyjny"));
        }
        if (getChatTeamsManager().getTeam(player).isPresent()) {
            channels.add(new ChatChannel('D', "Druzyna"));
        }
        RaisemonPacketHandler.INSTANCE.sendTo(new SCChatChannelsPacket(channels), (EntityPlayerMP) player);

    }

    public void saveRandomTpBlocks() {
        this.randomTpBlocks.forEach((s, vector3is) -> {
            try {
                node.getNode("random-tp-blocks", s).setValue(new TypeToken<List<Vector3i>>() {
                }, vector3is);
            } catch (ObjectMappingException e) {
                e.printStackTrace();
            }
        });
        ApiPlugin.getInstance().saveConfig(this);
    }

    public PlayerStatManager getManager(PlayerStatType type) {
        return managers.get(type);
    }
}
