package pl.raisemon.extras.listeners;

import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.item.inventory.CraftItemEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.inventory.crafting.CraftingInventory;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.extras.data.ShapedMultiRecipe;

public class MultiCraftingListener {
    public MultiCraftingListener(ExtrasPlugin extrasPlugin) {
    }

    @Listener
    public void onPreview(CraftItemEvent.Preview event) {
        ItemType type = event.getPreview().getDefault().getType();
        CraftingInventory craftingInventory = event.getCraftingInventory();
        CraftingGridInventory craftingGrid = craftingInventory.getCraftingGrid();
        for (ShapedMultiRecipe multiRecipe : ExtrasPlugin.getInstance().getCraftingConfig().getCraftings()) {
            if(!multiRecipe.getType().equals(type)){
                continue;
            }
            if (!multiRecipe.check(craftingGrid, 0)) {
                event.setCancelled(true);
            }
        }
    }

    @Listener
    public void onCraft(CraftItemEvent.Craft event) {
        ItemStackSnapshot crafted = event.getCrafted();
        ItemType type = crafted.getType();
        CraftingInventory craftingInventory = event.getCraftingInventory();
        CraftingGridInventory craftingGrid = craftingInventory.getCraftingGrid();
        for (ShapedMultiRecipe multiRecipe : ExtrasPlugin.getInstance().getCraftingConfig().getCraftings()) {
            if(!multiRecipe.getType().equals(type)){
                continue;
            }
            if (!multiRecipe.craft(craftingGrid)) {
                event.setCancelled(true);
            }
        }
    }
}
