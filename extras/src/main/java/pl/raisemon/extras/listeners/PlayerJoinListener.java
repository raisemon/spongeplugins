package pl.raisemon.extras.listeners;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.effect.potion.PotionEffect;
import org.spongepowered.api.effect.potion.PotionEffectTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.leaders.BadgeManager;
import pl.raisemon.mod.listeners.PokeDexListener;
import pl.raisemon.mod.skill.SkillManager;
import pl.raisemon.mod.skills.Skill;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

public class PlayerJoinListener {
    private static final double defaultSpeed = 0.1f;
    private ExtrasPlugin extrasPlugin;

    public PlayerJoinListener(ExtrasPlugin extrasPlugin) {
        this.extrasPlugin = extrasPlugin;
    }

    @Listener
    public void onJoin(ClientConnectionEvent.Join event) {
        event.getTargetEntity().offer(Keys.WALKING_SPEED, defaultSpeed * SkillManager.getBonus(event.getTargetEntity().getUniqueId(), Skill.SPEED) *
                (BadgeManager.countBadges(event.getTargetEntity().getUniqueId()) > 0 ? 1.2f : 1));
        extrasPlugin.updateChatTabs(event.getTargetEntity());
        if (BadgeManager.countBadges(event.getTargetEntity().getUniqueId()) > 5) {
            event.getTargetEntity().offer(Keys.POTION_EFFECTS, Collections.singletonList(PotionEffect.builder().potionType(PotionEffectTypes.HASTE).amplifier(1).particles(false).duration(Integer.MAX_VALUE).build()));
        }
        PokeDexListener.giveCharm(event.getTargetEntity().getUniqueId());
        Sponge.getScheduler().createTaskBuilder().delay(5, TimeUnit.SECONDS).execute(()->{
            PokeDexListener.giveCharm(event.getTargetEntity().getUniqueId());
        }).submit(ExtrasPlugin.getInstance());
    }

    @Listener
    public void onWorldJoin(MoveEntityEvent.Teleport event){
        if(!(event.getTargetEntity() instanceof Player)){
            return;
        }
        if(!event.getToTransform().getExtent().getName().equals(event.getFromTransform().getExtent().getName())){
            extrasPlugin.updateChatTabs((Player) event.getTargetEntity());
            PokeDexListener.giveCharm(event.getTargetEntity().getUniqueId());
        }
    }
}
