package pl.raisemon.extras.listeners;

import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.extras.PlayerStatType;

public class BlockBreakListener {
    @Listener
    public void onBreak(ChangeBlockEvent.Break event, @First Player player){
        for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
            if(transaction.getOriginal().getState().getType().equals(BlockTypes.STONE)){
                ExtrasPlugin.getInstance().getManager(PlayerStatType.MINED_STONE).increment(player.getUniqueId());

            }
        }
    }
}
