package pl.raisemon.extras.listeners;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.world.World;
import pl.raisemon.extras.ExtrasPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class MoveRandomTpListener {
    private ExtrasPlugin extrasPlugin;
    private Map<UUID, Long> rtpDelay = new HashMap<>();

    public MoveRandomTpListener(ExtrasPlugin extrasPlugin) {
        this.extrasPlugin = extrasPlugin;
    }

    @Listener
    public void onMove(MoveEntityEvent event){
        if (event.getTargetEntity() instanceof Player) {
            Vector3d toPos = event.getToTransform().getPosition();
            Vector3d fromPos = event.getFromTransform().getPosition();
            World extent = event.getToTransform().getExtent();
            if(toPos.getFloorY() != fromPos.getFloorY()){
                Vector3i vector3i = toPos.toInt();
                for(int i = 0; i<6; i++){
                    Vector3i sub = vector3i.sub(0, i, 0);
                    BlockType blockType = extent.getBlockType(sub);
                    if (blockType == BlockTypes.BARRIER) {
                        List<Vector3i> vector3is = extrasPlugin.getRandomTpBlocks().get(event.getToTransform().getExtent().getName());
                        if(vector3is != null && vector3is.contains(sub)){
                            if(rtpDelay.containsKey(event.getTargetEntity().getUniqueId()) && rtpDelay.get(event.getTargetEntity().getUniqueId()) + 1000 > System.currentTimeMillis()){
                                return;
                            }
                            rtpDelay.put(event.getTargetEntity().getUniqueId(), System.currentTimeMillis());
                            Sponge.getCommandManager().process(Sponge.getServer().getConsole(), "rtp " + ((Player) event.getTargetEntity()).getName() + " world");
                        }
                    }
                }
            }
        }
    }
}
