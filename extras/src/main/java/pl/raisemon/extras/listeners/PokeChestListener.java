package pl.raisemon.extras.listeners;

import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;

public class PokeChestListener {
    @Listener
    public void onClick(InteractBlockEvent event){
        BlockType type = event.getTargetBlock().getState().getType();
        if(type.getId().startsWith("pixelmon") && type.getId().endsWith("chest")){
            event.getTargetBlock().getLocation().ifPresent(loc -> loc.setBlockType(BlockTypes.AIR));
            event.setCancelled(true);
        }
    }
}
