package pl.raisemon.extras.listeners;

import com.pixelmonmod.pixelmon.api.events.spawning.PixelmonSpawnerEvent;
import net.minecraft.util.math.Vec3i;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class PokemonSpawnListener {
    @SubscribeEvent
    public void onSpawn(PixelmonSpawnerEvent event){
        if(event.spawner.getWorld().getWorldInfo().getWorldName().equalsIgnoreCase("world")) {
            if (event.spawner.getPos().distanceSq(new Vec3i(0, event.spawner.getPos().getY(), 0)) > 500 * 500) {
                event.setCanceled(true);
            }
        }
    }
}
