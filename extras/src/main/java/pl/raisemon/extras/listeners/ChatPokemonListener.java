package pl.raisemon.extras.listeners;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.StatsType;
import net.minecraft.entity.player.EntityPlayerMP;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.message.MessageEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.extras.ExtrasPlugin;

public class ChatPokemonListener {
    private ExtrasPlugin extrasPlugin;

    public ChatPokemonListener(ExtrasPlugin extrasPlugin) {
        this.extrasPlugin = extrasPlugin;
    }

    @Listener
    public void onChat(MessageEvent event, @First Player player) {
        Text replace = MessageUtils.replace(event.getMessage(), "#POKE1", buildText(Pixelmon.storageManager.getParty((EntityPlayerMP) player).get(0)));
        replace = (MessageUtils.replace(replace, "#POKE2", buildText(Pixelmon.storageManager.getParty((EntityPlayerMP) player).get(1))));
        replace = (MessageUtils.replace(replace, "#POKE3", buildText(Pixelmon.storageManager.getParty((EntityPlayerMP) player).get(2))));
        replace = (MessageUtils.replace(replace, "#POKE4", buildText(Pixelmon.storageManager.getParty((EntityPlayerMP) player).get(3))));
        replace = (MessageUtils.replace(replace, "#POKE5", buildText(Pixelmon.storageManager.getParty((EntityPlayerMP) player).get(4))));
        replace = (MessageUtils.replace(replace, "#POKE6", buildText(Pixelmon.storageManager.getParty((EntityPlayerMP) player).get(5))));
        event.setMessage(replace);
    }

    private Text buildText(Pokemon pokemon) {
        if (pokemon == null) {
            return Text.of("");
        }
        return Text
                .builder()
                .append(Text.of(
                        TextColors.GOLD,
                        TextStyles.UNDERLINE,
                        pokemon.getSpecies().name))
                .onHover(TextActions.showText(
                        Text.builder()
                                //Lvl, Natura, Abillitka, Gender, Ivsy ,Move
                                .append(Text.of(
                                        TextColors.GOLD, "LVL: ",
                                        TextColors.GRAY, pokemon.getLevel(),
                                        TextColors.GOLD, "Naturka: ",
                                        TextColors.GRAY, pokemon.getNature().getLocalizedName(),

                                        TextColors.GOLD, "Abilitka: ",
                                        TextColors.GRAY, pokemon.getAbilityName(),

                                        TextColors.GOLD, "Plec: ",
                                        TextColors.GRAY, pokemon.getGender().getLocalizedName()
                                )).append(Text.NEW_LINE)
                                .append(Text.of(
                                        TextColors.GOLD, "IVs: ",
                                        TextColors.GREEN, "HP: ", TextColors.YELLOW, pokemon.getIVs().get(StatsType.HP),
                                        TextColors.GREEN, " Attack: ", TextColors.YELLOW, pokemon.getIVs().get(StatsType.Attack),
                                        TextColors.GREEN, " Defence: ", TextColors.YELLOW, pokemon.getIVs().get(StatsType.Defence),
                                        TextColors.GREEN, " Sp. Attack: ", TextColors.YELLOW, pokemon.getIVs().get(StatsType.SpecialAttack),
                                        TextColors.GREEN, " Sp. Defence: ", TextColors.YELLOW, pokemon.getIVs().get(StatsType.SpecialDefence),
                                        TextColors.GREEN, " Speed: ", TextColors.YELLOW, pokemon.getIVs().get(StatsType.Speed)
                                )).append(Text.NEW_LINE)
                                .append(Text.of(
                                        TextColors.GOLD, "Ataki: ",
                                        TextColors.GREEN, pokemon.getMoveset().get(0) == null ? "" : pokemon.getMoveset().get(0).getActualMove().getLocalizedName(),
                                        ", ", pokemon.getMoveset().get(1) == null ? "" : pokemon.getMoveset().get(1).getActualMove().getLocalizedName(),
                                        ", ", pokemon.getMoveset().get(2) == null ? "" : pokemon.getMoveset().get(2).getActualMove().getLocalizedName(),
                                        ", ", pokemon.getMoveset().get(3) == null ? "" : pokemon.getMoveset().get(3).getActualMove().getLocalizedName()
                                ))
                                .build()
                ))
                .build();
    }
}
