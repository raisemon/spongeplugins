package pl.raisemon.extras.listeners;

import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.animal.Chicken;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.SpawnEntityEvent;

public class ChickenSpawnListener {
    @Listener
    public void onSpawn(SpawnEntityEvent entityEvent){
        for (Entity entity : entityEvent.getEntities()) {
            if(entity instanceof Chicken){
                entityEvent.setCancelled(true);
            }
        }
    }
}
