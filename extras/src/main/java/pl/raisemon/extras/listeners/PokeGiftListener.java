package pl.raisemon.extras.listeners;

import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class PokeGiftListener {
    @Listener
    public void onPlace(ChangeBlockEvent.Place blockEvent){
        for (Transaction<BlockSnapshot> transaction : blockEvent.getTransactions()) {
            if(transaction.getOriginal().getState().getType().getId().equalsIgnoreCase("pixelmon:poke_gift")){
                blockEvent.setCancelled(true);
            }
        }
    }
    @Listener
    public void onInteract(InteractBlockEvent blockEvent){
        if(blockEvent.getTargetBlock().getState().getType().getId().equalsIgnoreCase("pixelmon:poke_gift")){
            blockEvent.setCancelled(true);
            if(blockEvent.getCause().containsType(Player.class)){
                blockEvent.getCause().first(Player.class).get().sendMessage(Text.of(TextColors.RED, "Poke Gifty są zablokowane!"));
                System.out.println("interact with pokegift " + blockEvent.getTargetBlock().getLocation().orElse(null));
            }
        }
    }
}
