package pl.raisemon.extras.listeners;

import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.item.inventory.CraftItemEvent;
import org.spongepowered.api.item.ItemType;
import pl.raisemon.extras.ExtrasPlugin;

public class BlockCraftingListener {
    @Listener
    public void onCraft(CraftItemEvent.Craft event){
        ItemType type = event.getCrafted().getType();
        if(type.getId().contains("shulker")){
            event.setCancelled(true);
        }
        if(ExtrasPlugin.getInstance().getBlockedCraftings().contains(type)){
            event.setCancelled(true);
        }
    }
    @Listener
    public void onPreview(CraftItemEvent.Preview event){
        ItemType type = event.getPreview().getDefault().getType();
        if(type.getId().contains("shulker")){
            event.setCancelled(true);
        }
        if(ExtrasPlugin.getInstance().getBlockedCraftings().contains(type)){
            event.setCancelled(true);
        }
    }
}
