package pl.raisemon.extras.listeners;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.filter.cause.First;
import pl.raisemon.drop.events.ItemsFishedOutEvent;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.extras.PlayerStatType;

public class FishingListener {
    @Listener
    public void onFishing(ItemsFishedOutEvent event, @First Player player){
        ExtrasPlugin.getInstance().getManager(PlayerStatType.FISHED_OUT).increment(player.getUniqueId());

    }
}
