package pl.raisemon.extras.listeners;

import com.pixelmonmod.pixelmon.api.events.ShopkeeperEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ShopKeeperListener {
    @SubscribeEvent
    public void onInteract(ShopkeeperEvent.Sell event){
        event.setCanceled(true);
    }
    @SubscribeEvent
    public void onInteract(ShopkeeperEvent.Purchase event){
        event.setCanceled(true);
    }
}
