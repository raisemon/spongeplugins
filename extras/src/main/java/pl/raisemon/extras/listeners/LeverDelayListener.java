package pl.raisemon.extras.listeners;

import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import pl.raisemon.api.utils.MessageUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LeverDelayListener {
    private Map<UUID, Long> lastUsage = new HashMap<>();
    @Listener
    public void onInteract(InteractBlockEvent.Secondary.MainHand event, @First Player player){
        if (!event.getTargetBlock().getExtendedState().getType().equals(BlockTypes.LEVER)) {
            return;
        }
        if(!lastUsage.containsKey(player.getUniqueId())){
            lastUsage.put(player.getUniqueId(), System.currentTimeMillis());
            return;
        }
        if(lastUsage.get(player.getUniqueId()) + 1000 < System.currentTimeMillis()){
            lastUsage.put(player.getUniqueId(), System.currentTimeMillis());
            return;
        }
        MessageUtils.sendMessage(player, "AntyLag", "extras.antyLagLever");
        event.setCancelled(true);
    }
}
