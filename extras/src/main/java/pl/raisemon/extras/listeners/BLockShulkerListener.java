package pl.raisemon.extras.listeners;

import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;

public class BLockShulkerListener {
    @Listener
    public void onPlaceShulker(ChangeBlockEvent.Place event){
        for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
            if(transaction.getDefault().getState().getType().getId().contains("shulker")){
                event.setCancelled(true);
            }
        }
    }
}
