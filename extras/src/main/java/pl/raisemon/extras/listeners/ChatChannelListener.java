package pl.raisemon.extras.listeners;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.text.transform.SimpleTextTemplateApplier;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.extras.chatteams.ChatTeam;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ChatChannelListener {

    private final Pattern CHANNEL_REGEXP = Pattern.compile("[?A-Z]>");
    @Listener(order = Order.LATE)
    public void onDrop(MessageChannelEvent.Chat event, @First Player player) {
        final Text.Builder builder = Text.builder();
        boolean found = false;
        for (SimpleTextTemplateApplier simpleTextTemplateApplier : event.getFormatter().getBody()) {
            String serialize = TextSerializers.FORMATTING_CODE.serialize(simpleTextTemplateApplier.toText());
            if(player.hasPermission("pl.raisemon.rainbow")) {
                serialize = serialize.replace("&t", "§t")
                        .replace("&T", "§t");
            }
            final Matcher matcher = CHANNEL_REGEXP.matcher(serialize);
            if(matcher.find()){
                found = true;
                final String group = matcher.group(0);
                char channel = group.charAt(0);
                if(channel == 'A'){
                    if(!player.hasPermission("pl.raisemon.adminchatchannel")){
                        channel = 'G';
                    }else{
                        event.setChannel(() -> Sponge.getServer().getOnlinePlayers().stream().filter(p -> p.hasPermission("pl.raisemon.adminchatchannel")).collect(Collectors.toList()));
                    }
                }
                if(channel == 'C'){
                    channel = 'G';
                }
                if(channel == 'D'){
                    final Optional<ChatTeam> team = ExtrasPlugin.getInstance().getChatTeamsManager().getTeam(player);
                    if(team.isPresent()){
                        event.setChannel(() -> team.get().getUsers().stream().map(uuid -> Sponge.getServer().getPlayer(uuid).get()).collect(Collectors.toList()));
                    }else{
                        event.setCancelled(true);
                    }
                }
                event.getFormatter().setHeader(Text.builder().append(Text.of(channel + ">")).append(event.getFormatter().getHeader().toText()).build());
            }
            builder.append(TextSerializers.FORMATTING_CODE.deserialize(matcher.replaceFirst("")));
        }
        if(!found){
            event.getFormatter().setHeader(Text.builder().append(Text.of("G>")).append(event.getFormatter().getHeader().toText()).build());
        }
        event.getFormatter().setBody(builder.build());
    }
}
