package pl.raisemon.extras.listeners;

import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import pl.raisemon.api.utils.InventoryUtils;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.mod.RegistrationHandler;
import pl.raisemon.mod.skills.Skill;

import java.util.Random;

public class BookPackListener {
    private static final Random RAND = new Random();

    public BookPackListener(ExtrasPlugin extrasPlugin) {
    }

    @Listener
    public void onInteract(InteractItemEvent event, @First Player player) {
        if (event instanceof InteractItemEvent.Primary.OffHand || event instanceof InteractItemEvent.Secondary.OffHand) {
            return;
        }
        int type = -1;
        if (event.getItemStack().getType().equals(RegistrationHandler.BOOK_PACK)) {
            type = 0;
        } else if (event.getItemStack().getType().equals(RegistrationHandler.BOOK_PACK_Z)) {
            type = 1;
        } else if (event.getItemStack().getType().equals(RegistrationHandler.BOOK_PACK_M)) {
            type = 2;
        }
        if (type != -1) {
            player.getItemInHand(HandTypes.MAIN_HAND).ifPresent(itemStack -> itemStack.setQuantity(itemStack.getQuantity() - 1));
            int n = 1;
            float v = RAND.nextFloat();
            if (v < 0.15f) {
                n = 4;
            } else if (v < 0.35f) {
                n = 3;
            } else if (v < 0.50f) {
                n = 2;
            }
            for (int i = 0; i < n; i++) {
                InventoryUtils.giveItem(player, ItemStack.builder().itemType((ItemType) RegistrationHandler.BOOKS[RAND.nextInt(Skill.ALL.length)][type]).build());
            }
        }
    }
}
