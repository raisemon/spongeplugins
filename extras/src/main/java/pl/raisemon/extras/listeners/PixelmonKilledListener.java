package pl.raisemon.extras.listeners;

import com.pixelmonmod.pixelmon.api.events.BeatWildPixelmonEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.extras.PlayerStatType;

import java.util.Arrays;
import java.util.UUID;

public class PixelmonKilledListener {
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void handle(BeatWildPixelmonEvent event) {
        if(event.isCanceled() || event.player == null || Arrays.stream(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerProfiles()).noneMatch(p->p.getId().equals(event.player.getUniqueID()))){
            return;
        }
        if(!event.wpp.getWorld().getWorldInfo().getWorldName().equalsIgnoreCase("world")){
            return;
        }
        UUID uuid1 = event.player.getUniqueID();
        ExtrasPlugin.getInstance().getManager(PlayerStatType.KILLED_POKEMONS).increment(uuid1);

    }
}
