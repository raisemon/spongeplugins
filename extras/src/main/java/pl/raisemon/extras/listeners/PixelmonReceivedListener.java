package pl.raisemon.extras.listeners;

import com.pixelmonmod.pixelmon.api.enums.ReceiveType;
import com.pixelmonmod.pixelmon.api.events.PixelmonReceivedEvent;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import pl.raisemon.extras.ExtrasPlugin;
import pl.raisemon.extras.PlayerStatType;

import java.util.Arrays;
import java.util.UUID;

public class PixelmonReceivedListener {
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void handle(PixelmonReceivedEvent event) {
        if(event.isCanceled() || event.player == null || Arrays.stream(FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getOnlinePlayerProfiles()).noneMatch(p->p.getId().equals(event.player.getUniqueID()))){
            return;
        }

        if(event.receiveType != ReceiveType.PokeBall){
            return;
        }
        if(!event.player.world.getWorldInfo().getWorldName().equalsIgnoreCase("world")){
            return;
        }
        UUID uuid1 = event.player.getUniqueID();
        ExtrasPlugin.getInstance().getManager(PlayerStatType.CAUGHT_POKEMONS).increment(uuid1);
    }
}