package pl.raisemon.extras.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ConfigSerializable
public class MessagesCommand {
    @Setting
    private List<String> aliases;
    @Setting
    private String permission;
    @Setting
    private List<String> messageLines;
    @Setting
    private List<MessagesCommand> children;
}
