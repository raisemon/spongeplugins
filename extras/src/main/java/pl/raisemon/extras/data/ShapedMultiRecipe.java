package pl.raisemon.extras.data;

import lombok.Data;
import lombok.NoArgsConstructor;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.crafting.CraftingGridInventory;
import org.spongepowered.api.item.recipe.crafting.CraftingRecipe;
import org.spongepowered.api.item.recipe.crafting.Ingredient;
import org.spongepowered.api.item.recipe.crafting.ShapedCraftingRecipe;
import pl.raisemon.extras.ExtrasPlugin;

import java.util.List;
import java.util.Map;

@Data
@ConfigSerializable
@NoArgsConstructor
public class ShapedMultiRecipe {

    @Setting
    private String id;
    @Setting
    private String row1;
    @Setting
    private String row2;
    @Setting
    private String row3;
    @Setting
    private Map<String, ItemType> items;
    @Setting
    private ItemType type;
    @Setting
    private List<Integer> amounts;

    public ShapedMultiRecipe(String id, String row1, String row2, String row3, Map<String, ItemType> items, ItemType type, List<Integer> amounts) {
        this.id = id;
        this.row1 = row1;
        this.row2 = row2;
        this.row3 = row3;
        this.items = items;
        this.type = type;
        this.amounts = amounts;
    }

    public CraftingRecipe getRecipe(){
        ShapedCraftingRecipe.Builder.AisleStep aisle = ShapedCraftingRecipe.builder()
                .aisle(row1, row2, row3);
        for (Map.Entry<String, ItemType> entry : items.entrySet()) {
            aisle = aisle.where(entry.getKey().charAt(0), Ingredient.of(entry.getValue()));
        }
        return ((ShapedCraftingRecipe.Builder.ResultStep)aisle)
                .result(ItemStack.of(type, 1))
                .build(id, ExtrasPlugin.getInstance());
    }

    public boolean check(CraftingGridInventory craftingGrid, int z){
        for(int i = 0; i<9; i++){
            if(!requireAmount(craftingGrid, i%3, i/3, amounts.get(i) + z)){
                return false;
            }
        }
        return true;
    }

    public boolean craft(CraftingGridInventory craftingGrid){
        if(!check(craftingGrid, -1)){
            return false;
        }
        for(int i = 0; i<9; i++){
            removeSlot(craftingGrid, i%3, i/3, amounts.get(i)-1);
        }
        return true;
    }



    private void removeSlot(CraftingGridInventory craftingGrid, int x, int y, int amount) {
        craftingGrid.getSlot(x, y).ifPresent(slot -> slot.poll(amount));
    }

    private boolean requireAmount(CraftingGridInventory craftingGrid, int x, int y, int amount) {
        if(amount == 0){
            return true;
        }
        return craftingGrid.getSlot(x, y)
                .map(inventories -> inventories.peek().filter(itemStack -> itemStack.getQuantity() >= amount).isPresent())
                .orElse(false);

    }
}
