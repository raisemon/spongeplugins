package pl.raisemon.extras;

import lombok.Getter;

public enum PlayerStatType {
    CAUGHT_POKEMONS("rsmon_caught_pokemons", "zlapane"),
    KILLED_POKEMONS("rsmon_killed_pokemons", "zabite"),
    FISHED_OUT("rsmon_fished_out", "zlowione"),
    MINED_STONE("rsmon_mined_stone", "stone");

    @Getter
    private String table_name;
    @Getter
    private String name;

    PlayerStatType(String table_name, String name) {
        this.table_name = table_name;
        this.name = name;
    }
}

