package pl.raisemon.drop.events;

import org.spongepowered.api.event.Event;
import org.spongepowered.api.event.cause.Cause;
import pl.raisemon.drop.data.FishingDrop;

import java.util.List;

public class ItemsFishedOutEvent implements Event {
    private final Cause cause;
    private final List<FishingDrop> fishingDrops;

    public ItemsFishedOutEvent(Cause cause, List<FishingDrop> fishingDrops) {
        this.cause = cause;
        this.fishingDrops = fishingDrops;
    }

    @Override
    public Cause getCause() {
        return cause;
    }
}
