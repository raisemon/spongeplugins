package pl.raisemon.drop.commands;

import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.DyeColors;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.property.SlotPos;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.item.inventory.type.GridInventory;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.drop.DropPlugin;
import pl.raisemon.drop.data.BlockDrop;
import pl.raisemon.drop.data.DropSettings;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class DropCommand extends BasePlayerCommand {

    private final static ItemStack dropAllOn = ItemStack.builder()
            .itemType(ItemTypes.WOOL)
            .build();
    private final static ItemStack dropAllOff = ItemStack.builder()
            .itemType(ItemTypes.WOOL)
            .build();

    static{
        dropAllOn.offer(Keys.DYE_COLOR, DyeColors.GREEN);
        dropAllOn.offer(Keys.DISPLAY_NAME, Text.of(TextColors.GREEN, "Wlacz caly drop"));
        dropAllOff.offer(Keys.DYE_COLOR, DyeColors.RED);
        dropAllOff.offer(Keys.DISPLAY_NAME, Text.of(TextColors.RED, "Wylacz caly drop"));
    }

    private DropPlugin plugin;


    public DropCommand(DropPlugin plugin) {
        super(plugin, "DROP");
        this.plugin = plugin;
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {

        final DropSettings settings = DropPlugin.getInstance().getSettings(player);
        player.sendMessage(Text.of(TextColors.GREEN, "DROP ZE STONE"));

        for (BlockDrop blockDrop : DropPlugin.getInstance().getBlockDrops().get(BlockTypes.STONE)) {
            player.sendMessage(Text.of(TextActions.executeCallback(user ->{
                if(settings.isActive(blockDrop.getDropItem().getType())){
                    settings.disable(blockDrop.getDropItem().getType());
                    player.sendMessage(Text.of(TextColors.RED, "Wylaczyles drop " + blockDrop.getDropItem().toItemStack().getTranslation().get()));
                }else{
                    settings.enable(blockDrop.getDropItem().getType());
                    player.sendMessage(Text.of(TextColors.GREEN, "Wlaczyles drop " + blockDrop.getDropItem().toItemStack().getTranslation().get()));
                }
            }), TextActions.showText(Text.of(blockDrop.getLore(settings, player).stream().collect(Text::builder, (r, text) -> r.append(text).append(Text.NEW_LINE), (builder, builder2) -> builder.append(builder2.build())).build())), TextColors.GOLD, blockDrop.getDropItem().toItemStack().getTranslation().get()));
        }
        player.sendMessage(Text.of());
        player.sendMessage(Text.of(TextActions.executeCallback((callback)->{
            settings.setDropToInventory(!settings.isDropToInventory());
            if(settings.isDropToInventory()){
                player.sendMessage(Text.of(TextColors.GREEN, "Wlaczyles drop do EQ"));
            }else{
                player.sendMessage(Text.of(TextColors.RED, "Wylaczyles drop do EQ"));
            }
        }),TextActions.showText(Text.of("kliknij by zmienic")), TextColors.YELLOW, "Drop do ekwipunku: ",settings.isDropToInventory()?Text.of(TextColors.GREEN, "ON"):Text.of(TextColors.RED, "OFF")));
        player.sendMessage(Text.of(TextActions.executeCallback((callback)->{
            settings.setDropCobble(!settings.isDropCobble());
            if(settings.isDropCobble()){
                player.sendMessage(Text.of(TextColors.GREEN, "Wlaczyles drop cobblestone"));
            }else{
                player.sendMessage(Text.of(TextColors.RED, "Wylaczyles drop cobblestone"));
            }
        }), TextActions.showText(Text.of("kliknij by zmienic")),TextColors.YELLOW, "Drop cobblestone: ", settings.isDropCobble()?Text.of(TextColors.GREEN, "ON"):Text.of(TextColors.RED, "OFF")));

        /*int height = (int) Math.min((DropPlugin.getInstance().getBlockDrops().get(BlockTypes.STONE).size()/9)+2, 6);

        Inventory dropInventory = Inventory.builder()
                .of(InventoryArchetypes.CHEST)
                .property(InventoryDimension.of(9, height))
                .property(InventoryTitle.PROPERTY_NAME, InventoryTitle.of(DropPlugin.getInstance().getInventoryTitle()))
                .listener(InteractInventoryEvent.class, e -> {
                    if (e instanceof InteractInventoryEvent.Open || e instanceof InteractInventoryEvent.Close) {
                        return;
                    }
                    e.setCancelled(true);
                    if (e instanceof ClickInventoryEvent) {
                        final ItemStackSnapshot original = e.getCursorTransaction().getDefault();
                        boolean changed = false;
                        if (ItemTypes.WOOL.equals(original.getType())) {
                            final DyeColor dyeColor = original.get(Keys.DYE_COLOR).orElse(DyeColors.BLACK);
                            if(dyeColor.equals(DyeColors.RED)){
                                DropPlugin.getInstance().getBlockDrops().get(BlockTypes.STONE).forEach(i -> settings.disable(i.getDropItem().getType()));
                                changed = true;
                            }else if(dyeColor.equals(DyeColors.GREEN)){
                                settings.removeAll();
                                changed = true;
                            }
                        }else if(ItemTypes.DIAMOND_PICKAXE.equals(original.getType())){
                            settings.setDropToInventory(!settings.isDropToInventory());
                            changed = true;
                        }else if(ItemTypes.COBBLESTONE.equals(original.getType())){
                            settings.setDropCobble(!settings.isDropCobble());
                            changed = true;
                        }else if(!ItemTypes.AIR.equals(original.getType())){
                            settings.toggle(original.getType());
                            changed = true;
                        }
                        if(changed){
                            Sponge.getScheduler().createTaskBuilder().execute(()->{
                                fillTop(player.getOpenInventory().get(), settings, player);
                            }).submit(plugin);
                        }
                    }
                })
                .build(plugin);
        fillTop(dropInventory, settings, player);
        player.openInventory(dropInventory);*/
        return CommandResult.success();
    }

    private void fillTop(Inventory dropInventory, DropSettings settings, Player player) {
        int height = (int) Math.min((DropPlugin.getInstance().getBlockDrops().get(BlockTypes.STONE).size()/9)+2, 6);
        int x = 0;
        for (BlockDrop blockDrop : DropPlugin.getInstance().getBlockDrops().get(BlockTypes.STONE)) {
            dropInventory.query(QueryOperationTypes.INVENTORY_TYPE.of(GridInventory.class))
                    .query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(x % 9, x / 9)))
                    .set(blockDrop.getInventoryItemstack(settings, player));
            x++;
        }
        dropInventory.query(QueryOperationTypes.INVENTORY_TYPE.of(GridInventory.class))
                .query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(2, height-1)))
                .set(dropAllOn.copy());
        dropInventory.query(QueryOperationTypes.INVENTORY_TYPE.of(GridInventory.class))
                .query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(3, height-1)))
                .set(dropAllOff.copy());


        ItemStack dropToInventory = ItemStack.builder()
                .itemType(ItemTypes.DIAMOND_PICKAXE)
                .build();
        dropToInventory.offer(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Drop do ekwipunku: ",
                settings.isDropToInventory()?Text.of(TextColors.GREEN, "ON"):Text.of(TextColors.RED, "OFF")));

        dropInventory.query(QueryOperationTypes.INVENTORY_TYPE.of(GridInventory.class))
                .query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(5, height - 1))).set(dropToInventory);


        ItemStack dropCobble = ItemStack.builder()
                .itemType(ItemTypes.COBBLESTONE)
                .build();
        dropCobble.offer(Keys.DISPLAY_NAME, Text.of(TextColors.YELLOW, "Drop cobblestone: ",
                settings.isDropCobble()?Text.of(TextColors.GREEN, "ON"):Text.of(TextColors.RED, "OFF")));

        dropInventory.query(QueryOperationTypes.INVENTORY_TYPE.of(GridInventory.class))
                .query(QueryOperationTypes.INVENTORY_PROPERTY.of(SlotPos.of(6, height - 1))).set(dropCobble);

    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("drop");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.dropgui";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }
}
