package pl.raisemon.drop.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.drop.DropPlugin;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class DropLimitCommand extends BasePlayerCommand {
    public DropLimitCommand(DropPlugin dropPlugin) {
        super(dropPlugin, "LIMIT");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        int limit = DropPlugin.getInstance().getDropLimitManager().getLimit();
        int amount = DropPlugin.getInstance().getDropLimitManager().getAmount(player);
        int percent = amount * 100 / limit;
        int blocks = 40 * percent / 100;
        StringBuffer progress = new StringBuffer("[");
        for (int i = 0; i < 40; i++) {
            if (blocks <= i) {
                progress.append("-");
            } else {
                progress.append("█");
            }
        }
        progress.append("]");
        player.sendMessage(Text.of(TextColors.GOLD, progress));
        player.sendMessage(Text.of(TextColors.GOLD, amount, TextColors.GRAY, "/", TextColors.GOLD, limit,
                TextColors.GRAY, " (", TextColors.GOLD, percent, "%", TextColors.GRAY, ")"));

        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("droplimit");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Collection<BaseCommand> getChildren() {
        return null;
    }
}
