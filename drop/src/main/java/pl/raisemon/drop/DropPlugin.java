package pl.raisemon.drop;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.config.PixelmonItemsHeld;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.enums.EnumSpecies;
import com.pixelmonmod.pixelmon.items.heldItems.ItemMegaStone;
import lombok.Getter;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.slf4j.Logger;
import org.spongepowered.api.Game;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.Configurable;
import pl.raisemon.api.utils.TimeUtils;
import pl.raisemon.drop.commands.DropCommand;
import pl.raisemon.drop.commands.DropLimitCommand;
import pl.raisemon.drop.data.*;
import pl.raisemon.drop.listeners.*;
import pl.raisemon.leaders.BadgeManager;
import pl.raisemon.mod.skill.SkillManager;
import pl.raisemon.mod.skills.Skill;

import java.util.*;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

@Plugin(
        id = "raisemon_drop",
        name = "RaisemonDrop",
        url = "https://raisemon.pl",
        version = "1.0",
        description = "Drop",
        authors = {
                "XopyIP"
        },
        dependencies = {
                @Dependency(id = "raisemon_api")
        }
)
public class DropPlugin implements Configurable {

    @Inject
    private Logger logger;
    @Inject
    private Game game;

    @Getter
    private Map<BlockType, List<BlockDrop>> blockDrops = new HashMap<>();
    private Map<String, List<PokemonDrop>> pokemonDrops = new HashMap<>();

    public static final Random RANDOM = new Random();
    @Getter
    private static DropPlugin instance;
    @Getter
    private List<BlockType> disabledBlocks;
    @Getter
    private List<String> disabledPokemonDrops;

    private Map<UUID, DropSettings> dropSettingsMap = new HashMap<>();


    @Getter
    private Text inventoryTitle;
    @Getter
    private FishingConfig fishingConfig;
    @Getter
    private LimitManager dropLimitManager;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        instance = this;
        this.dropLimitManager = new LimitManager(50000, "rsm_limit_stone");
        this.dropLimitManager.load();
        Pixelmon.EVENT_BUS.register(new PokemonDefeatListener());

        ApiPlugin.getInstance().loadConfig(this);
        ApiPlugin.getInstance().loadConfig(this.fishingConfig = new FishingConfig());
        game.getEventManager().registerListeners(this, new BlockBreakListener());
        game.getEventManager().registerListeners(this, new FishingListener());
        game.getEventManager().registerListeners(this, new StoneGeneratorListener());
        game.getEventManager().registerListeners(this, new JoinLeaveListener());
        ApiPlugin.getInstance().registerCommand(new DropCommand(this));
        ApiPlugin.getInstance().registerCommand(new DropLimitCommand(this));
    }

    @Override
    public String getConfigName() {
        return "Drop";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {
        try {
            dropLimitManager.setLimit(node.getNode("limit").getInt(50000));
            node.getNode("title").setValue(TextSerializers.FORMATTING_CODE.serialize(Text.of(TextColors.RED, "Drop ze stone")));
            node.getNode("blocked", "block").setValue(new TypeToken<List<BlockType>>() {
                                                      },
                    game.getRegistry().getAllOf(BlockType.class).stream().filter(t -> t.getId().contains("ore")).collect(Collectors.toList()));
            node.getNode("blocked", "pokemon_drops").setValue(new TypeToken<List<String>>() {
                                                              },
                    PixelmonItemsHeld.getHeldItemList().stream().filter(item -> item instanceof ItemMegaStone).map(i -> i.getRegistryName().getPath()).collect(Collectors.toList()));
            node.getNode("drop", "block", BlockTypes.STONE.getId()).setValue(new TypeToken<List<BlockDrop>>() {
            }, Arrays.asList(
                    new BlockDrop(20.0f, 5, 0, 255,
                            new DropItem(1, 3, ItemTypes.DIAMOND, 0, Text.of(TextColors.RED, "Testowy Diament"),
                                    Arrays.asList(Text.of("No to jest wlasnie"), Text.of("jakis testowy diament")),
                                    Arrays.asList(Enchantment.of(EnchantmentTypes.FIRE_ASPECT, 10),
                                            Enchantment.of(EnchantmentTypes.FORTUNE, 8270))),
                            Arrays.asList(ItemTypes.DIAMOND_PICKAXE, ItemTypes.GOLDEN_PICKAXE)),
                    new BlockDrop(20.0f, 5, 0, 255,
                            new DropItem(1, 3, ItemTypes.EMERALD, 0, Text.of(TextColors.BLUE, "Testowy EMERALD"),
                                    Collections.emptyList(), Collections.emptyList()),
                            Collections.emptyList())
            ));
            node.getNode("drop", "pokemons", EnumSpecies.Magikarp.name).setValue(new TypeToken<List<PokemonDrop>>() {
            }, Arrays.asList(
                    new PokemonDrop(50, 1, 1, 100,
                            new DropItem(1, 5, ItemTypes.EMERALD, 0, Text.of(TextColors.BLUE, "Testowy EMERALD"),
                                    Collections.emptyList(), Collections.emptyList()), Arrays.asList("day"), Arrays.asList("world"))
            ));

        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(CommentedConfigurationNode node) {
        blockDrops.clear();
        pokemonDrops.clear();
        dropSettingsMap.clear();
        this.inventoryTitle = TextSerializers.FORMATTING_CODE.deserialize(node.getNode("title").getString());
        node.getNode("drop", "block").getChildrenMap().forEach((name, obj) -> {
            final Optional<BlockType> type = game.getRegistry().getType(BlockType.class, (String) name);
            if (!type.isPresent()) {
                System.out.printf("Block id %s not found\r\n", name);
                return;
            }
            try {
                blockDrops.put(type.get(), obj.getList(TypeToken.of(BlockDrop.class)));
            } catch (ObjectMappingException e) {
                e.printStackTrace();
            }
        });
        node.getNode("drop", "pokemons").getChildrenMap().forEach((name, obj) -> {
            try {
                pokemonDrops.put((String) name, obj.getList(TypeToken.of(PokemonDrop.class)));
            } catch (ObjectMappingException e) {
                e.printStackTrace();
            }
        });
        blockDrops.forEach((blockType, blockDrops) -> blockDrops.sort(Comparator.comparingDouble((ToDoubleFunction<BlockDrop>) BlockDrop::getChance)));

        try {
            this.disabledBlocks = node.getNode("blocked", "block").getList(TypeToken.of(BlockType.class));
            this.disabledPokemonDrops = node.getNode("blocked", "pokemon_drops").getList(TypeToken.of(String.class));
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ConfigurationOptions getConfigurationOptions() {
        ConfigurationOptions defaults = ConfigurationOptions.defaults();
        defaults.acceptsType(BlockDrop.class);
        defaults.acceptsType(FishingDrop.class);
        defaults.acceptsType(DropItem.class);
        return defaults;
    }

    public List<BlockDrop> getDrop(BlockType type, Location<World> worldLocation, Player player, ItemStack tool) {
        final List<BlockDrop> blockDrops = this.blockDrops.get(type);
        List<BlockDrop> ret = new ArrayList<>();
        if (blockDrops == null) {
            return ret;
        }
        for (BlockDrop blockDrop : blockDrops) {
            if (RANDOM.nextDouble() * 100 <= (blockDrop.getChance()
                    + skillBonus(player)
                    + badgeBonus(player)) &&
                    worldLocation.getBlockY() >= blockDrop.getMinLevel() && worldLocation.getBlockY() <= blockDrop.getMaxLevel() &&
                    (blockDrop.getTools() == null || blockDrop.getTools().isEmpty() || blockDrop.getTools().contains(tool.getType()))) {
                ret.add(blockDrop);
            }
        }
        return ret;
    }

    public static float skillBonus(Player player) {
        return SkillManager.getBonus(player.getUniqueId(), Skill.DROP_CHANCE) * 100;
    }

    public static float badgeBonus(Player player) {
        return (BadgeManager.countBadges(player.getUniqueId()) >= 4 ? 0.1f : 0f);
    }

    public List<PokemonDrop> getPokemonDrop(EntityPixelmon pokemon, Player player) {
        final List<PokemonDrop> pokemonDrops = this.pokemonDrops.get(pokemon.getPokemonName());
        if(pokemon.isBossPokemon()){
            List<PokemonDrop> ret = new ArrayList<>();
            findPokemonDrops(pokemon, this.pokemonDrops.get("Boss" + pokemon.getBossMode().name()), ret);
            return ret;
        }
        final List<PokemonDrop> allDrops = this.pokemonDrops.get("all");
        List<PokemonDrop> ret = new ArrayList<>();
        findPokemonDrops(pokemon, pokemonDrops, ret);
        findPokemonDrops(pokemon, allDrops, ret);
        return ret;
    }

    private void findPokemonDrops(EntityPixelmon pokemon, List<PokemonDrop> allDrops, List<PokemonDrop> ret) {
        if (allDrops != null) {
            for (PokemonDrop pokemonDrop : allDrops) {
                if (pokemonDrop.getMinLevel() > pokemon.getPokemonData().getLevel() ||
                        pokemonDrop.getMaxLevel() < pokemon.getPokemonData().getLevel() ||
                        (pokemonDrop.getWorlds() != null && pokemonDrop.getWorlds().size() > 0 && !pokemonDrop.getWorlds().contains(pokemon.world.getWorldInfo().getWorldName())) ||
                        (pokemonDrop.getTime() != null && pokemonDrop.getTime().size() > 0 && !pokemonDrop.getTime().contains(TimeUtils.EnumServerTime.getTime(pokemon.getEntityWorld().getWorldTime()).name().toLowerCase()))) {
                    continue;
                }
                if (RANDOM.nextDouble() * 100 <= pokemonDrop.getChance()) {
                    ret.add(pokemonDrop);
                }
            }
        }
    }

    public DropSettings getSettings(Player player){
        return getSettings(player.getUniqueId());
    }
    public DropSettings getSettings(UUID player){
        if (!dropSettingsMap.containsKey(player)) {
            dropSettingsMap.put(player, new DropSettings());
        }
        return dropSettingsMap.get(player);
    }
}
