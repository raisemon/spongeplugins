package pl.raisemon.drop.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data

@ConfigSerializable
public class PokemonDrop {
    @Setting(value = "chance")
    private float chance;
    @Setting(value = "exp")
    private int exp;
    @Setting(value = "levelMin")
    private int minLevel = 0;
    @Setting(value = "levelMax")
    private int maxLevel = 100;
    @Setting(value = "item")
    private DropItem dropItem;
    @Setting(value = "time")
    private List<String> time;
    @Setting(value = "worlds")
    private List<String> worlds;
}
