package pl.raisemon.drop.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;

@AllArgsConstructor
@NoArgsConstructor
@Data

@ConfigSerializable
public class FishingDrop {
    @Setting(value = "chance")
    private float chance;
    @Setting(value = "exp")
    private int exp;
    @Setting(value = "item")
    private DropItem dropItem;
}
