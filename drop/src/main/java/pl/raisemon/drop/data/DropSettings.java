package pl.raisemon.drop.data;

import lombok.Getter;
import lombok.Setter;
import org.spongepowered.api.item.ItemType;

import java.util.ArrayList;
import java.util.List;

public class DropSettings {
    @Getter
    @Setter
    private boolean dropToInventory = false;
    @Getter
    @Setter
    private boolean dropCobble = true;
    private List<String> disabledDrops = new ArrayList<>();

    public boolean isActive(ItemType itemType) {
        return !disabledDrops.contains(itemType.getId());
    }

    public boolean toggle(ItemType itemType) {
        if(disabledDrops.contains(itemType.getId())){
            disabledDrops.remove(itemType.getId());
            return false;
        }
        disabledDrops.add(itemType.getId());
        return true;
    }
    public void disable(ItemType itemType) {
        disabledDrops.add(itemType.getId());
    }
    public void enable(ItemType itemType) {
        disabledDrops.remove(itemType.getId());
    }

    public void removeAll() {
        disabledDrops.clear();
    }
}
