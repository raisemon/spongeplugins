package pl.raisemon.drop.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.spongepowered.api.data.DataContainer;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import pl.raisemon.drop.DropPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ConfigSerializable
public class DropItem {
    @Setting(value = "amountMin")
    private int minAmount;
    @Setting(value = "amountMax")
    private int maxAmount;
    @Setting(value = "type")
    private ItemType type;
    @Setting(value = "data")
    private int data = 0;
    @Setting(value = "name")
    private Text name;
    @Setting(value = "lore")
    private List<Text> lore = new ArrayList<>();
    @Setting(value = "enchantments")
    private List<Enchantment> enchantments = new ArrayList<>();

    public ItemStack toItemStack() {
        ItemStack.Builder add = ItemStack.builder()
                .itemType(type)
                .add(Keys.ITEM_DURABILITY, data);
        if(data != 0){
            ItemStack itemStack = add.build();
            final DataContainer dataContainer = itemStack.toContainer();
            dataContainer.set(DataQuery.of("UnsafeDamage"), data);
            add = ItemStack.builder().fromContainer(dataContainer);
        }
        if (name != null) {
            add.add(Keys.DISPLAY_NAME, name);
        }
        if (lore != null) {
            add.add(Keys.ITEM_LORE, lore);
        }
        if (enchantments != null) {
            add.add(Keys.ITEM_ENCHANTMENTS, enchantments);
        }
        return add.quantity(DropPlugin.RANDOM.nextInt(maxAmount - minAmount + 1) + minAmount)
                .build();
    }

    public Text toReadableName() {
        if (getName() != null && !getName().isEmpty()) {
            return getName();
        }
        return Text.of(toItemStack().getTranslation().get(Locale.forLanguageTag("pl-PL")));
    }
}
