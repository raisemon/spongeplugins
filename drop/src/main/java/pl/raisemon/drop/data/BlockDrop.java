package pl.raisemon.drop.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.minecraft.util.text.TextFormatting;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import pl.raisemon.drop.DropPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@AllArgsConstructor
@NoArgsConstructor
@Data

@ConfigSerializable
public class BlockDrop {
    @Setting(value = "chance")
    private float chance;
    @Setting(value = "exp")
    private int exp;
    @Setting(value = "levelMin")
    private int minLevel;
    @Setting(value = "levelMax")
    private int maxLevel;
    @Setting(value = "item")
    private DropItem dropItem;
    @Setting(value = "tools")
    private List<ItemType> tools;

    public ItemStack getInventoryItemstack(DropSettings settings, Player player) {
        final ItemStack itemStack = dropItem.toItemStack();
        itemStack.setQuantity(1);
        List<Text> lore = getLore(settings, player);
        itemStack.offer(Keys.ITEM_LORE, lore);

        return itemStack;
    }

    public List<Text> getLore(DropSettings settings, Player player) {
        final List<Text> lore = new ArrayList<>();
        lore.add(Text.of());
        lore.add(Text.of(TextFormatting.BOLD, TextColors.YELLOW, "Ilość: ", TextFormatting.RESET, TextColors.RED, dropItem.getMinAmount(), dropItem.getMinAmount() != dropItem.getMaxAmount() ? Text.of(TextColors.GRAY, "-", TextColors.RED, dropItem.getMaxAmount()) : Text.of()));
        lore.add(Text.of(TextFormatting.BOLD, TextColors.YELLOW, "Szansa: ", TextFormatting.RESET, TextColors.RED, String.format("%.3f", this.chance), "%"));
        lore.add(Text.of(TextFormatting.BOLD, TextColors.YELLOW, "Bonus ze skilla: ", TextFormatting.RESET, TextColors.RED, String.format("%.3f", DropPlugin.skillBonus(player)), "%"));
        lore.add(Text.of(TextFormatting.BOLD, TextColors.YELLOW, "Bonus z odznak: ", TextFormatting.RESET, TextColors.RED, String.format("%.3f", DropPlugin.badgeBonus(player)), "%"));
        lore.add(Text.of(TextFormatting.BOLD, TextColors.YELLOW, "Exp: ", TextFormatting.RESET, TextColors.RED, exp, " pkt"));
        lore.add(Text.of(TextFormatting.BOLD, TextColors.YELLOW, "Wysokość: ", TextFormatting.RESET, TextColors.RED, minLevel, TextColors.GRAY, "-", TextColors.RED, maxLevel));
        lore.add(Text.of(TextFormatting.BOLD, TextColors.YELLOW, "Narzędzie: ", TextFormatting.RESET, TextColors.RED, (tools != null && tools.size() > 0) ? "" : "Dowolne"));
        if (tools != null) {
            for (ItemType tool : tools) {
                lore.add(Text.of(TextFormatting.BOLD, TextColors.YELLOW, " - ", TextFormatting.RESET, TextColors.RED, tool.getTranslation().get(Locale.forLanguageTag("pl-PL"))));
            }
        }
        lore.add(Text.of());
        lore.add(Text.of(TextFormatting.BOLD, TextColors.YELLOW, "Status: ", TextFormatting.RESET, settings.isActive(dropItem.getType()) ? Text.of(TextColors.GREEN, "ON") : Text.of(TextColors.RED, "OFF")));
        return lore;
    }
}
