package pl.raisemon.drop;

import com.google.common.reflect.TypeToken;
import lombok.Getter;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.weighted.WeightedTable;
import pl.raisemon.api.Configurable;
import pl.raisemon.drop.data.DropItem;
import pl.raisemon.drop.data.FishingDrop;
import pl.raisemon.mod.RegistrationHandler;
import pl.raisemon.mod.items.FishingRodItem;

import java.util.*;

public class FishingConfig implements Configurable {
    @Getter
    private Map<String, Map<Integer, WeightedTable<FishingDrop>>> fishingDrops = new HashMap<>();

    @Override
    public String getConfigName() {
        return "Fishing";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {
        try {
            node.getNode("drop", "fishing", "fishingrod_1", "skill0").setValue(new TypeToken<List<FishingDrop>>() {
            }, Arrays.asList(
                    new FishingDrop(60, 1, new DropItem(1, 5, ItemTypes.EMERALD, 0, Text.of(TextColors.BLUE, "Testowy EMERALD"),
                            Collections.emptyList(), Collections.emptyList()))
            ));
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(CommentedConfigurationNode node) {
        fishingDrops.clear();
        for (FishingRodItem fishingRod : RegistrationHandler.getFishingRods()) {
            CommentedConfigurationNode node1 = node.getNode("drop", "fishing", fishingRod.getRegistryName().getPath());
            Map<Integer, WeightedTable<FishingDrop>> drops = new HashMap<>();
            for (int i = 0; i <= 4; i++) {
                List<FishingDrop> list = null;
                CommentedConfigurationNode node2 = node1.getNode("skill" + i);
                if (!node2.isVirtual()) {
                    try {
                        list = node2.getList(TypeToken.of(FishingDrop.class));
                    } catch (ObjectMappingException e) {
                        e.printStackTrace();
                    }
                    WeightedTable<FishingDrop> tableEntries = new WeightedTable<>();
                    for (FishingDrop fishingDrop : list) {
                        tableEntries.add(fishingDrop, fishingDrop.getChance());
                    }
                    drops.put(i, tableEntries);

                } else {
                    drops.put(i, new WeightedTable<>());
                }
            }
            CommentedConfigurationNode node2 = node1.getNode("all");
            if(!node2.isVirtual()){
                List<FishingDrop> list = null;
                try {
                    list = node2.getList(TypeToken.of(FishingDrop.class));
                } catch (ObjectMappingException e) {
                    e.printStackTrace();
                }

                for (WeightedTable<FishingDrop> value : drops.values()) {
                    for (FishingDrop fishingDrop : list) {
                        value.add(fishingDrop, fishingDrop.getChance());
                    }
                }
            }

            this.fishingDrops.put(fishingRod.getRegistryName().toString(), drops);
        }
    }
}
