package pl.raisemon.drop.listeners;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.Item;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.gamemode.GameModes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.filter.IsCancelled;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.item.inventory.DropItemEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.entity.PlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.item.inventory.transaction.InventoryTransactionResult;
import org.spongepowered.api.util.Tristate;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.drop.DropPlugin;
import pl.raisemon.drop.data.BlockDrop;
import pl.raisemon.drop.data.DropSettings;
import pl.raisemon.mod.skill.SkillManager;
import pl.raisemon.mod.skills.Skill;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class BlockBreakListener {
    @Listener
    public void onDrop(DropItemEvent.Destruct event, @First Player player, @First BlockSnapshot snapshot) {
        if (snapshot.getState().getType().equals(BlockTypes.STONE)) {
            if (!DropPlugin.getInstance().getSettings(player).isDropCobble()) {
                event.getEntities().removeIf(entity -> entity instanceof Item
                                && (
                                ItemTypes.COBBLESTONE.equals(((Item) entity).getItemType()) ||
                                        ItemTypes.STONE.equals(((Item) entity).getItemType())
                        )
                );
            }
        }
    }

    @Listener(order = Order.LAST)
    @IsCancelled(value = Tristate.FALSE)
    public void onBlockBreak(ChangeBlockEvent.Break event, @First Player player) {
        if (player.gameMode().get() == GameModes.CREATIVE) {
            return;
        }
        final DropSettings settings = DropPlugin.getInstance().getSettings(player);
        for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
            final Optional<Location<World>> location = transaction.getOriginal().getLocation();
            if (DropPlugin.getInstance().getDisabledBlocks().contains(transaction.getOriginal().getState().getType())) {
                transaction.setValid(false);
                location.ifPresent(l ->
                        Sponge.getScheduler().createTaskBuilder().delayTicks(1).execute(task -> {
                            l.setBlock(BlockState.builder().blockType(BlockTypes.AIR).build());
                        }).submit(DropPlugin.getInstance())
                );
                MessageUtils.sendMessage(player, "DROP", "drop.block.disabled");
                continue;
            }
            if(DropPlugin.getInstance().getBlockDrops().containsKey(transaction.getOriginal().getState().getType()) &&
                    DropPlugin.getInstance().getDropLimitManager().incrAmount(player)) {
                final List<BlockDrop> drop = DropPlugin.getInstance().getDrop(transaction.getOriginal().getState().getType(),
                        location.get(),
                        player, player.getItemInHand(HandTypes.MAIN_HAND).orElse(null));
                drop.forEach(blockDrop -> {
                    if (!settings.isActive(blockDrop.getDropItem().getType())) {
                        return;
                    }
                    Collection<ItemStackSnapshot> rejectedItems = new ArrayList<>();
                    ItemStack itemStack = blockDrop.getDropItem().toItemStack();
                    //Fortuna 1	1-2
                    //Fortuna 2	1-3
                    //Fortuna 3	2-3
                    //Fortuna 4	2-4
                    switch (SkillManager.getBonus(player.getUniqueId(), Skill.FORTUNE)) {
                        default:
                        case 0:
                            break;
                        case 1:
                            if (DropPlugin.RANDOM.nextBoolean()) {
                                itemStack.setQuantity(itemStack.getQuantity() * 2);
                            }
                            break;
                        case 2:
                            itemStack.setQuantity(itemStack.getQuantity() * DropPlugin.RANDOM.nextInt(3));
                            break;
                        case 3:
                            itemStack.setQuantity(itemStack.getQuantity() * (1 + DropPlugin.RANDOM.nextInt(2)));
                            break;
                        case 4:
                            itemStack.setQuantity(itemStack.getQuantity() * (1 + DropPlugin.RANDOM.nextInt(3)));
                            break;
                    }
                    if (settings.isDropToInventory()) {
                        PlayerInventory query = player.getInventory()
                                .query(QueryOperationTypes.INVENTORY_TYPE.of(PlayerInventory.class));
                        InventoryTransactionResult offer = query.getMain().offer(itemStack);
                        rejectedItems = offer.getRejectedItems();
                    } else {
                        rejectedItems.add(itemStack.createSnapshot());
                    }
                    for (ItemStackSnapshot rejectedItem : rejectedItems) {
                        final Location<World> worldLocation = location.orElse(player.getLocation());
                        final Entity item = worldLocation.createEntity(EntityTypes.ITEM);
                        item.offer(Keys.REPRESENTED_ITEM, rejectedItem);
                        worldLocation.spawnEntity(item);
                    }
                    player.offer(Keys.TOTAL_EXPERIENCE, player.get(Keys.TOTAL_EXPERIENCE).orElse(0) + blockDrop.getExp());
                });
            }
        }
    }
}
