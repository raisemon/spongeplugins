package pl.raisemon.drop.listeners;

import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.DestructEntityEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;

public class JoinLeaveListener {
    @Listener
    public void onJoin(ClientConnectionEvent.Join event){
        event.setMessageCancelled(true);
    }
    @Listener
    public void onJoin(ClientConnectionEvent.Disconnect event){
        event.setMessageCancelled(true);
    }
    @Listener
    public void onEntityDeath(DestructEntityEvent.Death event) {
        // Some logic here
        event.setMessageCancelled(true);
        event.setKeepInventory(true);
    }
}
