package pl.raisemon.drop.listeners;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.drop.DropPlugin;
import pl.raisemon.mod.RegistrationHandler;

public class StoneGeneratorListener {
    @Listener(order = Order.LAST)
    public void onBlockPlace(ChangeBlockEvent.Place event){
        for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
            if(transaction.getDefault().getState().getType().getId()
                    .equals(RegistrationHandler.STONE_GENERATOR.getRegistryName().toString())){
                Location<World> add = transaction.getDefault().getLocation().get().add(0, 1, 0);
                if(add.getBlockType().equals(BlockTypes.AIR)){
                    Sponge.getScheduler().createTaskBuilder().delayTicks(5).execute(()->
                            add.setBlockType(BlockTypes.STONE)).submit(DropPlugin.getInstance());
                }
            }
        }
    }
    @Listener(order = Order.LAST)
    public void onBlockBreak(ChangeBlockEvent.Break event){
        for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
            Location<World> worldLocation = transaction.getOriginal().getLocation().get();
            if(transaction.getOriginal().getState().getType().equals(BlockTypes.STONE)){
                if(transaction.getOriginal().getLocation().isPresent() && worldLocation.sub(0, 1, 0).getBlockType().getId().equals(RegistrationHandler.STONE_GENERATOR.getRegistryName().toString())){

                    Sponge.getScheduler().createTaskBuilder().delayTicks(40).execute(()->worldLocation.setBlockType(BlockTypes.STONE)).submit(DropPlugin.getInstance());
                }
            }
        }
    }
}
