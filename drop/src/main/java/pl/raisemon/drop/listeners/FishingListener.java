package pl.raisemon.drop.listeners;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.action.FishingEvent;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.cause.EventContext;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import pl.raisemon.drop.DropPlugin;
import pl.raisemon.drop.data.FishingDrop;
import pl.raisemon.drop.events.ItemsFishedOutEvent;
import pl.raisemon.mod.RaisemonMod;
import pl.raisemon.mod.items.FishingRodItem;
import pl.raisemon.mod.skill.SkillManager;
import pl.raisemon.mod.skills.Skill;

import java.util.List;
import java.util.Optional;

public class FishingListener {
    @Listener
    public void onFishing(FishingEvent.Stop event, @First Player player){
        if(event.getTransactions().size()>0) {
            event.getTransactions().clear();
            ItemStack rod = null;
            Optional<ItemStack> mainHand = player.getItemInHand(HandTypes.MAIN_HAND);
            Optional<ItemStack> offHand = player.getItemInHand(HandTypes.OFF_HAND);
            if(mainHand.isPresent() && mainHand.get().getType() instanceof FishingRodItem){
                rod = mainHand.get();
            }
            if(rod == null && offHand.isPresent() && offHand.get().getType()instanceof FishingRodItem){
                rod = offHand.get();
            }
            if(rod == null){
                return;
            }

            List<FishingDrop> fishingDrops = DropPlugin.getInstance().getFishingConfig().getFishingDrops()
                    .get(rod.getType().getId())
                    .get(Skill.FISHING_LEVEL.getBonusFunction().apply(
                            SkillManager.getLevel(player.getUniqueId(), Skill.FISHING_LEVEL)))
                    .get(RaisemonMod.RAND);
            for (FishingDrop fishingDrop : fishingDrops) {
                ItemStackSnapshot snapshot = fishingDrop.getDropItem().toItemStack().createSnapshot();
                event.getTransactions().add(new Transaction<>(snapshot,snapshot));
            }
            if(fishingDrops.size() > 0){
                Sponge.getEventManager().post(new ItemsFishedOutEvent(Cause.builder().append(player).append(rod).build(EventContext.builder().build()),fishingDrops));
            }
        }
    }
}
