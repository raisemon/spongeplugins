package pl.raisemon.drop.listeners;

import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.entities.pixelmon.drops.DroppedItem;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.drop.DropPlugin;
import pl.raisemon.drop.data.PokemonDrop;

import java.util.ArrayList;
import java.util.Objects;

public class PokemonDefeatListener {

    @SubscribeEvent
    public void onPokemonDefeat(com.pixelmonmod.pixelmon.api.events.DropEvent event){
        if(!event.isPokemon()){
            return;
        }
        EntityPixelmon pokemon = (EntityPixelmon) event.entity;
        if(pokemon.isBossPokemon() || !pokemon.getEntityWorld().getWorldInfo().getWorldName().equalsIgnoreCase("world")){
            for (DroppedItem drop : event.getDrops()) {
                event.removeDrop(drop);
            }
        }
        for (DroppedItem drop : event.getDrops()) {
            if(drop.itemStack.getItem().getRegistryName().toString().contains("_z")){
                event.removeDrop(drop);
                continue;
            }
            if(DropPlugin.getInstance().getDisabledPokemonDrops().contains(drop.itemStack.getItem().getRegistryName().getPath())){
                event.removeDrop(drop);
            }
        }
        for (PokemonDrop pokemonDrop : DropPlugin.getInstance().getPokemonDrop(pokemon, (Player) event.player)) {
            MessageUtils.sendMessage((Player)event.player, "DROP", "drop.pokemon.item", TextSerializers.FORMATTING_CODE.serialize(pokemonDrop.getDropItem().toReadableName()));

            final org.spongepowered.api.item.inventory.ItemStack stack = pokemonDrop.getDropItem().toItemStack();
            net.minecraft.item.ItemStack itemStack = new net.minecraft.item.ItemStack(Objects.requireNonNull(Item.getByNameOrId(stack.getType().getId())));
            itemStack.setCount(stack.getQuantity());
            itemStack.setItemDamage(stack.getOrElse(Keys.ITEM_DURABILITY, 0));
            for (org.spongepowered.api.item.enchantment.Enchantment enchantment : stack.getOrElse(Keys.ITEM_ENCHANTMENTS, new ArrayList<>())) {
                itemStack.addEnchantment(Objects.requireNonNull(Enchantment.getEnchantmentByLocation(enchantment.getType().getId())), enchantment.getLevel());
            }

            if(pokemonDrop.getDropItem().getName() != null&&pokemonDrop.getDropItem().getName().toPlain().length()>0){
                itemStack.setStackDisplayName(TextSerializers.FORMATTING_CODE.serialize(pokemonDrop.getDropItem().getName()));
            }
            if(pokemonDrop.getDropItem().getLore() != null && pokemonDrop.getDropItem().getLore().size()>0){
                NBTTagList list = new NBTTagList();
                pokemonDrop.getDropItem().getLore().stream().map(TextSerializers.FORMATTING_CODE::serialize).forEach(t -> list.appendTag(new NBTTagString(t)));
                itemStack.getOrCreateSubCompound("display").setTag("Lore", list);
            }
            event.addDrop(itemStack);
            event.player.experienceTotal += pokemonDrop.getExp();
        }
    }
}
