package pl.raisemon.drop;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.spongepowered.api.entity.living.player.Player;
import pl.raisemon.api.DailyDatabaseTableUtils;
import pl.raisemon.api.utils.DatabaseUtil;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LimitManager {
    @Getter
    @Setter
    private int limit;
    private String tableName;

    private final Map<UUID, LimitData> limits = new HashMap<>();

    public LimitManager(int limit, String tableName) {
        this.limit = limit;
        this.tableName = tableName;
        DatabaseUtil.execute("CREATE TABLE IF NOT EXISTS `" + tableName + "` (" +
                "`id` INT NOT NULL AUTO_INCREMENT," +
                "`uuid` varchar(36) NOT NULL UNIQUE," +
                "`val` INT NOT NULL," +
                "`ts` INT NOT NULL," +
                "PRIMARY KEY (`id`)" +
                ");");
    }


    public void load() {
        DatabaseUtil.query("SELECT * FROM `" + tableName + "`;", results -> {
            try {
                while (results.next()) {
                    UUID player = UUID.fromString(results.getString("uuid"));
                    LimitData data = new LimitData(player);
                    data.setTimeStamp(results.getInt("ts"));
                    data.setValue(results.getInt("val"));
                    this.limits.put(player, data);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    public int getAmount(Player player) {
        return getAmount(player.getUniqueId());
    }


    public int getAmount(UUID uuid) {
        LimitData data = getData(uuid);
        if (data.getTimeStamp() != DailyDatabaseTableUtils.getTodayStamp()) {
            data.setTimeStamp(DailyDatabaseTableUtils.getTodayStamp());
            data.setValue(0);
            updateUser(uuid);
        }
        return data.getValue();
    }

    public LimitData getData(Player player) {
        return getData(player.getUniqueId());
    }

    public LimitData getData(UUID uuid) {
        limits.putIfAbsent(uuid, new LimitData(uuid));
        return limits.get(uuid);
    }

    public boolean incrAmount(Player player) {
        return incrAmount(player.getUniqueId());
    }

    public boolean incrAmount(UUID uuid) {
        int amount = getAmount(uuid);
        if (amount >= limit) {
            return false;
        }
        LimitData data = getData(uuid);
        data.incrValue();
        updateUser(uuid);
        return true;
    }

    private void updateUser(UUID uuid) {
        LimitData data = getData(uuid);
        DatabaseUtil.execute("INSERT INTO " + tableName + " (`uuid`, `val`, `ts`) " +
                "VALUES ('" + uuid.toString() + "', " + data.getValue() + ", " + data.getTimeStamp() +
                ") ON DUPLICATE KEY UPDATE `val`=VALUES(`ts`), `val`=VALUES(`ts`);");
    }

    @Data
    private class LimitData {
        private UUID player;
        private int timeStamp;
        private int value;

        public LimitData(UUID player) {
            this.player = player;
            this.timeStamp = DailyDatabaseTableUtils.getTodayStamp();
            this.value = 0;
        }

        public void incrValue() {
            this.value++;
        }
    }
}
