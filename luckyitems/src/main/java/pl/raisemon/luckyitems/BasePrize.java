package pl.raisemon.luckyitems;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public interface BasePrize {
    void givePlayer(Player player, Location<World> worldLocation);
    double getChance();
}
