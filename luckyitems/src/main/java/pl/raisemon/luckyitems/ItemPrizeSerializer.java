package pl.raisemon.luckyitems;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ItemPrizeSerializer implements TypeSerializer<ItemPrize> {
    @Override
    public ItemPrize deserialize( TypeToken<?> type,  ConfigurationNode value) throws ObjectMappingException {
        double chance = value.getNode("chance").getDouble();
        byte data = (byte) value.getNode("data").getInt(0);
        ItemType item = Sponge.getRegistry().getType(ItemType.class, value.getNode("item").getString()).orElse(null);
        if(item == null){
            System.out.println(value.getNode("item").getString());
        }
        Text name = Text.of();
        if(!value.getNode("name").isVirtual()){
            name = TextSerializers.FORMATTING_CODE.deserialize(value.getNode("name").getString());
        }
        List<Text> lore = new ArrayList<>();
        if(!value.getNode("lore").isVirtual()){
            lore = value.getNode("lore").getList(TypeToken.of(String.class)).stream().map(a->TextSerializers.FORMATTING_CODE.deserialize(a)).collect(Collectors.toList());
        }
        int amount = value.getNode("amount").getInt(1);
        boolean always = false;
        if(!value.getNode("always").isVirtual()){
            always = value.getNode("always").getBoolean();
        }
        return new ItemPrize(item, chance, data, name, lore, amount, always);
    }

    @Override
    public void serialize( TypeToken<?> type,  ItemPrize obj,  ConfigurationNode value) throws ObjectMappingException {
        value.getNode("chance").setValue(obj.getChance());
        value.getNode("item").setValue(obj.getItemType().getName());
        value.getNode("data").setValue(obj.getData());
        if(obj.getName().toPlain().length()>0) {
            value.getNode("name").setValue(TextSerializers.FORMATTING_CODE.serialize(obj.getName()));
        }
        if(obj.getLore().size()>0) {
            value.getNode("lore").setValue(obj.getLore().stream().map(TextSerializers.FORMATTING_CODE::serialize).collect(Collectors.toList()));
        }
        if(obj.isAlways()){
            value.getNode("always").setValue(true);
        }
        value.getNode("amount").setValue(obj.getAmount());
    }
}
