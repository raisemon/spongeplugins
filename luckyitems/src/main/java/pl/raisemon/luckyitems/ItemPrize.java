package pl.raisemon.luckyitems;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.CauseStackManager;
import org.spongepowered.api.event.cause.EventContextKeys;
import org.spongepowered.api.event.cause.entity.spawn.SpawnTypes;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.entity.MainPlayerInventory;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.item.inventory.transaction.InventoryTransactionResult;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.utils.MessageUtils;

import java.util.List;
import java.util.Locale;

@Data
@AllArgsConstructor
public class ItemPrize implements BasePrize {
    private ItemType itemType;
    private double chance;
    private byte data;
    private Text name;
    private List<Text> lore;
    private int amount;
    private boolean always;

    @Override
    public void givePlayer(Player player, Location<World> worldLocation) {


        final ItemStack.Builder builder = ItemStack.builder();
        builder.itemType(itemType);
        builder.quantity(amount);
        if (getName() != null && getName().toPlain().length() > 0) {
            builder.add(Keys.DISPLAY_NAME, getName());
        }
        if (getLore().size() > 0) {
            builder.add(Keys.ITEM_LORE, getLore());
        }
        final InventoryTransactionResult offer = player.getInventory().query(QueryOperationTypes.INVENTORY_TYPE.of(MainPlayerInventory.class)).offer(builder.build());
        for (ItemStackSnapshot rejectedItem : offer.getRejectedItems()) {
            Entity entity = worldLocation.getExtent().createEntity(EntityTypes.ITEM, worldLocation.getBlockPosition());

            entity.offer(Keys.REPRESENTED_ITEM, rejectedItem);
            try (CauseStackManager.StackFrame frame = Sponge.getCauseStackManager().pushCauseFrame()) {
                frame.addContext(EventContextKeys.SPAWN_TYPE, SpawnTypes.CUSTOM);
                worldLocation.getExtent().spawnEntity(entity);
            }
        }


        if(isAlways()){
            return;
        }
        for (Player player1 : Sponge.getServer().getOnlinePlayers()) {
            MessageUtils.sendMessage(player1, "LE", "luckyitems.luckyeggWinItem", player.getName(), itemType.getTranslation().get(new Locale.Builder().setLanguage("pl").setRegion("PL").build()));
        }
    }
}
