package pl.raisemon.luckyitems;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import lombok.Getter;
import net.minecraft.init.Enchantments;
import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Game;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.item.inventory.InteractItemEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.enchantment.Enchantment;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.Configurable;
import pl.raisemon.api.utils.MessageUtils;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Plugin(id = "raisemon_luckyitems",
        name = "Raisemon LuckyItems",
        version = "1.0",
        description = "LuckyItems",
        dependencies = {@Dependency(id = "raisemon_api"), @Dependency(id = "pixelmon")})
public class LuckyItemsPlugin implements Configurable {
    @Inject
    private Logger logger;
    @Inject
    private Game game;
    @Getter
    private static LuckyItemsPlugin instance;

    private Map<String, List<BasePrize>> prizes = new HashMap<>();
    private Map<String, Double> maxVal = new HashMap<>();
    private Map<String, List<BasePrize>> pursePrizes = new HashMap<>();
    private Map<String, Double> purseMaxVal = new HashMap<>();

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
    }

    @Listener
    public void onInit(GameInitializationEvent event) {
        ApiPlugin.getInstance().loadConfig(this);
    }

    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        instance = this;
    }

    @Override
    public String getConfigName() {
        return "LuckyItems";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {
        try {
            node.getNode("luckyblocks", "raisemonmod:luckyegg_water", "pokemon").setValue(new TypeToken<List<PokemonPrize>>() {
            }, Arrays.asList(
                    new PokemonPrize("Eevee", 50, 70, 100),
                    new PokemonPrize("Eevee s", 50, 90, 100)
            ));
            node.getNode("luckyblocks", "raisemonmod:luckyegg_water", "item").setValue(new TypeToken<List<ItemPrize>>() {
            }, Arrays.asList(
                    new ItemPrize(ItemTypes.DIAMOND, 50, (byte)0, Text.of(), Arrays.asList(Text.of("opis")), 2, false),
                    new ItemPrize(ItemTypes.GOLD_BLOCK, 50, (byte)0, Text.of("bloczek zlota"), Collections.emptyList(), 2, false)
            ));
            node.getNode("luckypurses", "raisemonmod:ivspack", "item").setValue(new TypeToken<List<ItemPrize>>() {
            }, Arrays.asList(
                    new ItemPrize(ItemTypes.DIAMOND, 50, (byte)0, Text.of(), Arrays.asList(Text.of("opis")), 2, false),
                    new ItemPrize(ItemTypes.GOLD_BLOCK, 50, (byte)0, Text.of("bloczek zlota"), Collections.emptyList(), 2, false)
            ));
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void load(CommentedConfigurationNode node) {
        prizes.clear();
        pursePrizes.clear();
        maxVal.clear();
        purseMaxVal.clear();
        node.getNode("luckyblocks").getChildrenMap().forEach((BiConsumer<Object, CommentedConfigurationNode>) (o, commentedConfigurationNode) -> {
            ArrayList<BasePrize> objects = new ArrayList<>();
            try {
                if (!commentedConfigurationNode.getNode("pokemon").isVirtual()) {
                    objects.addAll(commentedConfigurationNode.getNode("pokemon").getList(TypeToken.of(PokemonPrize.class)));
                }
                if (!commentedConfigurationNode.getNode("item").isVirtual()) {
                    objects.addAll(commentedConfigurationNode.getNode("item").getList(TypeToken.of(ItemPrize.class)));
                }
            } catch (ObjectMappingException e) {
                e.printStackTrace();
            }
            prizes.put((String) o, objects);
            double value = 0;
            for (BasePrize object : objects) {
                if (object instanceof ItemPrize && ((ItemPrize) object).isAlways()) {
                    continue;
                }
                value += object.getChance();
            }
            maxVal.put((String) o, value);
        });
        node.getNode("luckypurses").getChildrenMap().forEach((BiConsumer<Object, CommentedConfigurationNode>) (o, commentedConfigurationNode) -> {
            ArrayList<BasePrize> objects = new ArrayList<>();
            try {
                if (!commentedConfigurationNode.getNode("pokemon").isVirtual()) {
                    objects.addAll(commentedConfigurationNode.getNode("pokemon").getList(TypeToken.of(PokemonPrize.class)));
                }
                if (!commentedConfigurationNode.getNode("item").isVirtual()) {
                    objects.addAll(commentedConfigurationNode.getNode("item").getList(TypeToken.of(ItemPrize.class)));
                }
            } catch (ObjectMappingException e) {
                e.printStackTrace();
            }
            pursePrizes.put((String) o, objects);
            double value = 0;
            for (BasePrize object : objects) {
                if (object instanceof ItemPrize && ((ItemPrize) object).isAlways()) {
                    continue;
                }
                value += object.getChance();
            }
            purseMaxVal.put((String) o, value);
        });
    }

    @Override
    public ConfigurationOptions getConfigurationOptions() {
        final HashSet<Class<?>> objects = new HashSet<>();
        objects.add(PokemonPrize.class);
        objects.add(ItemPrize.class);
        final ConfigurationOptions configurationOptions = ConfigurationOptions.defaults()
                .setAcceptedTypes(objects);
        configurationOptions.getSerializers()
                .registerType(TypeToken.of(PokemonPrize.class), new PokemonPrizeSerializer())
                .registerType(TypeToken.of(ItemPrize.class), new ItemPrizeSerializer());
        return configurationOptions;
    }


    private final Map<UUID, Long> timeout = new HashMap<>();
    private final Random RANDOM = new Random();

    @Listener(order = Order.LAST)
    public void onLuckyblockBreak(ChangeBlockEvent.Break event, @First Player player) {

        if (timeout.containsKey(player.getUniqueId()) && System.currentTimeMillis() - timeout.get(player.getUniqueId()) < 3000) {
            MessageUtils.sendMessage(player, "LE", "luckyitems.luckyeggTimeout");
            event.setCancelled(true);
            return;
        }
        Optional<ItemStack> itemInHand1 = player.getItemInHand(HandTypes.MAIN_HAND);
        if (itemInHand1.isPresent()) {
            final Optional<List<Enchantment>> enchantments = itemInHand1.get().get(Keys.ITEM_ENCHANTMENTS);
            if (enchantments.isPresent()) {
                for (Enchantment itemEnchantment : enchantments.get()) {
                    if (itemEnchantment.getType().equals(Enchantments.SILK_TOUCH)) {
                        return;
                    }
                }
            }
        }
        for (Transaction<BlockSnapshot> blockSnapshotTransaction : event.getTransactions()) {
            if (!blockSnapshotTransaction.isValid()) {
                continue;
            }
            String name = blockSnapshotTransaction.getOriginal().getState().getType().getName();
            if (!prizes.containsKey(name)) {
                continue;
            }
            Location<World> worldLocation = blockSnapshotTransaction.getOriginal().getLocation().get();


            List<BasePrize> luckyBlockPrizes = prizes.get(name).stream().filter(p -> !(p instanceof ItemPrize) || !((ItemPrize) p).isAlways()).collect(Collectors.toList());
            prizes.get(name).stream().filter(p -> p instanceof ItemPrize && ((ItemPrize) p).isAlways()).forEach(p -> p.givePlayer(player, worldLocation));
            BasePrize prize = null;
            timeout.put(player.getUniqueId(), System.currentTimeMillis());
            double rand = RANDOM.nextDouble() * maxVal.get(name);
            double last = 0;
            for (BasePrize luckyBlockPrize : luckyBlockPrizes) {
                if (last + luckyBlockPrize.getChance() > rand) {
                    prize = luckyBlockPrize;
                    break;
                }
                last += luckyBlockPrize.getChance();
            }
            if (prize == null)
                continue;
            prize.givePlayer(player, worldLocation);
        }
    }

    @Listener(order = Order.LAST)
    public void onInteract(InteractItemEvent.Secondary.MainHand event, @First Player player) {
        ItemStack itemStack = player.getItemInHand(HandTypes.MAIN_HAND).orElse(null);
        if (itemStack == null) {
            return;
        }
        String name = itemStack.getType().getName();
        if (!pursePrizes.containsKey(name)) {
            return;
        }
        if (timeout.containsKey(player.getUniqueId()) && System.currentTimeMillis() - timeout.get(player.getUniqueId()) < 3000) {
            MessageUtils.sendMessage(player, "LE", "luckyitems.purseTimeout");
            event.setCancelled(true);
            return;
        }
        List<BasePrize> luckyBlockPrizes = pursePrizes.get(name).stream().filter(p -> !(p instanceof ItemPrize) || !((ItemPrize) p).isAlways()).collect(Collectors.toList());
        pursePrizes.get(name).stream().filter(p -> p instanceof ItemPrize && ((ItemPrize) p).isAlways()).forEach(p -> p.givePlayer(player, player.getLocation()));

        BasePrize prize = null;
        timeout.put(player.getUniqueId(), System.currentTimeMillis());
        double rand = RANDOM.nextDouble() * purseMaxVal.get(name);
        double last = 0;
        for (BasePrize luckyBlockPrize : luckyBlockPrizes) {
            if (last + luckyBlockPrize.getChance() > rand) {
                prize = luckyBlockPrize;
                break;
            }
            last += luckyBlockPrize.getChance();
        }
        if (prize == null)
            return;
        itemStack.setQuantity(itemStack.getQuantity() - 1);
        prize.givePlayer(player, player.getLocation());
    }
}
