package pl.raisemon.luckyitems;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.RandomHelper;
import com.pixelmonmod.pixelmon.advancements.PixelmonAdvancements;
import com.pixelmonmod.pixelmon.api.pokemon.Pokemon;
import com.pixelmonmod.pixelmon.api.pokemon.PokemonSpec;
import com.pixelmonmod.pixelmon.battles.BattleRegistry;
import com.pixelmonmod.pixelmon.entities.pixelmon.stats.IVStore;
import com.pixelmonmod.pixelmon.enums.EnumBossMode;
import com.pixelmonmod.pixelmon.enums.EnumSpecies;
import com.pixelmonmod.pixelmon.enums.forms.EnumAlolan;
import com.pixelmonmod.pixelmon.enums.items.EnumPokeballs;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.minecraft.entity.player.EntityPlayerMP;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.utils.MessageUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Data
@AllArgsConstructor
public class PokemonPrize implements BasePrize {
    private String arg;
    private double chance;
    private int minIvsPercent;
    private int maxIvsPercent;

    @Override
    public void givePlayer(Player p, Location<World> worldLocation) {
        EntityPlayerMP player = (EntityPlayerMP) p;
        PokemonSpec from = PokemonSpec.from(arg.split(" "));
        from.boss = (byte)EnumBossMode.NotBoss.index;
        Pokemon pokemon = Pixelmon.pokemonFactory.create(from);

        if (EnumSpecies.legendaries.contains(from.name)) {
            if (minIvsPercent != 0 || maxIvsPercent != 100) {
                int[] ivs = new int[6];
                int[] maxIVs = RandomHelper.getRandomDistinctNumbersBetween(0, 5, 3);
                Arrays.sort(maxIVs);
                int maxIVCounter = 0;

                int maxIvsSum = RandomHelper.getRandomNumberBetween(minIvsPercent * 93 / 100, maxIvsPercent * 93 / 100);
                Integer[] n = getRandomNumWithSum(3, maxIvsSum);

                List<Integer> nL = Arrays.asList(n);
                Collections.shuffle(nL);
                int z = 0;

                for (int i = 0; i < 6; ++i) {
                    if (maxIVs[maxIVCounter] == i) {
                        ivs[i] = 31;
                        if (maxIVCounter < maxIVs.length - 1) {
                            ++maxIVCounter;
                        }
                    } else {
                        ivs[i] = nL.get(z);
                        z++;
                    }
                }
                pokemon.getStats().ivs.CopyIVs(new IVStore(ivs));
            }
        } else {
            if (minIvsPercent != 0 || maxIvsPercent != 100) {
                IVStore iv = new IVStore();
                int maxIvsSum = RandomHelper.getRandomNumberBetween(minIvsPercent * 186 / 100, maxIvsPercent * 186 / 100);
                Integer[] n = getRandomNumWithSum(6, maxIvsSum);
                List<Integer> nL = Arrays.asList(n);
                Collections.shuffle(nL);
                iv.specialDefence = nL.get(0);
                iv.specialAttack = nL.get(1);
                iv.speed = nL.get(2);
                iv.defence = nL.get(3);
                iv.attack = nL.get(4);
                iv.hp = nL.get(5);
                pokemon.getStats().ivs.CopyIVs(iv);
            }
        }


        if(pokemon == null){
            System.out.println("Zly config pokemona! Arg:" + arg);
            return;
        }

        if (pokemon.getCaughtBall() == null) {
            pokemon.setCaughtBall(EnumPokeballs.PokeBall);
        }

        if (BattleRegistry.getBattle(player) == null) {

            Pixelmon.storageManager.getParty(player).add(pokemon);
        } else {
            Pixelmon.storageManager.getPCForPlayer(player).add(pokemon);
        }


        PixelmonAdvancements.POKEDEX_TRIGGER.trigger(player);
        String playerName = player.getGameProfile().getName();
        String pokemonName = pokemon.getDisplayName();
        if(pokemon.isShiny()){
            pokemonName = "Shiny " + pokemonName;
        }
        if(pokemon.getFormEnum() instanceof EnumAlolan && pokemon.getForm() == EnumAlolan.ALOLAN.getForm()){
            pokemonName = "Alolan " + pokemonName;
        }
        for (Player player1 : Sponge.getServer().getOnlinePlayers()) {
            MessageUtils.sendMessage(player1, "LE", "luckyitems.luckyeggWinPokemon", playerName, pokemonName);
        }
    }

    private Integer[] getRandomNumWithSum(int i, int maxIvsSum) {
        final Integer[] integers = new Integer[i];
        int n = 0;
        while(maxIvsSum>0){
            if(integers[n] == null){
                integers[n] = 0;
            }
            integers[n] += Math.min(31-integers[n], RandomHelper.getRandomNumberBetween(0, Math.min(31, maxIvsSum)));
            maxIvsSum -= integers[n];
            n++;
            if(n>=i){
                n=0;
            }
        }
        return integers;
    }
}
