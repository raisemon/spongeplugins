package pl.raisemon.luckyitems;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

public class PokemonPrizeSerializer implements TypeSerializer<PokemonPrize> {
    @Override
    public PokemonPrize deserialize(TypeToken<?> typeToken, ConfigurationNode configurationNode) throws ObjectMappingException {
        String arg = configurationNode.getNode("arg").getString();
        double chance = configurationNode.getNode("chance").getDouble();
        int minIvs = configurationNode.getNode("ivs", "min").getInt(0);
        int maxIvs = configurationNode.getNode("ivs", "max").getInt(100);
        return new PokemonPrize(arg, chance, minIvs, maxIvs);
    }

    @Override
    public void serialize(TypeToken<?> typeToken, PokemonPrize t, ConfigurationNode configurationNode) throws ObjectMappingException {
        configurationNode.getNode("arg").setValue(t.getArg());
        configurationNode.getNode("chance").setValue(t.getChance());
        configurationNode.getNode("ivs", "min").setValue(t.getMinIvsPercent());
        configurationNode.getNode("ivs", "max").setValue(t.getMaxIvsPercent());
    }
}
