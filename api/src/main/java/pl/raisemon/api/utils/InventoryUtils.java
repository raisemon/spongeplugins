package pl.raisemon.api.utils;

import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class InventoryUtils {
    public static void giveItem(Player player, ItemStack build) {
        for (ItemStackSnapshot rejectedItem : player.getInventory().offer(build).getRejectedItems()) {
            final Location<World> worldLocation = player.getLocation();
            final Entity item = worldLocation.createEntity(EntityTypes.ITEM);
            item.offer(Keys.REPRESENTED_ITEM, rejectedItem);
            worldLocation.spawnEntity(item);
        }

    }
}
