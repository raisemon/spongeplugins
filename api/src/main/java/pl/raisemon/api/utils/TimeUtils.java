package pl.raisemon.api.utils;

import lombok.Getter;

public class TimeUtils {

    public enum EnumServerTime {
        DAY(0, 12000),
        DUSK(12000, 13450),
        NIGHT(13450, 22550),
        DAWN(22550, 24000);

        @Getter
        private final int start;
        @Getter
        private final int end;

        EnumServerTime(int start, int end) {
            this.start = start;
            this.end = end;
        }
        public static EnumServerTime getTime(long ticks){
            for (EnumServerTime value : values()) {
                if(ticks >= value.start && ticks < value.end){
                    return value;
                }
            }
            return null;
        }
    }
}
