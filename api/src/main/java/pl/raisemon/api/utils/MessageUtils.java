package pl.raisemon.api.utils;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.LiteralText;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.api.ApiPlugin;

public class MessageUtils {
    public static void sendMessage(CommandSource src, String tag, Text of) {
        src.sendMessage(Text.of(tag.length() > 0 ? TextSerializers.FORMATTING_CODE.deserialize(String.format(ApiPlugin.getInstance().getMessagePrefix(), tag)) : Text.of(), of));
    }

    public static void sendMessage(CommandSource src, String tag, String key, Object... values) {
        for (String s : String.format(ApiPlugin.getInstance().getTranslate(key), values).split("\n")) {
            sendMessage(src, tag, TextSerializers.FORMATTING_CODE.deserialize(s));
        }
    }

    public static Text translate(String key, Object... values) {
        return TextSerializers.FORMATTING_CODE.deserialize(String.format(ApiPlugin.getInstance().getTranslate(key), values));
    }

    public static void broadcast(String tag, String key, Object... values) {
        for (Player onlinePlayer : Sponge.getServer().getOnlinePlayers()) {
            sendMessage(onlinePlayer, tag, key, values);
        }
    }

    public static Text replace(Text message, String s, Text buildText) {
        final Text.Builder builder = message.toBuilder();
        final Text.Builder ret = Text.builder();
        for (Text child : builder.getChildren()) {
            if (child.getChildren().size() == 0) {
                if (child instanceof LiteralText && ((LiteralText) child).getContent().contains(s)) {
                    final Text.Builder builder1 = Text.builder();
                    /*String content = ((LiteralText) child).getContent();
                    while (content.contains(s)) {
                        builder1.append(Text.builder().append(Text.of(content.substring(0, content.indexOf(s)))).build());
                        builder1.append(buildText);
                        content = content.substring(content.indexOf(s) + s.length());
                    }
                    builder1.append(Text.builder(child, "").append(Text.of(content)).build());*/
                    builder1.append(buildText);
                    ret.append(builder1.build());
                } else {
                    ret.append(child);
                }
                //String serialize = TextSerializers.FORMATTING_CODE.serialize(child);
                /*while (serialize.contains(s)) {
                    b.append(TextSerializers.FORMATTING_CODE.deserialize(serialize.substring(0, serialize.indexOf(s))))
                            .append(buildText);
                    serialize = serialize.substring(serialize.indexOf(s) + s.length());
                }
                b.append(TextSerializers.FORMATTING_CODE.deserialize(serialize));
                child.getClickAction().ifPresent(b::onClick);
                child.getHoverAction().ifPresent(b::onHover);
                child.getShiftClickAction().ifPresent(b::onShiftClick);
                child.getFormat().applyTo(b);
                ret.append(b.build());*/
            } else {
                ret.append(replace(child, s, buildText));
            }
        }
        return ret.build();
    }
}
