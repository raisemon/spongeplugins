package pl.raisemon.api.utils;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.service.sql.SqlService;

import javax.sql.DataSource;
import java.sql.*;
import java.util.function.Consumer;

public class DatabaseUtil {


    private static SqlService sql;
    public static DataSource getDataSource() throws SQLException {
        if (sql == null) {
            sql = Sponge.getServiceManager().provide(SqlService.class).get();
        }
        return sql.getDataSource(sql.getConnectionUrlFromAlias("raisemon").orElseThrow(()->new Error("Database not found")));
    }

    public static void query(String sql, Consumer<ResultSet> task) {

        try (Connection conn = getDataSource().getConnection();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            task.accept(rs);
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean execute(String sql) {
        try (Connection conn = getDataSource().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql)) {
            return stmt.execute();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int executeUpdate(String sql) {
        int ret = 0;
        try (Connection conn = getDataSource().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql)) {
            ret = stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }
    public static int executeUpdate(String sql, Consumer<PreparedStatement> statementConsumer) {
        int ret = 0;
        try (Connection conn = getDataSource().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql)) {
            statementConsumer.accept(stmt);
            ret = stmt.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static long insert(String sql, Consumer<PreparedStatement> statementConsumer){
        try (Connection conn = getDataSource().getConnection();
             PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statementConsumer.accept(stmt);

            int i = stmt.executeUpdate();
            if (i == 0) {
                System.out.println("0 rows affected");
            }

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getLong(1);
                } else {
                    throw new SQLException("Creating match failed, no ID obtained.");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
