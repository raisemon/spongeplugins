package pl.raisemon.api;

import com.google.inject.Inject;
import lombok.Getter;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.service.user.UserStorageService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.ConfigReloadCommand;
import pl.raisemon.api.commands.RaisemonCommand;
import pl.raisemon.api.utils.MessageUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Plugin(
        id = "raisemon_api",
        name = "RaisemonApi",
        authors = {
                "XopyIP"
        }
)
public class ApiPlugin implements Configurable {

    @Inject
    private java.util.logging.Logger logger;
    @Inject
    private Game game;
    @Getter
    @Inject
    @ConfigDir(sharedRoot = false)
    private Path privateConfigDir;

    private UserStorageService userStorageService;


    @Getter
    private static ApiPlugin instance;
    private Map<String, HoconConfigurationLoader> loaders = new HashMap<>();
    private Map<String, CommentedConfigurationNode> nodes = new HashMap<>();
    private Set<Configurable> configurables = new HashSet<>();

    @Getter
    private String messagePrefix;

    private static Map<UUID, Long> teleportCooldowns = new HashMap<>();

    public static void teleport(Player player, Location<World> location) {
        if(!teleportCooldowns.containsKey(player.getUniqueId())){
            player.setLocation(location);
            teleportCooldowns.put(player.getUniqueId(), System.currentTimeMillis() + 3000);
            return;
        }
        if(teleportCooldowns.get(player.getUniqueId()) > System.currentTimeMillis()){
            MessageUtils.sendMessage(player, "TP", "api.teleportCooldown");
            return;
        }
        player.setLocation(location);
        teleportCooldowns.put(player.getUniqueId(), System.currentTimeMillis() + 3000);

    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        userStorageService = Sponge.getServiceManager().provide(UserStorageService.class).get();
    }
    @Listener
    public void onPreInit(GamePreInitializationEvent event){
        instance = this;
        if(!privateConfigDir.toFile().exists()){
            privateConfigDir.toFile().mkdirs();
        }
        loadConfig(this);
        game.getCommandManager().register(this, CommandSpec.builder()
                .permission("pl.raisemon.reload")
                .child(CommandSpec.builder()
                        .arguments(GenericArguments.choices(Text.of("plugin"),
                                () -> ApiPlugin.this.configurables.stream().map(Configurable::getConfigName).collect(Collectors.toList()),
                                (Function<String, Configurable>) s -> ApiPlugin.this.configurables.stream().filter(c -> c.getConfigName().equals(s)).findFirst().orElse(null)))
                        .executor(new ConfigReloadCommand())
                        .build(), "reload")
                .executor(new RaisemonCommand())
                .build(), "raisemon");

    }

    public CommentedConfigurationNode loadConfig(Configurable plugin) {
        return loadConfig(plugin, false);
    }
    public CommentedConfigurationNode loadConfig(Configurable plugin, boolean reload) {
        this.configurables.add(plugin);
        if (!this.loaders.containsKey(plugin.getConfigName())) {
            this.loaders.put(plugin.getConfigName(), HoconConfigurationLoader.builder().setPath(privateConfigDir.resolve(plugin.getConfigName() + ".conf")).build());
        }

        if (!this.nodes.containsKey(plugin.getConfigName()) || reload) {
            try {
                CommentedConfigurationNode node = this.loaders.get(plugin.getConfigName()).load(plugin.getConfigurationOptions());
                this.nodes.put(plugin.getConfigName(), node);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        CommentedConfigurationNode node = this.nodes.get(plugin.getConfigName());
        if(!privateConfigDir.resolve(plugin.getConfigName() + ".conf").toFile().exists()){
            plugin.defaultConfig(node);
            saveConfig(plugin);
        }
        plugin.load(node);
        return node;
    }

    public void saveConfig(Configurable plugin) {
        try {
            this.loaders.get(plugin.getConfigName()).save(this.nodes.get(plugin.getConfigName()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getConfigName() {
        return "api";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {
        node.getNode("messages", "prefix").setValue("&f[&a%s&f]&r ").setComment("%s - tag");
    }

    @Override
    public void load(CommentedConfigurationNode node) {
        this.messagePrefix = node.getNode("messages", "prefix").getString();
    }


    public CommandSpec buildCommand(BaseCommand command) {
        final CommandSpec.Builder builder = CommandSpec.builder();
        if(command.hasPermission()){
            builder.permission(command.getPermission());
        }
        if(command.getChildren() != null){
            command.getChildren().forEach((cmd)-> builder.child(buildCommand(cmd), cmd.getAliases()));
        }

        return builder.executor(command)
                .arguments(command.getArguments())
                .build();
    }

    public Optional<CommandMapping> registerCommand(BaseCommand command){
        return game.getCommandManager().register(command.getPlugin(), buildCommand(command), command.getAliases());

    }

    @Getter
    private Map<String, String> translations = new HashMap<>();
    private TranslationsConfig translationsConfig = new TranslationsConfig();
    public String getTranslate(String key) {
        if(!translations.containsKey(key)){
            final CommentedConfigurationNode node = loadConfig(translationsConfig).getNode((Object[]) key.split("\\."));
            if(node.isVirtual()){
                node.setValue("&4Missing translation key");
            }
            saveConfig(translationsConfig);
            final String string = node.getString();
            translations.put(key, string);
        }
        return translations.get(key);
    }

    public Optional<User> getUser(UUID uuid){
        return userStorageService.get(uuid);
    }

    public UserStorageService getUserService() {
        return game.getServiceManager().provide(UserStorageService.class).orElse(null);
    }
}
