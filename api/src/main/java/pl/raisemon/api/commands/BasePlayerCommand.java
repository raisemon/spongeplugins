package pl.raisemon.api.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.entity.living.player.Player;
import pl.raisemon.api.utils.MessageUtils;

public abstract class BasePlayerCommand extends BaseCommand {

    private String tag;

    public BasePlayerCommand(Object plugin, String tag) {
        super(plugin);
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if(src instanceof Player){
            return executePlayer((Player)src, args);
        }
        MessageUtils.sendMessage(src, this.tag, "command.notPlayer");
        return CommandResult.success();
    }

    public abstract CommandResult executePlayer(Player player, CommandContext args) throws CommandException;
}
