package pl.raisemon.api.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.Configurable;
import pl.raisemon.api.utils.MessageUtils;

import java.util.Optional;

public class ConfigReloadCommand implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        final Optional<Configurable> plugin = args.getOne("plugin");
        if(plugin.isPresent()){
            ApiPlugin.getInstance().loadConfig(plugin.get(), true);
            MessageUtils.sendMessage(src, "API", Text.of(TextColors.GREEN, "Przeladowano konfiguracje pluginu " + plugin.get().getConfigName()));
            return CommandResult.success();
        }
        MessageUtils.sendMessage(src, "API", Text.of(TextColors.RED, "Nie odnaleziono pluginu o podanej nazwie lub nie zostal on jeszcze zaladowany!"));

        return CommandResult.success();
    }
}
