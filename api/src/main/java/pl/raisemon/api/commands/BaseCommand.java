package pl.raisemon.api.commands;

import lombok.Getter;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.spec.CommandExecutor;

import java.util.Collection;
import java.util.List;

public abstract class BaseCommand implements CommandExecutor {
    @Getter
    private Object plugin;

    public BaseCommand(Object plugin) {
        this.plugin = plugin;
    }

    public abstract List<String> getAliases();

    public abstract String getPermission();

    public boolean hasPermission() {
        return getPermission() != null && getPermission().length()>0;
    }

    public abstract CommandElement[] getArguments();

    public abstract Collection<BaseCommand> getChildren();
}
