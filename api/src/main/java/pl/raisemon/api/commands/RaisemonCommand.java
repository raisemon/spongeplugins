package pl.raisemon.api.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;

import java.net.MalformedURLException;
import java.net.URL;

public class RaisemonCommand implements CommandExecutor {
    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        try {
            src.sendMessage(Text.of(TextActions.openUrl(new URL("https://raisemon.pl")), TextColors.RED, "Serwer Raisemon.pl"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return CommandResult.success();
    }
}
