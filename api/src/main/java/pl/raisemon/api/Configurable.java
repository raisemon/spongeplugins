package pl.raisemon.api;

import ninja.leaping.configurate.ConfigurationOptions;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;

public interface Configurable {
    String getConfigName();

    void defaultConfig(CommentedConfigurationNode node);

    default ConfigurationOptions getConfigurationOptions() {
        return ConfigurationOptions.defaults();
    }

    void load(CommentedConfigurationNode node);
}
