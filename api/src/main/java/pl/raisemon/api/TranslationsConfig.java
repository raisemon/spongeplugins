package pl.raisemon.api;

import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import org.spongepowered.api.Sponge;

import java.io.IOException;

public class TranslationsConfig implements Configurable {
    @Override
    public String getConfigName() {
        return "Translations";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {

    }

    @Override
    public void load(CommentedConfigurationNode node) {
        ApiPlugin.getInstance().getTranslations().clear();
        Sponge.getAssetManager().getAsset(ApiPlugin.getInstance(), "translations.conf").ifPresent(asset ->
        {
            try {
                node.mergeValuesFrom(HoconConfigurationLoader.builder().setURL(asset.getUrl()).build().load(getConfigurationOptions()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        ApiPlugin.getInstance().saveConfig(this);
    }
}
