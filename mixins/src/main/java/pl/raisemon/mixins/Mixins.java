package pl.raisemon.mixins;

import com.google.inject.Inject;
import net.minecraft.tileentity.TileEntity;
import org.slf4j.Logger;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import pl.raisemon.mod.TileEntityRaiseHopper;

@Plugin(
        id = "mixins",
        name = "Mixins",
        dependencies = {
                @Dependency(id = "pixelmon"),
                @Dependency(id = "raisemonmod")
        }
)
public class Mixins {

    @Inject
    private Logger logger;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {

    }

    public static TileEntity newTileEntityHopper() {
        return new TileEntityRaiseHopper();
    }
}
