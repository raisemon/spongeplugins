package pl.raisemon.mixins.mixin;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.network.NetHandlerPlayServer;
import net.minecraft.network.play.client.CPacketCustomPayload;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Mixin(NetHandlerPlayServer.class)
public class NetHandlerServerMixin {
    @Shadow
    public EntityPlayerMP player;
    @Inject(method = "processCustomPayload(Lnet/minecraft/network/play/client/CPacketCustomPayload;)V", at = @At("HEAD"))
    public void processCustomPayload(CPacketCustomPayload packetIn, CallbackInfo info){
        if(packetIn.getChannelName().equals("MC|BSign") || packetIn.getChannelName().equals("MC|BEdit")){
            if(this.player.getHeldItemMainhand().getItem().equals(Items.WRITABLE_BOOK) ||
                    this.player.getHeldItemMainhand().getItem().equals(Items.WRITTEN_BOOK)) {
                this.player.getHeldItemMainhand().setCount(0);
            }
        }
    }


}
