package pl.raisemon.mixins.mixin;

import net.minecraft.block.BlockRedstoneRepeater;
import net.minecraft.block.state.IBlockState;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(BlockRedstoneRepeater.class)
public class BlockRedstoneRepeaterMixin {
    @Inject(method = "getDelay(Lnet/minecraft/block/state/IBlockState;)I", at = @At("HEAD"), cancellable = true)
    public void createNewTileEntity(IBlockState state, CallbackInfoReturnable<Integer> info) {
        info.setReturnValue(state.getValue(BlockRedstoneRepeater.DELAY).intValue() * 20);
    }
}