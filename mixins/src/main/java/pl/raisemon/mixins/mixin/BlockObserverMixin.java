package pl.raisemon.mixins.mixin;

import net.minecraft.block.Block;
import net.minecraft.block.BlockObserver;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Redirect;

@Mixin(BlockObserver.class)
public class BlockObserverMixin {
    @Redirect(method = "startSignal", at = @At(value = "INVOKE",
            target = "Lnet/minecraft/world/World;scheduleUpdate(Lnet/minecraft/util/math/BlockPos;" +
                    "Lnet/minecraft/block/Block;I)V"))
    public void scheduleUpdate(World world, BlockPos pos, Block block, int delay) {
        world.scheduleUpdate(pos, block, delay*10);
    }
    @Redirect(method = "updateTick", at = @At(value = "INVOKE",
            target = "Lnet/minecraft/world/World;scheduleUpdate(Lnet/minecraft/util/math/BlockPos;" +
                    "Lnet/minecraft/block/Block;I)V"))
    public void updateTick(World world, BlockPos pos, Block block, int delay) {
        world.scheduleUpdate(pos, block, delay*10);
    }
}
