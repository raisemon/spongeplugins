package pl.raisemon.mixins.mixin;

import net.minecraft.block.BlockHopper;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import pl.raisemon.mixins.Mixins;

@Mixin(BlockHopper.class)
public class BlockHopperMixin {
    @Inject(method = "createNewTileEntity(Lnet/minecraft/world/World;I)Lnet/minecraft/tileentity/TileEntity;", at = @At("HEAD"), cancellable = true)
    public void createNewTileEntity(World world, int meta, CallbackInfoReturnable<TileEntity> info){
        TileEntity tileEntity = Mixins.newTileEntityHopper();
        info.setReturnValue(tileEntity);
    }


}
