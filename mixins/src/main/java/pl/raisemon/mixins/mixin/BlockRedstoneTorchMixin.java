package pl.raisemon.mixins.mixin;

import net.minecraft.block.BlockRedstoneTorch;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(BlockRedstoneTorch.class)
public class BlockRedstoneTorchMixin {
/*    @Redirect(method = "isBurnedOut", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/World;getTotalWorldTime()J"))
    protected long getTotalWorldTime(final World world) {
        return world.getTotalWorldTime() + 600;
    }*/
    @Inject(method = "tickRate(Lnet/minecraft/world/World;)I", at = @At("HEAD"), cancellable = true)
    public void createNewTileEntity(World world, CallbackInfoReturnable<Integer> info){
        info.setReturnValue(15);
    }
}
