package pl.raisemon.leaders.listeners;

import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;

public class ChatListener {
    @Listener
    public void onChat(MessageChannelEvent.Chat e, @First Player player){
        Gym gym = LeadersPlugin.getInstance().getGym(player.getUniqueId());
        String serialize = TextSerializers.JSON.serialize(e.getMessage());
        int idx = serialize.indexOf("{LIDER}");
        if(idx>=0){
            serialize = serialize.substring(0, idx) + (gym == null ? "" : TextSerializers.LEGACY_FORMATTING_CODE.serialize(gym.getPrefix())) +
                    serialize.substring(idx + "{LIDER}".length());
        }
        e.setMessage(TextSerializers.JSON.deserialize(serialize));
    }
}
