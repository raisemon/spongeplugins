package pl.raisemon.leaders.listeners;

import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;
import pl.raisemon.mod.networking.SCLeadersGuiOpenPacket;

public class DisconnectListener {
    @Listener
    public void onDisconnect(ClientConnectionEvent.Disconnect event){
        Gym gym = LeadersPlugin.getInstance().getGym(event.getTargetEntity().getUniqueId());
        if(gym == null){
            return;
        }
        gym.setStatus(SCLeadersGuiOpenPacket.LeaderGymStatus.NOT_AVAILABLE);
    }
}
