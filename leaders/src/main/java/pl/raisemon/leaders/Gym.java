package pl.raisemon.leaders;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import net.minecraft.item.Item;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.mod.networking.SCLeadersGuiOpenPacket;

import java.util.UUID;

@Data
@RequiredArgsConstructor
public class Gym {
    @NonNull
    private String name;
    @NonNull
    private Text prefix;
    @NonNull
    private byte level;
    private UUID leader;
    @NonNull
    private Item badge;
    private Location<World> gymLocation;
    private SCLeadersGuiOpenPacket.LeaderGymStatus status = SCLeadersGuiOpenPacket.LeaderGymStatus.NO_LEADER;

    public String getLeaderName() {
        if(leader == null){
            return "-Brak-";
        }
        return ApiPlugin.getInstance().getUserService().get(leader).map(User::getName).orElse("-Brak-");
    }

    public void setLeader(UUID leader) {
        if(leader == null){
            status = SCLeadersGuiOpenPacket.LeaderGymStatus.NO_LEADER;
        }else{
            status = SCLeadersGuiOpenPacket.LeaderGymStatus.NOT_AVAILABLE;
        }
        this.leader = leader;
    }
}
