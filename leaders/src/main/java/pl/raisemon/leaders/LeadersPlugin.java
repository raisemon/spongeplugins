package pl.raisemon.leaders;

import com.google.inject.Inject;
import lombok.Getter;
import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Node;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import org.slf4j.Logger;
import org.spongepowered.api.Game;
import org.spongepowered.api.data.type.ComparatorTypes;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.Configurable;
import pl.raisemon.leaders.commands.*;
import pl.raisemon.leaders.listeners.ChatListener;
import pl.raisemon.leaders.listeners.DisconnectListener;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Plugin(
        id = "raisemon_leaders",
        name = "RaisemonLeaders",
        authors = {
                "XopyIP"
        },
        dependencies = {
                @Dependency(id = "raisemon_api"),
                @Dependency(id = "pixelmon"),
                @Dependency(id = "raisemonmod"),
        }
)
public class LeadersPlugin implements Configurable {

    @Inject
    private Logger logger;
    @Inject
    private Game game;
    @Getter
    private static LeadersPlugin instance;
    @Getter
    private List<Gym> gyms = new ArrayList<>();
    @Getter
    private CommentedConfigurationNode node;
    @Getter
    private Node phPermissionNode;


    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        BadgeManager.load();
        game.getEventManager().registerListeners(this, new DisconnectListener());
        game.getEventManager().registerListeners(this, new ChatListener());
    }

    @Listener
    public void onInit(GameInitializationEvent event) {
        this.node = ApiPlugin.getInstance().loadConfig(this);
        phPermissionNode = LuckPerms.getApi().getNodeFactory().newBuilder("pl.raisemon.command.ph").build();
        ApiPlugin.getInstance().registerCommand(new LeadersCommand(this));
        ApiPlugin.getInstance().registerCommand(new GymCommand(this));
        ApiPlugin.getInstance().registerCommand(new BattlesCommand(this));
        ApiPlugin.getInstance().registerCommand(new WinCommand(this));

    }

    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        instance = this;
    }

    @Override
    public String getConfigName() {
        return "Leaders";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {
        node.getNode("gyms", "grass", "name").setValue("&aGrass");
        node.getNode("gyms", "grass", "prefix").setValue("&a[&2Grass&a]&f");
        node.getNode("gyms", "grass", "lvl").setValue(40);
        node.getNode("gyms", "grass", "badge").setValue(ItemTypes.DIAMOND.getId());
    }

    @Override
    public void load(CommentedConfigurationNode node) {
        gyms.clear();
        node.getNode("gyms").getChildrenMap().forEach((key, value) -> {
            final Gym gym = loadGym((String) key, value);
            gyms.add(gym);
        });
        gyms.sort((o1, o2) -> o1.getLevel() - o2.getLevel());
    }


    private Gym loadGym(String name, CommentedConfigurationNode node) {
        Gym gym = new Gym(name,
                TextSerializers.FORMATTING_CODE.deserialize(node.getNode("prefix").getString()),
                (byte) node.getNode("lvl").getInt(),
                ForgeRegistries.ITEMS.getValue(new ResourceLocation(node.getNode("badge").getString()))
        );
        if (!node.getNode("leader").isVirtual()) {
            gym.setLeader(UUID.fromString(node.getNode("leader").getString()));
        }
        return gym;
    }


    public Gym getGym(String unformattedName) {
        return gyms.stream().filter(gym -> gym.getName().equals(unformattedName)).findAny().orElse(null);
    }

    public Gym getGym(UUID uniqueId) {
        if (uniqueId == null) return null;
        return gyms.stream().filter(gym -> uniqueId.equals(gym.getLeader())).findAny().orElse(null);
    }

    public boolean isLeader(User user) {
        return gyms.stream().anyMatch(gym -> user.getUniqueId().equals(gym.getLeader()));
    }


}
