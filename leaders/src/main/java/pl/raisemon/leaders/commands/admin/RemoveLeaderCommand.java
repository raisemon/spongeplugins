package pl.raisemon.leaders.commands.admin;

import me.lucko.luckperms.LuckPerms;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;
import pl.raisemon.mod.networking.SCLeadersGuiOpenPacket;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class RemoveLeaderCommand extends BaseCommand {
    public RemoveLeaderCommand(LeadersPlugin leadersPlugin) {
        super(leadersPlugin);
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("zabierz");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.leaders.admin";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.user(Text.of("user")),
        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        Optional<User> userOpt = args.getOne("user");
        User user = userOpt.get();
        Gym gym = LeadersPlugin.getInstance().getGym(user.getUniqueId());
        if(gym == null){
            src.sendMessages(Text.of(TextColors.RED, "Gracz nie jest liderem"));
            return CommandResult.empty();
        }
        MessageUtils.broadcast("Leaders", "leaders.noLongerLeader", user.getName(), (gym.getName()));

        me.lucko.luckperms.api.User permsUser = LuckPerms.getApi().getUser(user.getUniqueId());
        if(permsUser == null){
            LuckPerms.getApi().getStorage().loadUser(user.getUniqueId()).thenAcceptAsync(wasSuccessful -> {
                if (!wasSuccessful) {
                    return;
                }
                me.lucko.luckperms.api.User loadedUser = LuckPerms.getApi().getUser(user.getUniqueId());
                if (loadedUser == null) {
                    return;
                }
                loadedUser.unsetPermission(LeadersPlugin.getInstance().getPhPermissionNode());
                LuckPerms.getApi().getUserManager().saveUser(permsUser);
            }, LuckPerms.getApi().getStorage().getSyncExecutor());
        }else {
            permsUser.unsetPermission(LeadersPlugin.getInstance().getPhPermissionNode());
            LuckPerms.getApi().getStorage().saveUser(permsUser);
        }
        gym.setLeader(null);
        gym.setStatus(SCLeadersGuiOpenPacket.LeaderGymStatus.NOT_AVAILABLE);
        LeadersPlugin.getInstance().getNode().getNode("gyms", gym.getName(), "leader").setValue(null);
        ApiPlugin.getInstance().saveConfig(LeadersPlugin.getInstance());

        return CommandResult.success();
    }
}
