package pl.raisemon.leaders.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GymCommand extends BasePlayerCommand {
    public GymCommand(LeadersPlugin leadersPlugin) {
        super(leadersPlugin, "Liderzy");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        final Gym gym = args.<Gym>getOne("gym").get();
        if(gym.getGymLocation() == null){
            MessageUtils.sendMessage(player, "Liderzy", "leaders.teleportUnavailable", (gym.getName()));
            return CommandResult.success();
        }
        MessageUtils.sendMessage(player, "Liderzy", "leaders.teleportedtogym", (gym.getName()));
        ApiPlugin.teleport(player, gym.getGymLocation());
        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("sala");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.leaders.gymteleport";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.choices(Text.of("gym"), () -> LeadersPlugin.getInstance().getGyms().stream().map(g -> g.getName()).collect(Collectors.toList()), (Function<String, Gym>) s -> LeadersPlugin.getInstance().getGym(s))
        };
    }

    @Override
    public Collection<BaseCommand> getChildren() {
        return null;
    }
}
