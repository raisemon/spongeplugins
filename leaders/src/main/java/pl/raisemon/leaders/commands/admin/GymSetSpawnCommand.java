package pl.raisemon.leaders.commands.admin;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GymSetSpawnCommand extends BasePlayerCommand {
    public GymSetSpawnCommand(LeadersPlugin leadersPlugin) {
        super(leadersPlugin, "liderzy");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        Optional<Gym> gymName = args.getOne("gym");
        Gym gym = gymName.get();
        gym.setGymLocation(player.getLocation());
        MessageUtils.sendMessage(player, "Liderzy", "leaders.spawnSet", (gym.getName()));
        try {
            LeadersPlugin.getInstance().getNode().getNode("gyms", gym.getName(), "loc").setValue(TypeToken.of(Location.class), gym.getGymLocation());
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
        ApiPlugin.getInstance().saveConfig(LeadersPlugin.getInstance());
        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("ustawsale");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.leaders.admin";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.choices(Text.of("gym"), () -> LeadersPlugin.getInstance().getGyms().stream().map(Gym::getName).collect(Collectors.toList()), (Function<String, Gym>) s -> LeadersPlugin.getInstance().getGym(s))

        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }
}
