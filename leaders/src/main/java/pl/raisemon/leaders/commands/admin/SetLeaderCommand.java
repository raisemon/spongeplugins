package pl.raisemon.leaders.commands.admin;

import me.lucko.luckperms.LuckPerms;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class SetLeaderCommand extends BaseCommand {
    public SetLeaderCommand(Object plugin) {
        super(plugin);
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("daj");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.leaders.admin";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.user(Text.of("user")),
                GenericArguments.choices(Text.of("gym"), () -> LeadersPlugin.getInstance().getGyms().stream().map(g -> g.getName()).collect(Collectors.toList()), (Function<String, Gym>) s -> LeadersPlugin.getInstance().getGym(s))
        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        Optional<User> userOpt = args.getOne("user");
        Optional<Gym> gymName = args.getOne("gym");
        Gym gym = gymName.get();
        User user = userOpt.get();
        if(LeadersPlugin.getInstance().isLeader(user)){
            MessageUtils.sendMessage(src, "Liderzy", "leaders.alreadyLeader", user.getName());
            return CommandResult.empty();
        }
        if(gym.getLeader()!=null){
            MessageUtils.sendMessage(src, "Liderzy", "leaders.alreadyHasLeader", (gym.getName()));
            return CommandResult.empty();
        }
        MessageUtils.broadcast("Leaders", "leaders.newLeader", user.getName(), (gym.getName()));
        gym.setLeader(user.getUniqueId());
        me.lucko.luckperms.api.User permsUser = LuckPerms.getApi().getUser(user.getUniqueId());
        if(permsUser == null){
            LuckPerms.getApi().getStorage().loadUser(user.getUniqueId()).thenAcceptAsync(wasSuccessful -> {
                if (!wasSuccessful) {
                    return;
                }
                me.lucko.luckperms.api.User loadedUser = LuckPerms.getApi().getUser(user.getUniqueId());
                if (loadedUser == null) {
                    return;
                }

                loadedUser.setPermission(LeadersPlugin.getInstance().getPhPermissionNode());
                LuckPerms.getApi().getStorage().saveUser(loadedUser);
            }, LuckPerms.getApi().getStorage().getSyncExecutor());
        }else{
            permsUser.setPermission(LeadersPlugin.getInstance().getPhPermissionNode());
            LuckPerms.getApi().getStorage().saveUser(permsUser);
        }
        LeadersPlugin.getInstance().getNode().getNode("gyms", gym.getName(), "leader").setValue(user.getUniqueId().toString());
        ApiPlugin.getInstance().saveConfig(LeadersPlugin.getInstance());
        return CommandResult.success();
    }
}
