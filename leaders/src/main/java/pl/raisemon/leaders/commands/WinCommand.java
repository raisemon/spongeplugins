package pl.raisemon.leaders.commands;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Node;
import me.lucko.luckperms.api.User;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.item.inventory.transaction.InventoryTransactionResult;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.leaders.BadgeManager;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;
import pl.raisemon.mod.RegistrationHandler;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WinCommand extends BasePlayerCommand {
    private static final Node TM_NODE = LuckPerms.getApi().buildNode("nucleus.kits.tm").build();
    private static final Node PH_NODE = LuckPerms.getApi().buildNode("pl.raisemon.commands.ph").build();
    public WinCommand(LeadersPlugin leadersPlugin) {
        super(leadersPlugin, "Liderzy");
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("win");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.choices(Text.of("gym"), () -> LeadersPlugin.getInstance().getGyms().stream().map(g -> g.getName()).collect(Collectors.toList()), (Function<String, Gym>) s -> LeadersPlugin.getInstance().getGym(s)),
                GenericArguments.player(Text.of("player"))
        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }
    @Override
    public CommandResult executePlayer(Player src, CommandContext args) throws CommandException {
        Gym gym = args.<Gym>getOne("gym").get();
        if (!gym.getLeader().equals(src.getUniqueId())) {
            MessageUtils.sendMessage(src, "Liderzy", "leaders.playerIsNotLeader");
            return CommandResult.empty();
        }
        Player player = args.<Player>getOne("player").get();
        if(gym.getLevel() > LeadersPlugin.getInstance().getGyms().stream().mapToInt(Gym::getLevel).min().orElse(Integer.MAX_VALUE)){
            final InventoryTransactionResult offer = player.getInventory().offer(ItemStack.builder()
                    .itemType((ItemType) RegistrationHandler.TMS_PACK)
                    .quantity(1).build());
            if(offer.getRejectedItems().size()>0){
                MessageUtils.sendMessage(src, "Liderzy", "leaders.leaderPlayerFullEQ", player.getName());
                return CommandResult.success();
            }
        }
        if(BadgeManager.countBadges(player.getUniqueId()) == 1){
            final InventoryTransactionResult offer = player.getInventory().offer(ItemStack.builder()
                    .itemType((ItemType) RegistrationHandler.EFFICIENCY_STONE)
                    .add(Keys.ITEM_DURABILITY, 2)
                    .quantity(1).build());
            if(offer.getRejectedItems().size()>0){
                MessageUtils.sendMessage(src, "Liderzy", "leaders.leaderPlayerFullEQ", player.getName());
                player.getInventory().query(QueryOperationTypes.ITEM_TYPE.of((ItemType) RegistrationHandler.TMS_PACK)).poll(1);
                return CommandResult.success();
            }
        }
        if(BadgeManager.countBadges(player.getUniqueId()) == 5){
            User user = LuckPerms.getApi().getUser(player.getUniqueId());
            if(user != null){
                user.setPermission(TM_NODE);
            }
        }
        if(BadgeManager.countBadges(player.getUniqueId()) == 7){
            User user = LuckPerms.getApi().getUser(player.getUniqueId());
            if(user != null){
                user.setPermission(PH_NODE);
            }
        }
        MessageUtils.sendMessage(src, "Liderzy", "leaders.leaderPlayerWin", player.getName());
        MessageUtils.sendMessage(player, "Liderzy", "leaders.playerWin", (gym.getName()));
        BadgeManager.setBadge(player, gym.getName());

        return CommandResult.success();
    }
}
