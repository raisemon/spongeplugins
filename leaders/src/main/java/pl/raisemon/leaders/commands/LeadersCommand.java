package pl.raisemon.leaders.commands;

import net.minecraft.entity.player.EntityPlayerMP;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.entity.living.player.Player;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.leaders.BadgeManager;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;
import pl.raisemon.leaders.commands.admin.GymSetSpawnCommand;
import pl.raisemon.leaders.commands.admin.RemoveBadgeCommand;
import pl.raisemon.leaders.commands.admin.RemoveLeaderCommand;
import pl.raisemon.leaders.commands.admin.SetLeaderCommand;
import pl.raisemon.mod.networking.RaisemonPacketHandler;
import pl.raisemon.mod.networking.SCLeadersGuiOpenPacket;

import java.util.*;

public class LeadersCommand extends BasePlayerCommand {
    public LeadersCommand(LeadersPlugin leadersPlugin) {
        super(leadersPlugin, "Liderzy");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        List<SCLeadersGuiOpenPacket.LeadersGym> leadersGyms = new ArrayList<>();
        for (Gym gym : LeadersPlugin.getInstance().getGyms()) {
            leadersGyms.add(new SCLeadersGuiOpenPacket.LeadersGym(
                    gym.getName(),
                    gym.getLevel(), gym.getLeaderName(),
                    gym.getStatus(), gym.getBadge().getRegistryName().toString(),
                    BadgeManager.hasBadge(player.getUniqueId(), gym)
            ));
        }
        RaisemonPacketHandler.INSTANCE.sendTo(new SCLeadersGuiOpenPacket(leadersGyms), (EntityPlayerMP) player);
        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("liderzy");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Collection<BaseCommand> getChildren() {
        Set<BaseCommand> child = new HashSet<>();
        child.add(new GymSetSpawnCommand(LeadersPlugin.getInstance()));
        child.add(new RemoveBadgeCommand(LeadersPlugin.getInstance()));
        child.add(new RemoveLeaderCommand(LeadersPlugin.getInstance()));
        child.add(new SetLeaderCommand(LeadersPlugin.getInstance()));
        return child;
    }
}
