package pl.raisemon.leaders.commands.admin;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.leaders.BadgeManager;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RemoveBadgeCommand extends BaseCommand {
    public RemoveBadgeCommand(LeadersPlugin leadersPlugin) {
        super(leadersPlugin);
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("zabierzodznake");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.leaders.admin";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.user(Text.of("user")),
                GenericArguments.choices(Text.of("gym"), () -> LeadersPlugin.getInstance().getGyms().stream().map(Gym::getName).collect(Collectors.toList()), (Function<String, Gym>) s -> LeadersPlugin.getInstance().getGym(s))

        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        User user = args.<User>getOne("user").get();
        Gym gym = args.<Gym>getOne("gym").get();
        BadgeManager.removeBadge(user.getUniqueId(), gym);
        MessageUtils.sendMessage(src, "Liderzy", "leaders.badgeRemoved", (gym.getName()), user.getName());

        return CommandResult.success();
    }
}
