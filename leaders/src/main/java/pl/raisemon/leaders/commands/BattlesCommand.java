package pl.raisemon.leaders.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.leaders.Gym;
import pl.raisemon.leaders.LeadersPlugin;
import pl.raisemon.mod.networking.SCLeadersGuiOpenPacket;

import java.util.*;
import java.util.function.Function;

public class BattlesCommand extends BasePlayerCommand {
    public BattlesCommand(LeadersPlugin leadersPlugin) {
        super(leadersPlugin, "Liderzy");
    }

    @Override
    public CommandResult executePlayer(Player src, CommandContext args) throws CommandException {
        Optional<Boolean> enable = args.getOne("enable");
        Gym gym = LeadersPlugin.getInstance().getGym(src.getUniqueId());
        if (gym == null) {
            MessageUtils.sendMessage(src, "Liderzy", "leaders.playerIsNotLeader");
            return CommandResult.empty();
        }
        if (enable.get()) {
            MessageUtils.broadcast("Liderzy", "leaders.leadersBattlesEnabled", src.getName(), (gym.getName()));
            gym.setStatus(SCLeadersGuiOpenPacket.LeaderGymStatus.AVAILABLE);
        }else {
            MessageUtils.broadcast("Liderzy", "leaders.leadersBattlesDisabled", src.getName(), (gym.getName()));
            gym.setStatus(SCLeadersGuiOpenPacket.LeaderGymStatus.NOT_AVAILABLE);
        }
        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("walki");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.choices(Text.of("enable"), () -> Arrays.asList("ON", "OFF"), (Function<String, Boolean>) s -> s.equalsIgnoreCase("ON"))
        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }
}
