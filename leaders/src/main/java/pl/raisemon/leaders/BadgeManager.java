package pl.raisemon.leaders;

import pl.raisemon.api.utils.DatabaseUtil;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.util.Tuple;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;


public class BadgeManager {

    private static final String BADGE_TABLE_NAME = "rsm_badges";
    private static Set<Tuple<UUID, String>> badges = new HashSet<>();

    public static boolean hasBadge(UUID player, Gym gym) {
        return badges.contains(new Tuple<>(player, gym.getName()));
    }

    public static int countBadges(UUID player){
        return (int) badges.stream().filter(b -> b.getFirst().equals(player)).count();
    }

    public static void setBadge(User user, String gym) {
        Tuple<UUID, String> tuple = new Tuple<>(user.getUniqueId(), gym);
        if(!badges.contains(tuple)){
            badges.add(tuple);
            DatabaseUtil.insert("INSERT INTO `" + BADGE_TABLE_NAME +"`" +
                    "(`uuid`, `nickname`,`gym`) VALUES" +
                    "(?,      ?,          ?)", stmt->{

                try{
                    stmt.setString(1, user.getUniqueId().toString().replace("-",""));
                    stmt.setString(2, user.getName());
                    stmt.setString(3, gym);
                }catch(SQLException ex){
                    ex.printStackTrace();
                }
            });
        }
    }

    public static void removeBadges(UUID uniqueId) {
        badges.removeIf(tuple -> tuple.getFirst().equals(uniqueId));
        DatabaseUtil.execute("DELETE FROM `"+ BADGE_TABLE_NAME +"` WHERE `uuid`='"+uniqueId.toString().replace("-","")+"';");
    }

    public static void load() {
        DatabaseUtil.execute("CREATE TABLE IF NOT EXISTS `"+ BADGE_TABLE_NAME +"` (" +
                "`id` INT NOT NULL AUTO_INCREMENT," +
                "`uuid` varchar(32) NOT NULL," +
                "`nickname` varchar(32) NOT NULL," +
                "`gym` varchar(32) NOT NULL," +
                "PRIMARY KEY (`id`)" +
                ");");
        DatabaseUtil.query("SELECT * FROM `" + BADGE_TABLE_NAME + "`;", results->{
            try{
                while (results.next()) {
                    String gym = results.getString("gym");
                    if (LeadersPlugin.getInstance().getGym(gym)==null) {
                        results.deleteRow();
                        continue;
                    }
                    badges.add(new Tuple<>(UUID.fromString(results.getString("uuid").replaceAll(
                            "(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})",
                            "$1-$2-$3-$4-$5")), gym));
                }
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        });
    }

    public static void removeBadge(UUID uniqueId, Gym gym1) {
        badges.removeIf(tuple -> tuple.getFirst().equals(uniqueId) && tuple.getSecond().equals(gym1.getName()));
        DatabaseUtil.execute("DELETE FROM `"+ BADGE_TABLE_NAME +"` WHERE `uuid`='"+uniqueId.toString().replace("-","")+"' AND `gym`=\""+gym1.getName()+"\";");
    }
}
