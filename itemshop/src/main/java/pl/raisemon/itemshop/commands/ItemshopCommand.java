package pl.raisemon.itemshop.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.itemshop.ItemShopPlugin;
import pl.raisemon.itemshop.ItemshopItem;

import java.util.*;

public class ItemshopCommand extends BasePlayerCommand {
    private ItemShopPlugin itemShopPlugin;

    public ItemshopCommand(ItemShopPlugin itemShopPlugin) {
        super(itemShopPlugin, "IS");
        this.itemShopPlugin = itemShopPlugin;
    }

    @Override
    public List<String> getAliases() {
        return Arrays.asList("itemshop", "is", "odbierz");
    }

    @Override
    public String getPermission() {
        return "com.raisemon.itemshop";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        final HashSet<BaseCommand> baseCommands = new HashSet<>();
        baseCommands.add(new ItemshopGiveCommand(itemShopPlugin));
        baseCommands.add(new ItemshopSeeCommand(itemShopPlugin));
        baseCommands.add(new ItemshopReceiveCommand(itemShopPlugin));
        return baseCommands;
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {

        List<ItemshopItem> items = ItemShopPlugin.getInstance().getItems(player.getName());
        if(items.size()==0){
            MessageUtils.sendMessage(player, "IS", "commands.itemshopSelfNoItems");
            return CommandResult.success();
        }
        MessageUtils.sendMessage(player, "IS", "commands.itemshopSelfItemsHeader");
        for (int i = 0; i < items.size(); i++) {
            ItemshopItem itemshopItem = items.get(i);
            final String s = itemshopItem.getType().getTranslation().get(new Locale.Builder().setLanguage("pl").setRegion("PL").build());
            final Text is = MessageUtils.translate("commands.itemshopSelfItemsEntry", (i + 1), s);
            MessageUtils.sendMessage(player, "IS",
                    Text.of(
                            TextActions.showText(MessageUtils.translate("commands.itemshopItemHover")),
                            TextActions.runCommand("/itemshop rec " + itemshopItem.getDbid()),
                            is
                    ));

        }
        return CommandResult.success();
    }
}
