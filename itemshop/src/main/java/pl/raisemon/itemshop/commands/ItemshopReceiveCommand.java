package pl.raisemon.itemshop.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.entity.Hotbar;
import org.spongepowered.api.item.inventory.entity.PlayerInventory;
import org.spongepowered.api.item.inventory.property.SlotIndex;
import org.spongepowered.api.item.inventory.type.GridInventory;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.itemshop.ItemShopPlugin;
import pl.raisemon.itemshop.ItemshopItem;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class ItemshopReceiveCommand extends BasePlayerCommand {
    public ItemshopReceiveCommand(ItemShopPlugin itemShopPlugin) {
        super(itemShopPlugin, "IS");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        Optional<Integer> num = args.getOne("num");
        boolean haveEmptySlot = false;
        PlayerInventory playerInventory = player.getInventory().query(PlayerInventory.class);
        GridInventory main = playerInventory.getMain();
        for(int y = 0; y<main.getRows(); y++){
            for(int x = 0; x<main.getColumns(); x++){
                Optional<Slot> slot = main.getSlot(x, y);
                if(slot.isPresent() && !slot.get().peek().isPresent()){
                    haveEmptySlot = true;
                    break;
                }
            }
            if(haveEmptySlot){
                break;
            }
        }

        Hotbar hotbar = playerInventory.getHotbar();
        if(!haveEmptySlot){
            for(int i = 0; i<9; i++){
                Optional<Slot> slot = hotbar.getSlot(SlotIndex.of(i));
                if(slot.isPresent() && !slot.get().peek().isPresent()){
                    haveEmptySlot = true;
                    break;
                }
            }
        }

        if(!haveEmptySlot){
            MessageUtils.sendMessage(player, "IS", "commands.itemshopFullEQ");
            return CommandResult.success();
        }

        ItemshopItem itemshopItem = ItemShopPlugin.getInstance().getItem(player.getName(), num.get());
        if(itemshopItem == null){
            MessageUtils.sendMessage(player, "IS", "commands.itemshopIncorrectNumber");
            return CommandResult.success();
        }
        itemshopItem.give(player);

        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("rec");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{GenericArguments.integer(Text.of("num"))};
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }
}
