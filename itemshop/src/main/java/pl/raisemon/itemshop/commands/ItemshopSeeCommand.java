package pl.raisemon.itemshop.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.itemshop.ItemShopPlugin;
import pl.raisemon.itemshop.ItemshopItem;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class ItemshopSeeCommand extends BaseCommand {
    public ItemshopSeeCommand(ItemShopPlugin itemShopPlugin) {
        super(itemShopPlugin);
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("zobacz");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.admin";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.string(Text.of("user"))
        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        Optional<User> user = args.getOne("user");

        List<ItemshopItem> items = ItemShopPlugin.getInstance().getItems(user.get().getName());
        if(items.size()==0){
            MessageUtils.sendMessage(src, "IS", "commands.itemshopPlayerHasNoItems");
            return CommandResult.success();
        }
        MessageUtils.sendMessage(src, "IS", "commands.itemshopPlayerItemsHeader",user.get().getName());
        for (int i = 0; i < items.size(); i++) {
            ItemshopItem itemshopItem = items.get(i);
            MessageUtils.sendMessage(src, "IS", "commands.itemshopPlayerItemsEntry", (i+1), itemshopItem.getType().getTranslation().get());
        }
        return CommandResult.success();

    }
}
