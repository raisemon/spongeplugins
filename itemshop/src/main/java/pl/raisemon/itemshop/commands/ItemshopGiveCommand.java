package pl.raisemon.itemshop.commands;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.itemshop.ItemShopPlugin;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

public class ItemshopGiveCommand extends BaseCommand {
    public ItemshopGiveCommand(ItemShopPlugin itemShopPlugin) {
        super(itemShopPlugin);
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("przyznaj");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.admin";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.string(Text.of("user")),
                GenericArguments.catalogedElement(Text.of("item"), ItemType.class),
                GenericArguments.remainingRawJoinedStrings(Text.of("args"))
        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        Logger logger = ItemShopPlugin.getInstance().getLogger();
        Optional<String> userOptional = args.getOne("user");
        Optional<ItemType> itemTypeOptional = args.getOne("item");
        Optional<String> argsOptional = args.getOne("args");

        if (!userOptional.isPresent() || !itemTypeOptional.isPresent() || !argsOptional.isPresent()) {
            MessageUtils.sendMessage(src, "IS", "commands.itemshopWrongArgument");
            return CommandResult.empty();
        }

        ItemShopPlugin.getInstance().addItem(userOptional.get(), itemTypeOptional.get(), argsOptional.get());
        logger.info("Gracz " + userOptional.get() + " otrzymal przedmiot " + itemTypeOptional.get().getName());
        Sponge.getServer().getPlayer(userOptional.get()).ifPresent(player ->
                MessageUtils.sendMessage(player, "IS", "commands.itemshopItemReceived", itemTypeOptional.get().getName()));
        MessageUtils.sendMessage(src, "IS", "commands.itemshopItemGiven", userOptional.get(), itemTypeOptional.get().getName());
        return CommandResult.success();
    }
}
