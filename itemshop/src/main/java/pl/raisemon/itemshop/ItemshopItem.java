package pl.raisemon.itemshop;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.ItemType;
import pl.raisemon.api.utils.DatabaseUtil;
import pl.raisemon.api.utils.MessageUtils;

import java.util.Optional;
import java.util.logging.Logger;

public class ItemshopItem {
    private final String nick;
    private int dbid;
    private final Optional<ItemType> type;
    private String args;

    public ItemshopItem(int dbid, String nick, Optional<ItemType> type, String args) {
        this.dbid = dbid;
        this.nick = nick;
        this.type = type;
        this.args = args;
    }

    public ItemType getType() {
        return type.orElse(null);
    }

    public int getDbid() {
        return dbid;
    }

    public String getArgs() {
        return args;
    }

    public void give(Player player){
        Logger logger = ItemShopPlugin.getInstance().getLogger();

        if(!type.isPresent()){
            MessageUtils.sendMessage(player, "IS", "commands.itemshopItemNotFound");
            return;
        }
        logger.info("Gracz " + player.getName() + " odebral item " + this.type.get().getId() + " " + this.args);
        this.remove();
        Sponge.getCommandManager().process(Sponge.getServer().getConsole(), "give " + player.getName() + " " + this.type.get().getId() + " " + this.args);
        MessageUtils.sendMessage(player, "IS", "commands.itemshopItemGot");
    }

    public boolean remove(){
        return DatabaseUtil.execute("DELETE FROM `"+ ItemShopPlugin.ITEMS_TABLE_NAME +"` WHERE `id`="+this.dbid+";");
    }

}
