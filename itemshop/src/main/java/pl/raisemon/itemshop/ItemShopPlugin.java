package pl.raisemon.itemshop;

import pl.raisemon.itemshop.commands.ItemshopCommand;
import com.google.inject.Inject;
import lombok.Getter;
import org.spongepowered.api.Game;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.utils.DatabaseUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Logger;

@Plugin(id = "raisemon_itemshop", name = "Raisemon Itemshop", version = "1.0", description = "Itemshop", dependencies = {@Dependency(id = "raisemon_api")})
public class ItemShopPlugin {
    @Getter
    @Inject
    private Logger logger;
    @Inject
    private Game game;
    @Getter
    private static ItemShopPlugin instance;
    public static String ITEMS_TABLE_NAME = "rsm_itemshop";

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
    }

    @Listener
    public void onInit(GameInitializationEvent event) {

        DatabaseUtil.execute("CREATE TABLE IF NOT EXISTS `" + ITEMS_TABLE_NAME + "` (" +
                "`id` INT NOT NULL AUTO_INCREMENT," +
                "`nickname` varchar(32) NOT NULL," +
                "`item` varchar(128) NOT NULL," +
                "`args` VARCHAR(256) NOT NULL," +
                "PRIMARY KEY (`id`)" +
                ");");

        ApiPlugin.getInstance().registerCommand(new ItemshopCommand(this));
    }

    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        instance = this;
    }


    public void addItem(String nick, ItemType type, String args) {
        DatabaseUtil.insert("INSERT INTO `" + ITEMS_TABLE_NAME + "`" +
                "(`nickname`,`item`,`args`) VALUES" +
                "(?,          ?,    ?)", stmt -> {
            try {
                stmt.setString(1, nick);
                stmt.setString(2, type.getId());
                stmt.setString(3, args);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }


    public List<ItemshopItem> getItems(String nick) {

        List<ItemshopItem> ret = new ArrayList<>();
        DatabaseUtil.query("SELECT * FROM `" + ITEMS_TABLE_NAME + "` WHERE `nickname`=\"" + nick + "\";", results -> {
            try {
                while (results.next()) {
                    ret.add(new ItemshopItem(results.getInt("id"), (results.getString("nickname")), game.getRegistry().getType(ItemType.class, results.getString("item")), results.getString("args")));
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
        return ret;
    }

    public ItemshopItem getItem(String name, int num) {
        AtomicReference<ItemshopItem> ret =  new AtomicReference<>();
        DatabaseUtil.query("SELECT * FROM `" + ITEMS_TABLE_NAME + "` WHERE `nickname`=\"" + name + "\" AND `id`="+num+";", results -> {
            try {
                if (results.next()) {
                    ret.set(new ItemshopItem(results.getInt("id"), (results.getString("nickname")), game.getRegistry().getType(ItemType.class, results.getString("item")), results.getString("args")));
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
        return ret.get();
    }
}