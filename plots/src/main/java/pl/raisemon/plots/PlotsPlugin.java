package pl.raisemon.plots;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import lombok.Getter;
import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Group;
import me.lucko.luckperms.api.Node;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import org.spongepowered.api.Game;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.EntityType;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.ApiPlugin;
import pl.raisemon.api.Configurable;
import pl.raisemon.plots.commands.PlotsCommand;
import pl.raisemon.plots.commands.RanchCommand;
import pl.raisemon.plots.listeners.*;
import pl.raisemon.plots.managers.RanchManager;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;
import pl.raisemon.plots.plots.RanchPlot;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Plugin(id = "raisemonplots", name = "RaisemonPlots", version = "1.0", description = "Plots",
        dependencies = {@Dependency(id = "raisemon_api"), @Dependency(id = "luckperms"), @Dependency(id="raisemonmod")})
public class PlotsPlugin implements Configurable {
    @Inject
    private Logger logger;
    @Inject
    private Game game;
    @Getter
    private static PlotsPlugin instance;

    @Getter
    private int size;
    @Getter
    private List<String> worlds;
    @Getter
    private String plotType;
    @Getter
    private String premiumPlotType;
    @Getter
    private long plotTime;
    @Getter
    private Set<UUID> bypassPlayers = new HashSet<>();
    @Getter
    private List<EntityType> blockedEntities = new ArrayList<>();
    private CommentedConfigurationNode node;

    @Getter
    private Location<World> ranchLocationMin;
    @Getter
    private Location<World> ranchLocationMax;
    @Getter
    private List<String> extraCommandsBlocked;

    public long getTimeFromItem(ItemStackSnapshot itemStackSnapshot) {
        final Optional<List<Text>> texts = itemStackSnapshot.get(Keys.ITEM_LORE);
        if(!texts.isPresent() || texts.get().size() == 0){
            return plotTime;
        }
        try{
            final String s = texts.get().get(texts.get().size() - 1).toPlain().split(": ")[1];
            return Utils.durationFromString(s);
        }catch (NumberFormatException | IndexOutOfBoundsException ex ){
            return plotTime;
        }
    }
    public long getTimeFromItem(ItemStack itemStackSnapshot) {
        return getTimeFromItem(itemStackSnapshot.createSnapshot());
    }


    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        WorldPlotManager.getInstance().load();
        RanchManager.getInstance().load();
        try {
            this.blockedEntities = node.getNode("plots", "blockedEntities").getList(TypeToken.of(String.class)).stream().map(n -> game.getRegistry().getType(EntityType.class, n).orElse(null)).collect(Collectors.toList());
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
        game.getScheduler().createTaskBuilder().execute(() -> WorldPlotManager.getInstance().getOutdatedPlots()
                .forEach(plot -> WorldPlotManager.getInstance().remove(plot)))
                .interval(1, TimeUnit.MINUTES).delay(5, TimeUnit.MINUTES).submit(this);
        Optional<World> world = Sponge.getServer().getWorld(node.getNode("ranch", "min", "world").getString());
        Optional<World> world2 = Sponge.getServer().getWorld(node.getNode("ranch", "max", "world").getString());
        ranchLocationMin = new Location<World>(world.get(), node.getNode("ranch", "min", "x").getInt(),
                node.getNode("ranch", "min", "y").getInt(), node.getNode("ranch", "min", "z").getInt());
        ranchLocationMax = new Location<World>(world2.get(), node.getNode("ranch", "max", "x").getInt(),
                node.getNode("ranch", "max", "y").getInt(), node.getNode("ranch", "max", "z").getInt());
    }

    @Listener
    public void onInit(GameInitializationEvent event) {
        node = ApiPlugin.getInstance().loadConfig(this);
        game.getEventManager().registerListeners(this, new ChangeBlockOnPlotListener());
        game.getEventManager().registerListeners(this, new InteractOnPlotListeners());
        game.getEventManager().registerListeners(this, new PlotCreateListeners());
        game.getEventManager().registerListeners(this, new PlotBreakListeners());

        game.getEventManager().registerListeners(this, new PlayerMoveListener());

        game.getEventManager().registerListeners(this, new PlotInfoListener());
        game.getEventManager().registerListeners(this, new PlayerJoinListener());

        game.getEventManager().registerListeners(this, new RanchListener());

        ApiPlugin.getInstance().registerCommand(new PlotsCommand(this));
        ApiPlugin.getInstance().registerCommand(new RanchCommand(this));

    }

    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        instance = this;
    }

    @Override
    public String getConfigName() {
        return "Plots";
    }

    @Override
    public void defaultConfig(CommentedConfigurationNode node) {
        node.getNode("plot", "worlds").setValue(Collections.singletonList("world"));
        node.getNode("plot", "plotType").setValue("raisemonmod:plot");
        node.getNode("plot", "premiumPlotType").setValue("raisemonmod:premium_plot");
        node.getNode("plot", "plotTime").setValue(Utils.durationToString(TimeUnit.DAYS.toSeconds(34)));
        node.getNode("plot", "plotSize").setValue(25);
        node.getNode("plots", "blockedEntities").setValue(Arrays.asList(
                EntityTypes.ARMOR_STAND.getId(),
                EntityTypes.ITEM_FRAME.getId(),
                EntityTypes.PAINTING.getId()
        ));
    }

    @Override
    public void load(CommentedConfigurationNode node) {
        try {
            this.worlds = node.getNode("plot", "worlds").getList(TypeToken.of(String.class));
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
        this.plotType = node.getNode("plot", "plotType").getString();
        this.premiumPlotType = node.getNode("plot", "premiumPlotType").getString();
        this.plotTime = Utils.durationFromString(node.getNode("plot", "plotTime").getString());
        this.size = node.getNode("plot", "plotSize").getInt(25);

        try {
            this.extraCommandsBlocked = node.getNode("blockedCommandsExtra").getList(TypeToken.of(String.class), Arrays.asList("sethome", "tpaccept"));
        } catch (ObjectMappingException e) {
            e.printStackTrace();
        }
    }


    public int getMaxPlayerPremiumPlots(Player player) {
        return LuckPerms.getApi().getGroups()
                .stream().filter(group -> player.hasPermission("group." + group.getName()))
                .flatMap((Function<Group, Stream<? extends Node>>) group -> group.getPermissions().stream())
                .map(Node::getPermission)
                .filter(permission -> permission.startsWith("pl.raisemon.plots."))
                .mapToInt(permission -> Integer.parseInt(permission.substring("pl.raisemon.plots.".length())))
                .max().orElse(0);
    }

    public Plot.PlotType getPlotType(ItemStackSnapshot itemStackSnapshot) {
        return itemStackSnapshot.getType().getName().equals(this.plotType)? Plot.PlotType.DEFAULT:(itemStackSnapshot.getType().getName().equals(this.premiumPlotType)? Plot.PlotType.PREMIUM:null);
    }

    public boolean bypass(UUID uniqueId) {
        final boolean contains = this.bypassPlayers.contains(uniqueId);
        if(contains){
            this.bypassPlayers.remove(uniqueId);
        }else{
            this.bypassPlayers.add(uniqueId);
        }
        return !contains;
    }

    public Plot.PlotType getPlotType(BlockState state) {
        return state.getType().getId().equals(this.plotType)? Plot.PlotType.DEFAULT : (state.getType().getId().equals(this.premiumPlotType)? Plot.PlotType.PREMIUM:null);
    }

    public boolean isRanchRegion(Location<World> location) {
        return location.getExtent().getName().equalsIgnoreCase(ranchLocationMin.getExtent().getName()) &&
                location.getBlockX() >= ranchLocationMin.getBlockX() &&
                location.getBlockZ() >= ranchLocationMin.getBlockZ() &&
                location.getBlockX() <= ranchLocationMax.getBlockX() &&
                location.getBlockZ() <= ranchLocationMax.getBlockZ();
    }

    //player standing inside ranch region and on plot with access
    public boolean haveRanchPermission(Player player, Location<World> location) {
        if(!location.getExtent().getName().equalsIgnoreCase(ranchLocationMin.getExtent().getName())){
            return false;
        }
        if (!isRanchRegion(location)) {
            return false;
        }
        Optional<RanchPlot> plot = RanchManager.getInstance().getPlot(location, false);
        if(!plot.isPresent()){
            return false;
        }
        return plot.get().hasAccess(player.getUniqueId());
    }

    public void createRanchPlot(Location<World> location, Player player) {
        RanchManager.getInstance().register(new RanchPlot(player.getUniqueId(), new Region(location, 9)));
    }

    public boolean canCreate(Player player, Location<World> location) {
        if (RanchManager.getInstance().getPlots(player.getUniqueId()).count() >= 3) {
            player.sendMessage(Text.of(TextColors.RED, "Mozesz posiadac max 3 ranchblocki"));
            return false;
        }
        return !RanchManager.getInstance().getPlot(location, false).isPresent();
    }

    public void setRanchMin(Location<World> location) {
        ranchLocationMin = location;
        saveRanch();
    }

    private void saveRanch() {
        if (ranchLocationMin!=null) {
            node.getNode("ranch", "min", "world").setValue(ranchLocationMin.getExtent().getName());
            node.getNode("ranch", "min", "x").setValue(ranchLocationMin.getBlockX());
            node.getNode("ranch", "min", "y").setValue(ranchLocationMin.getBlockY());
            node.getNode("ranch", "min", "z").setValue(ranchLocationMin.getBlockZ());
        }
        if (ranchLocationMax != null) {

            node.getNode("ranch", "max", "world").setValue(ranchLocationMax.getExtent().getName());
            node.getNode("ranch", "max", "x").setValue(ranchLocationMax.getBlockX());
            node.getNode("ranch", "max", "y").setValue(ranchLocationMax.getBlockY());
            node.getNode("ranch", "max", "z").setValue(ranchLocationMax.getBlockZ());
        }
        ApiPlugin.getInstance().saveConfig(this);
    }

    public void setRanchMax(Location<World> location) {
        ranchLocationMax = location;
        saveRanch();
    }
}
