package pl.raisemon.plots.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.plots.PlotsPlugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

public class RanchCommand extends BasePlayerCommand {
    public RanchCommand(PlotsPlugin plotsPlugin) {
        super(plotsPlugin, "RANCH");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("ranch");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Collection<BaseCommand> getChildren() {
        ArrayList<BaseCommand> baseCommands = new ArrayList<>();
        baseCommands.add(new Admin());
        return baseCommands;
    }

    private class Admin extends BasePlayerCommand {
        public Admin() {
            super(PlotsPlugin.getInstance(), "RANCH");
        }

        @Override
        public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
            if(args.<String>getOne("pos").get().equalsIgnoreCase("pos1")){
                PlotsPlugin.getInstance().setRanchMin(player.getLocation());
            }else{
                PlotsPlugin.getInstance().setRanchMax(player.getLocation());
            }
            return CommandResult.success();
        }

        @Override
        public List<String> getAliases() {
            return Collections.singletonList("admin");
        }

        @Override
        public String getPermission() {
            return null;
        }

        @Override
        public CommandElement[] getArguments() {
            return new CommandElement[]{
                    GenericArguments.choices(Text.of("pos"), () -> {

                        ArrayList<String> objects = new ArrayList<>();
                        objects.add("pos1");
                        objects.add("pos2");
                        return objects;
                    }, (Function<String, String>) s -> s)
            };
        }

        @Override
        public Collection<BaseCommand> getChildren() {
            return null;
        }
    }
}
