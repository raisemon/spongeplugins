package pl.raisemon.plots.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.PlotsPlugin;
import pl.raisemon.plots.commands.admin.AdminCommand;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PlotsCommand extends BaseCommand {
    public PlotsCommand(PlotsPlugin plotsPlugin) {
        super(plotsPlugin);
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("dzialka");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.commands.plot";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        Set<BaseCommand> c = new HashSet<>();
        c.add(new ChangeEntryAllowedCommand(getPlugin()));
        c.add(new ShowBorderCommand(getPlugin()));
        c.add(new ExpandTimeCommand(getPlugin()));
        c.add(new PlotRemoveMemberCommand(getPlugin()));
        c.add(new PlotAddMemberCommand(getPlugin()));
        c.add(new PlotListCommand(getPlugin()));
        c.add(new AdminCommand(getPlugin()));
        return c;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        MessageUtils.sendMessage(src, "", "commands.subCommandList", "/dzialka lista", "Lista Twoich dzialek");
        MessageUtils.sendMessage(src, "", "commands.subCommandList", "/dzialka przedluz", "Umozliwia przedluzenie dzialki premium");
        MessageUtils.sendMessage(src, "", "commands.subCommandList", "/dzialka zapros [nick]", "Dodaje gracza do dzialki na ktorej stoisz");
        MessageUtils.sendMessage(src, "", "commands.subCommandList", "/dzialka wywal [nick]", "Usuwa gracza z dzialki na ktorej stoisz");
        MessageUtils.sendMessage(src, "", "commands.subCommandList", "/dzialka granica", "Pokazuje granice dzialki");
        //MessageUtils.sendMessage(src, "", "commands.subCommandList", "/dzialka pvp"));
        if(src.hasPermission("pl.raisemon.commands.plot.admin")){
            MessageUtils.sendMessage(src, "", "commands.adminSubCommandList", "/dzialka admin", "Komendy administracyjne");
        }
        return CommandResult.success();
    }
}
