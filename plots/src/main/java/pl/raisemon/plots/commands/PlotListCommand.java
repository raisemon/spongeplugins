package pl.raisemon.plots.commands;

import pl.raisemon.plots.Utils;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;
import pl.raisemon.api.commands.BaseCommand;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.TextElement;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class PlotListCommand extends BaseCommand {
    public PlotListCommand(Object plugin) {
        super(plugin);
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("lista");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{
                GenericArguments.optional(GenericArguments.user(Text.of("user")))
        };
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        if (!(src instanceof Player) && !(args.hasAny("user"))) {
            return CommandResult.empty();
        }
        Optional<User> user = args.getOne("user");
        List<Plot> plots = null;
        if (user.isPresent() && src.hasPermission("pl.raisemon.commands.plots.listOthers")) {
            plots = WorldPlotManager.getInstance().getPlots(user.get().getUniqueId()).collect(Collectors.toList());
        } else if (src instanceof Player) {
            plots = WorldPlotManager.getInstance().getPlots(((Player) src).getUniqueId()).collect(Collectors.toList());
        }
        if (plots == null) {
            return CommandResult.empty();
        }
        src.sendMessage(TextSerializers.FORMATTING_CODE.deserialize(user.map(user1 -> "&3Dzialki gracza " + user1.getName()).orElse("&3Twoje dzialki:")));

        for (Plot plot : plots) {
            TextElement element = Text.of();
            if (src.hasPermission("pl.raisemon.commands.plot.teleport")) {
                element = Text.of(TextActions.executeCallback(commandSource -> {
                            if (commandSource instanceof Player) {
                                ((Player) commandSource).setLocation(plot.getRegion().getCenter().add(0, 1, 0));
                            }
                        }),
                        TextActions.showText(Text.of(TextColors.RED, "Kliknij by teleportować!")));
            }
            src.sendMessage(Text.of(
                    TextColors.DARK_AQUA, "Typ: ",
                    TextColors.AQUA, plot.getType() == Plot.PlotType.PREMIUM ? "premium " : "zwykla ",
                    Text.of(
                            element,
                            TextColors.DARK_AQUA, "Swiat: ",
                            TextColors.RED, plot.getRegion().getCenter().getExtent().getName(),
                            TextColors.DARK_AQUA, " X: ",
                            TextColors.RED, plot.getRegion().getCenter().getBlockX(),
                            TextColors.DARK_AQUA, " Z: ",
                            TextColors.RED, plot.getRegion().getCenter().getBlockZ()
                    ),
                    TextColors.DARK_AQUA, " Wygasa za:: ",
                    TextColors.RED, Utils.durationToString((int) ((plot.getExpireTime() - System.currentTimeMillis()) / 1000))
            ));
        }
        return CommandResult.success();
    }
}
