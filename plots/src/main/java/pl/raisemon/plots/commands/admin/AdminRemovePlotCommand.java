package pl.raisemon.plots.commands.admin;

import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.user.UserStorageService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.*;

public class AdminRemovePlotCommand extends BasePlayerCommand {
    private static final Map<UUID, String> confirm = new HashMap<>();
    public AdminRemovePlotCommand(Object plugin) {
        super(plugin, "Dzialka");
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {

        List<Plot> plots = WorldPlotManager.getInstance().getAllPlots(player.getLocation());
        if(plots.size()>1){
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize( "&7[&6Dzialki&7] &cStoisz na wiecej niz jednej dzialce, zglos sie do administratora!"));
            return CommandResult.empty();
        }
        if(plots.size() == 0){
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize( "&7[&6Dzialki&7] &cMusisz stac na terenie dzialki!"));
            return CommandResult.empty();
        }
        Plot plotByBlock = plots.get(0);
        UserStorageService userStorageService = Sponge.getServiceManager().provide(UserStorageService.class).get();
        if (!plotByBlock.getOwner().isPresent()) {
            WorldPlotManager.getInstance().remove(plotByBlock, true);
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize( "&7[&6Dzialki&7] &cUsunieto dzialke!"));
            return CommandResult.success();
        }
        String name = userStorageService.get(plotByBlock.getOwner().get()).get().getName();
        if( args.hasAny("confirm") && confirm.containsKey(player.getUniqueId())){
            if(!confirm.get(player.getUniqueId()).equalsIgnoreCase(args.<String>getOne("confirm").get()) ||
                    !confirm.get(player.getUniqueId()).equalsIgnoreCase(name)){
                player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize( "&cZly nick potwierdzający!"));
                return CommandResult.success();
            }
            WorldPlotManager.getInstance().remove(plotByBlock, true);
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize( "&7[&6Dzialki&7] &cUsunieto dzialke!"));
        }else{
            confirm.put(player.getUniqueId(), name);
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize( "&7[&6Dzialki&7] &aCzy na pewno chcesz skasowac dzialke gracza "+name+"? Aby potwierdzic wpisz &c/dzialka admin skasuj " + name));
        }

        return CommandResult.success();
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("skasuj");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{GenericArguments.optional(GenericArguments.string(Text.of("confirm")))};
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }
}
