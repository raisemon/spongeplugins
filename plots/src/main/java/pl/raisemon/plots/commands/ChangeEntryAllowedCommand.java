package pl.raisemon.plots.commands;

import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.entity.living.player.Player;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.plots.Plot;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ChangeEntryAllowedCommand extends PlotOwnerAccessCommand {
    public ChangeEntryAllowedCommand(Object plugin) {
        super(plugin, "Dzialka");
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("wejscie");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.commands.plots.entry";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult executePlotOwner(Player player, CommandContext args, Plot plotByBlock) {
        plotByBlock.changeEntry();
        if(plotByBlock.isEntryBlocked()){
            MessageUtils.sendMessage(player, "Dzialki", "plots.entryBlocked");
        }else{
            MessageUtils.sendMessage(player, "Dzialki", "plots.entryAllowed");
        }
        return CommandResult.success();
    }
}
