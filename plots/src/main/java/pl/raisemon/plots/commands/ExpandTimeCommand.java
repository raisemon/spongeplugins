package pl.raisemon.plots.commands;

import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.item.inventory.ItemStack;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.PlotsPlugin;
import pl.raisemon.plots.plots.Plot;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class ExpandTimeCommand extends PlotOwnerAccessCommand {
    public ExpandTimeCommand(Object plugin) {
        super(plugin, "Dzialka");
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("przedluz");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult executePlotOwner(Player player, CommandContext args, Plot plotByBlock) {

        if(plotByBlock.getType() != Plot.PlotType.PREMIUM){
            MessageUtils.sendMessage(player, "Dzialki", "plots.extendOnlyPremium");
            return CommandResult.empty();
        }
        Optional<ItemStack> itemInHand = player.getItemInHand(HandTypes.MAIN_HAND);

        if(!itemInHand.isPresent() || !itemInHand.get().getType().getName().equalsIgnoreCase(PlotsPlugin.getInstance().getPremiumPlotType())){
            MessageUtils.sendMessage(player, "Dzialki", "plots.noPremiumPlotInHand");
            return CommandResult.empty();
        }

        ItemStack itemStack = itemInHand.get();
        Long aLong = PlotsPlugin.getInstance().getTimeFromItem(itemStack);
        itemStack.setQuantity(itemStack.getQuantity()-1);
        plotByBlock.setExpireTime(plotByBlock.getExpireTime()+(aLong*1000));
        if(itemStack.getQuantity()<1){
            player.setItemInHand(HandTypes.MAIN_HAND, null);
        }else{
            player.setItemInHand(HandTypes.MAIN_HAND, itemStack);
        }
        MessageUtils.sendMessage(player, "Dzialki", "plots.premiumPlotExtended");
        return CommandResult.success();
    }
}
