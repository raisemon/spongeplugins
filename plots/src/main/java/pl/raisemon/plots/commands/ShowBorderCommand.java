package pl.raisemon.plots.commands;

import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.entity.living.player.Player;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.plots.Plot;

import java.util.Collections;
import java.util.List;
import java.util.Set;

public class ShowBorderCommand extends PlotAccessCommand {
    public ShowBorderCommand(Object plugin) {
        super(plugin, "Dzialka");
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("granica");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult executePlot(Player player, CommandContext args, Plot plotByBlock) {
        plotByBlock.showBorder(player);
        MessageUtils.sendMessage(player, "Dzialki", "plots.borderShowed");
        return CommandResult.success();
    }
}
