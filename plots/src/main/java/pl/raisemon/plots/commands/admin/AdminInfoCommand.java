package pl.raisemon.plots.commands.admin;

import pl.raisemon.plots.Utils;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.service.user.UserStorageService;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.serializer.TextSerializers;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AdminInfoCommand extends BasePlayerCommand {
    public AdminInfoCommand(Object plugin) {
        super(plugin, "Dzialka");
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("info");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        List<Plot> allPlotsByBlock = WorldPlotManager.getInstance().getAllPlots(player.getLocation());
        if(allPlotsByBlock.size() == 0){
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&7[&6Dzialki&7] &cNie stoisz na dzialce!"));
            return CommandResult.success();
        }
        for (Plot plotBySponge : allPlotsByBlock) {
            UserStorageService userStorageService = Sponge.getServiceManager().provide(UserStorageService.class).get();

            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&cDzialka gracza " + (plotBySponge.getOwner().isPresent() ? userStorageService.get(plotBySponge.getOwner().get()).get().getName() : "Brak")));
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&cDzialka " + (plotBySponge.getType() == Plot.PlotType.PREMIUM ? "premium" : "zwykla")));
            if (plotBySponge.getType() == Plot.PlotType.PREMIUM)
                player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&3Wygasa za: &b" + Utils.durationToString((int) ((plotBySponge.getExpireTime() - System.currentTimeMillis()) / 1000))));
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&3Wlasciciel: &b" + (plotBySponge.getOwner().isPresent() ? userStorageService.get(plotBySponge.getOwner().get()).get().getName() : "Brak")));
            String s = plotBySponge.getMembers().stream().map(uuid -> userStorageService.get(uuid).get().getName()).collect(Collectors.joining(", "));
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&3Czlonkowie: &b" + (s.length() > 0 ? s : "brak")));
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&3Wejscie: &b" + (plotBySponge.isEntryBlocked() ? "Zablokowane" : "Odblokowane")));
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&3X: &b" + plotBySponge.getRegion().getCenter().getBlockX() + " &3Z: &b " + plotBySponge.getRegion().getCenter().getBlockZ()));
            player.sendMessage(Text.of(""));
        }
        return CommandResult.success();
    }
}
