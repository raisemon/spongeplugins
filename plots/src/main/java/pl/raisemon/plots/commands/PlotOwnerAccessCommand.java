package pl.raisemon.plots.commands;

import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.entity.living.player.Player;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.plots.Plot;

public abstract class PlotOwnerAccessCommand extends PlotAccessCommand {
    public PlotOwnerAccessCommand(Object plugin, String tag) {
        super(plugin, tag);
    }

    @Override
    public CommandResult executePlot(Player player, CommandContext args, Plot plotByBlock) {
        if (!plotByBlock.getOwner().isPresent()) {
            MessageUtils.sendMessage(player, "Dzialki", "plots.noOwner");
            return CommandResult.empty();
        }
        if(!plotByBlock.getOwner().get().equals(player.getUniqueId())){
            MessageUtils.sendMessage(player, "Dzialki", "plots.notOwner");
            return CommandResult.empty();
        }
        return executePlotOwner(player, args, plotByBlock);
    }

    public abstract CommandResult executePlotOwner(Player player, CommandContext args, Plot plotByBlock);
}
