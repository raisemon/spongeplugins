package pl.raisemon.plots.commands.admin;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.serializer.TextSerializers;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.plots.PlotsPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AdminBypassCommand extends BasePlayerCommand {
    public AdminBypassCommand(Object plugin) {
        super(plugin, "Dzialka");
    }

    @Override
    public List<String> getAliases() {
        List<String> l = new ArrayList<>();
        l.add("bypass");
        l.add("b");
        return l;
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {

        if (PlotsPlugin.getInstance().bypass(player.getUniqueId())) {
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize( "&7[&6Dzialki&7] &cWlaczyles modyfikowanie dzialek graczy!"));
        }else{
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize( "&7[&6Dzialki&7] &aWylaczyles modyfikowanie dzialek graczy!"));
        }
        return CommandResult.success();
    }
}
