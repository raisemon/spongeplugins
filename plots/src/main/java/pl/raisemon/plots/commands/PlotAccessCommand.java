package pl.raisemon.plots.commands;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.entity.living.player.Player;
import pl.raisemon.api.commands.BasePlayerCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;

import java.util.List;

public abstract class PlotAccessCommand extends BasePlayerCommand {

    public PlotAccessCommand(Object plugin, String tag) {
        super(plugin, tag);
    }

    @Override
    public CommandResult executePlayer(Player player, CommandContext args) throws CommandException {
        List<Plot> plots = WorldPlotManager.getInstance().getAllPlots(player.getLocation());
        if(plots.size()>1){
            MessageUtils.sendMessage(player, "Dzialki", "plots.standingOnMoreThanOnePlot");
            return CommandResult.empty();
        }
        if(plots.size() == 0){
            MessageUtils.sendMessage(player, "Dzialki", "plots.standingOutsidePlot");
            return CommandResult.empty();
        }
        Plot plotByBlock = plots.get(0);
        return executePlot(player, args, plotByBlock);
    }

    public abstract CommandResult executePlot(Player player, CommandContext args, Plot plotByBlock);
}
