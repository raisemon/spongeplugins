package pl.raisemon.plots.commands.admin;

import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AdminCommand extends BaseCommand {
    public AdminCommand(Object plugin) {
        super(plugin);
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("admin");
    }

    @Override
    public String getPermission() {
        return "pl.raisemon.permissions.plots.admin";
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[0];
    }

    @Override
    public Set<BaseCommand> getChildren() {
        Set<BaseCommand> c = new HashSet<>();
        c.add(new AdminRemovePlotCommand(getPlugin()));
        c.add(new AdminBypassCommand(getPlugin()));
        c.add(new AdminInfoCommand(getPlugin()));
        return c;
    }

    @Override
    public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
        MessageUtils.sendMessage(src, "", "commands.adminSubCommandList", "/dzialka admin skasuj", "Umozliwia skasowanie dzialki na ktorej stoisz");
        MessageUtils.sendMessage(src, "", "commands.adminSubCommandList", "/dzialka admin bypass", "Wlacza mozliwosc ingerencji w dzialke gracza");
        MessageUtils.sendMessage(src, "", "commands.adminSubCommandList", "/dzialka admin info", "Pokazuje informacje na temat dzialki");
        return CommandResult.success();
    }
}
