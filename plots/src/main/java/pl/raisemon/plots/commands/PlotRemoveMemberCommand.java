package pl.raisemon.plots.commands;

import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.CommandElement;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import pl.raisemon.api.commands.BaseCommand;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.plots.Plot;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class PlotRemoveMemberCommand extends PlotOwnerAccessCommand {
    public PlotRemoveMemberCommand(Object o) {
        super(o, "Dzialka");
    }

    @Override
    public List<String> getAliases() {
        return Collections.singletonList("wywal");
    }

    @Override
    public String getPermission() {
        return null;
    }

    @Override
    public CommandElement[] getArguments() {
        return new CommandElement[]{GenericArguments.user(Text.of("user"))};
    }

    @Override
    public Set<BaseCommand> getChildren() {
        return null;
    }

    @Override
    public CommandResult executePlotOwner(Player player, CommandContext args, Plot plotByBlock) {
        Optional<User> user = args.getOne("user");
        if(plotByBlock.removeMember(user.get().getUniqueId())){
            MessageUtils.sendMessage(player, "Dzialki", "plots.memberRemoved");
        }else{
            MessageUtils.sendMessage(player, "Dzialki", "plots.noMember");
        }
        return CommandResult.success();
    }
}
