package pl.raisemon.plots;

import com.flowpowered.math.vector.Vector3d;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;

import java.util.Optional;

public class PlayerMoveListener {

    @Listener
    public void handle(MoveEntityEvent event) throws Exception {
        if (!(event.getTargetEntity() instanceof Player) && event.getTargetEntity().getPassengers().stream().noneMatch(e -> e instanceof Player)) {
            return;
        }
        Vector3d from = event.getFromTransform().getPosition();
        Vector3d to = event.getToTransform().getPosition();
        Player targetEntity;
        if(event.getTargetEntity() instanceof Player){
            targetEntity = (Player) event.getTargetEntity();
        }else{
            targetEntity = (Player) event.getTargetEntity().getPassengers().stream().filter(e -> e instanceof Player).findAny().get();
        }
        if(from.getFloorX() != to.getFloorX() ||
                from.getFloorZ() != to.getFloorZ()){
            Optional<Plot> plotByBlock = WorldPlotManager.getInstance().getPlot(event.getToTransform().getLocation(), false);
            if(plotByBlock.isPresent()){
                if(plotByBlock.get().isEntryBlocked()){
                    if(!plotByBlock.get().hasAccess(targetEntity.getUniqueId())){
                        if(!targetEntity.hasPermission("pl.raisemon.moveOnBlockedPlot")) {
                            MessageUtils.sendMessage(targetEntity, "Dzialki", "plots.moveOnBlockedPlot");
                            event.setCancelled(true);
                        }
                    }
                }
            }
        }
    }
}
