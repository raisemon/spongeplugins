package pl.raisemon.plots;

import com.pixelmonmod.pixelmon.entities.custom.EntityPixelmonPainting;
import net.foxdenstudio.sponge.foxguard.plugin.FGManager;
import net.foxdenstudio.sponge.foxguard.plugin.handler.GlobalHandler;
import net.foxdenstudio.sponge.foxguard.plugin.handler.IHandler;
import net.foxdenstudio.sponge.foxguard.plugin.region.IRegion;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.hanging.ItemFrame;
import org.spongepowered.api.entity.hanging.Painting;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.Boat;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.entity.DamageEntityEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.managers.WorldPlotManager;

import java.util.Optional;
import java.util.Set;

public class InteractOnPlotListeners {
    @Listener(order = Order.FIRST)
    public void interactOnPlot(InteractBlockEvent event, @First Player player) {
        BlockType type = event.getTargetBlock().getState().getType();

        Optional<Location<World>> location = event.getTargetBlock().getLocation();
        if (!location.isPresent()) {
            return;
        }
        if(type.getId().contains("customnpcs") ||
                type.getId().contains("hopper") ||
                type.getId().contains("minecraft:anvil")) {
            Set<IRegion> regionsAtPos = FGManager.getInstance().getRegionsAtPos(location.get().getExtent(), location.get().getBlockPosition());
            for (IRegion rgAtPos : regionsAtPos) {
                for (IHandler handler : rgAtPos.getHandlers()) {
                    if (!(handler instanceof GlobalHandler)) {
                        if (!player.hasPermission("pl.raisemon.admin")) {
                            event.setCancelled(true);
                            return;
                        }
                    }
                }
            }
        }
        if (WorldPlotManager.getInstance().hasAccessAtLocation(location.get(), player.getUniqueId())) {
            //player has access
            return;
        }

        if (PlotsPlugin.getInstance().getBypassPlayers().contains(player.getUniqueId())) {
            //player has bypass
            return;
        }

        if (location.get().getBlock().getType().getName().toLowerCase().contains("door") ||
                location.get().getBlock().getType().getName().toLowerCase().contains("chest")) {
            if (player.hasPermission("pl.raisemon.interactonplot")) {
                return;
            }
        }
        if(location.get().getBlock().getType().getName().toLowerCase().equals("pixelmon:pc") ||
                location.get().getBlock().getType().getName().toLowerCase().equals("pixelmon:trade_machine") ||
                location.get().getBlock().getType().getName().toLowerCase().equals("pixelmon:healer")){
            return;
        }
        event.setCancelled(true);
        if(event instanceof InteractBlockEvent.Secondary.MainHand || event instanceof InteractBlockEvent.Primary.MainHand) {
            MessageUtils.sendMessage(player, "Dzialki", "plots.noPermissions");
        }
    }

    @Listener(order = Order.FIRST)
    public void interactOnPlot(InteractEntityEvent event, @First Player player) {
        Location<World> location = event.getTargetEntity().getLocation();

        if(event.getTargetEntity() instanceof ItemFrame ||
                event.getTargetEntity() instanceof ArmorStand ||
                event.getTargetEntity() instanceof Painting ||
                event.getTargetEntity() instanceof EntityPixelmonPainting ||
                event.getTargetEntity() instanceof Minecart) {
            Set<IRegion> regionsAtPos = FGManager.getInstance().getRegionsAtPos(location.getExtent(), location.getBlockPosition());
            for (IRegion rgAtPos : regionsAtPos) {
                for (IHandler handler : rgAtPos.getHandlers()) {
                    if (!(handler instanceof GlobalHandler)) {
                        if (!player.hasPermission("pl.raisemon.admin")) {
                            event.setCancelled(true);
                            return;
                        }
                    }
                }
            }
        }


        if ((!WorldPlotManager.getInstance().hasAccessAtLocation(location, player.getUniqueId())
                ) &&
                !PlotsPlugin.getInstance().getBypassPlayers().contains(player.getUniqueId())
        ) {
            if (PlotsPlugin.getInstance().getBlockedEntities().contains(event.getTargetEntity().getType()) ||
                    event.getTargetEntity() instanceof ItemFrame ||
                    event.getTargetEntity() instanceof ArmorStand ||
                    event.getTargetEntity() instanceof Painting ||
                    event.getTargetEntity() instanceof EntityPixelmonPainting ||
                    event.getTargetEntity() instanceof Minecart) {
                event.setCancelled(true);
                MessageUtils.sendMessage(player, "Dzialki", "plots.noPremissions");
            }
        }
    }

    @Listener(order = Order.FIRST)
    public void interactOnPlot(DamageEntityEvent event, @First Player player) {
        Location<World> location = event.getTargetEntity().getLocation();

        if(event.getTargetEntity() instanceof ItemFrame ||
                event.getTargetEntity() instanceof ArmorStand ||
                event.getTargetEntity() instanceof Painting ||
                event.getTargetEntity() instanceof EntityPixelmonPainting ||
                event.getTargetEntity() instanceof Minecart) {
            if ((!WorldPlotManager.getInstance().hasAccessAtLocation(location, player.getUniqueId())
            ) &&
                    !PlotsPlugin.getInstance().getBypassPlayers().contains(player.getUniqueId())) {
                if (PlotsPlugin.getInstance().getBlockedEntities().contains(event.getTargetEntity().getType())) {
                    event.setCancelled(true);
                    MessageUtils.sendMessage(player, "Dzialki", "plots.noPremissions");
                }
            }
        }

    }

    @Listener(order = Order.FIRST)
    public void spawn(SpawnEntityEvent event, @First Player player) {
        for (Entity entity : event.getEntities()) {
            if(entity instanceof Boat || entity instanceof ItemFrame || entity instanceof ArmorStand) {
                Location<World> location = entity.getLocation();
                Set<IRegion> regionsAtPos = FGManager.getInstance().getRegionsAtPos(location.getExtent(), location.getBlockPosition());
                for (IRegion rgAtPos : regionsAtPos) {
                    for (IHandler handler : rgAtPos.getHandlers()) {
                        if (!(handler instanceof GlobalHandler)) {
                            if (!player.hasPermission("pl.raisemon.admin")) {
                                event.setCancelled(true);
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}
