package pl.raisemon.plots.managers;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.utils.DatabaseUtil;
import pl.raisemon.plots.plots.RanchPlot;
import pl.raisemon.plots.Region;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RanchManager {
    public static final String PLOTS_TABLE_NAME = "rsm_ranch";
    public static final String PLOT_MEMBERS_TABLE_NAME = "rsm_ranch_members";
    private static RanchManager instance;
    private Set<RanchPlot> plots = new HashSet<>();

    public static RanchManager getInstance() {
        if (instance == null) {
            instance = new RanchManager();
        }
        return instance;
    }

    public RanchManager() {
        DatabaseUtil.execute("CREATE TABLE IF NOT EXISTS `" + PLOTS_TABLE_NAME + "` (" +
                "`id` INT NOT NULL AUTO_INCREMENT," +
                "`uuid` varchar(36) ," +
                "`world` VARCHAR(32) NOT NULL, " +
                "`center_x` INT NOT NULL," +
                "`center_y` INT NOT NULL," +
                "`center_z` INT NOT NULL," +
                "PRIMARY KEY (`id`)" +
                ");");
        DatabaseUtil.execute("CREATE TABLE IF NOT EXISTS `" + PLOT_MEMBERS_TABLE_NAME + "` (" +
                "`id` INT NOT NULL AUTO_INCREMENT," +
                "`plot_id` INT NOT NULL," +
                "`uuid` VARCHAR(36) NOT NULL, " +
                "PRIMARY KEY (`id`)" +
                ");");
    }

    public void load() {
        DatabaseUtil.query("SELECT * FROM `" + PLOTS_TABLE_NAME + "`;", results -> {
            try {
                while (results.next()) {
                    String owner = results.getString("uuid");
                    Optional<World> world = Sponge.getServer().getWorld(results.getString("world"));
                    if (!world.isPresent()) {
                        continue;
                    }
                    RanchPlot plot = new RanchPlot(
                            owner.length() > 0 ? UUID.fromString(owner) : null,
                            new Region(
                                    world.get().getLocation(results.getInt("center_x"), results.getInt("center_y"), results.getInt("center_z")), 9
                            )
                    );
                    plot.setDbId(results.getLong("id"));

                    DatabaseUtil.query("SELECT * FROM `" + PLOT_MEMBERS_TABLE_NAME + "` WHERE `plot_id` = " + plot.getDbId() + ";",
                            resultsM -> {

                                try {
                                    while (resultsM.next()) {
                                        plot.getMembers().add(UUID.fromString(resultsM.getString("uuid")));
                                    }
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            });
                    plots.add(plot);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }
    public void save(RanchPlot plot) {
        long i = DatabaseUtil.insert("INSERT INTO `" + PLOTS_TABLE_NAME + "`" +
                "(`uuid`,`world`,`center_x`,`center_y`,`center_z`) VALUES" +
                "(?, ?, ?, ?, ?)", stmt->{
            try{
                stmt.setString(1, plot.getOwner().isPresent()? plot.getOwner().get().toString(): "");
                stmt.setString(2, plot.getRegion().getCenter().getExtent().getName());
                stmt.setInt(3, plot.getRegion().getCenter().getBlockX());
                stmt.setInt(4, plot.getRegion().getCenter().getBlockY());
                stmt.setInt(5, plot.getRegion().getCenter().getBlockZ());
            }catch(SQLException ex){
                ex.printStackTrace();
            }
        });
        plot.setDbId(i);

    }
    public void register(RanchPlot plot) {
        save(plot);
        plots.add(plot);
    }

    public Stream<RanchPlot> getPlots(UUID uniqueId) {
        return plots.stream().filter(plot -> plot.getOwner().isPresent() && plot.getOwner().get().equals(uniqueId));
    }


    public void remove(RanchPlot plot) {
        remove(plot, true);
    }

    public void remove(RanchPlot plot, boolean blockChange) {
        if (blockChange) {
            plot.getRegion().getCenter().setBlockType(BlockTypes.AIR);
        }
        DatabaseUtil.execute("DELETE FROM `" + PLOT_MEMBERS_TABLE_NAME + "` WHERE `plot_id`='" + plot.getDbId() + "';");
        DatabaseUtil.execute("DELETE FROM `" + PLOTS_TABLE_NAME + "` WHERE `id`='" + plot.getDbId() + "';");
        plots.remove(plot);
    }

    public boolean intersectsAnyRegion(Region region) {
        return plots.stream().anyMatch(p -> p.getRegion().getCenter().getExtent().getName().equals(region.getCenter().getExtent().getName()) &&
                p.getRegion().intersects(region));
    }
    public List<RanchPlot> getIntercectedRegions(Region region) {
        return plots.stream().filter(p -> p.getRegion().getCenter().getExtent().getName().equals(region.getCenter().getExtent().getName()) &&
                p.getRegion().intersects(region)).collect(Collectors.toList());
    }

    public Optional<RanchPlot> getPlot(Location<World> spongeLocation, boolean center) {
        if(center){
            return plots.stream().filter(plot -> plot.getRegion().getCenter().equals(spongeLocation)).findAny();
        }
        return plots.stream().filter(plot ->{
            final Location<World> min = plot.getRegion().getMin();
            final Location<World> max = plot.getRegion().getMax();
            return min.getExtent().getName().equals(spongeLocation.getExtent().getName()) &&
                    min.getBlockX()<=spongeLocation.getBlockX() && max.getBlockX()>=spongeLocation.getBlockX() &&
                    min.getBlockZ()<=spongeLocation.getBlockZ() && max.getBlockZ()>=spongeLocation.getBlockZ();
        }).findAny();
    }

    public boolean hasAccessAtLocation(Location<World> worldLocation, UUID uniqueId) {
        Optional<RanchPlot> plotByBlock = getPlot(worldLocation, false);
        return !plotByBlock.isPresent() || plotByBlock.get().hasAccess(uniqueId);
    }

    public List<RanchPlot> getAllPlots(Location<World> spongeLocation) {
        return plots.stream().filter(plot ->{
            final Location<World> min = plot.getRegion().getMin();
            final Location<World> max = plot.getRegion().getMax();
            return min.getExtent().getName().equals(spongeLocation.getExtent().getName()) &&
                    min.getBlockX()<=spongeLocation.getBlockX() && max.getBlockX()>=spongeLocation.getBlockX() &&
                    min.getBlockZ()<=spongeLocation.getBlockZ() && max.getBlockZ()>=spongeLocation.getBlockZ();
        }).collect(Collectors.toList());
    }
}
