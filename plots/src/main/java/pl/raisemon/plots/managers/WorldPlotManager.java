package pl.raisemon.plots.managers;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.utils.DatabaseUtil;
import pl.raisemon.plots.Region;
import pl.raisemon.plots.plots.Plot;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class WorldPlotManager {
    public static final String PLOTS_TABLE_NAME = "rsm_plots";
    public static final String PLOT_MEMBERS_TABLE_NAME = "rsm_plot_members";
    private static WorldPlotManager instance;
    private Set<Plot> plots = new HashSet<>();

    public static WorldPlotManager getInstance() {
        if (instance == null) {
            instance = new WorldPlotManager();
        }
        return instance;
    }

    public WorldPlotManager() {
        DatabaseUtil.execute("CREATE TABLE IF NOT EXISTS `" + PLOTS_TABLE_NAME + "` (" +
                "`id` INT NOT NULL AUTO_INCREMENT," +
                "`uuid` varchar(36) ," +
                "`world` VARCHAR(32) NOT NULL, " +
                "`center_x` INT NOT NULL," +
                "`center_y` INT NOT NULL," +
                "`center_z` INT NOT NULL," +
                "`min_x` INT NOT NULL," +
                "`min_z` INT NOT NULL," +
                "`max_x` INT NOT NULL," +
                "`max_z` INT NOT NULL," +
                "`type` VARCHAR(10) NOT NULL," +
                "`expire` BIGINT," +
                "`entry` BOOLEAN," +
                "PRIMARY KEY (`id`)" +
                ");");
        DatabaseUtil.execute("CREATE TABLE IF NOT EXISTS `" + PLOT_MEMBERS_TABLE_NAME + "` (" +
                "`id` INT NOT NULL AUTO_INCREMENT," +
                "`plot_id` INT NOT NULL," +
                "`uuid` VARCHAR(36) NOT NULL, " +
                "PRIMARY KEY (`id`)" +
                ");");
    }

    public void load() {
        DatabaseUtil.query("SELECT * FROM `" + PLOTS_TABLE_NAME + "`;", results -> {
            try {
                while (results.next()) {
                    String owner = results.getString("uuid");
                    Optional<World> world = Sponge.getServer().getWorld(results.getString("world"));
                    if (!world.isPresent()) {
                        continue;
                    }
                    Plot plot = new Plot(
                            owner.length() > 0 ? UUID.fromString(owner) : null,
                            new Region(
                                    world.get().getLocation(results.getInt("center_x"), results.getInt("center_y"), results.getInt("center_z")),
                                    world.get().getLocation(results.getInt("min_x"), 0, results.getInt("min_z")),
                                    world.get().getLocation(results.getInt("max_x"), 255, results.getInt("max_z"))
                            ),
                            Plot.PlotType.valueOf(results.getString("type")),
                            results.getLong("expire")
                    );
                    plot.setDbId(results.getLong("id"));
                    plot.setEntryBlocked(results.getBoolean("entry"));

                    DatabaseUtil.query("SELECT * FROM `" + PLOT_MEMBERS_TABLE_NAME + "` WHERE `plot_id` = " + plot.getDbId() + ";",
                            resultsM -> {

                                try {
                                    while (resultsM.next()) {
                                        plot.getMembers().add(UUID.fromString(resultsM.getString("uuid")));
                                    }
                                } catch (SQLException e) {
                                    e.printStackTrace();
                                }
                            });
                    plots.add(plot);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }
    public void save(Plot plot) {
        if(plot.getDbId() == -1){
            long i = DatabaseUtil.insert("INSERT INTO `" + PLOTS_TABLE_NAME + "`" +
                    "(`uuid`,`world`,`center_x`,`center_y`,`center_z`,`min_x`,`min_z`,`max_x`,`max_z`,`type`,`expire`,`entry`) VALUES" +
                    "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", stmt->{
                try{
                    stmt.setString(1, plot.getOwner().isPresent()? plot.getOwner().get().toString(): "");
                    stmt.setString(2, plot.getRegion().getCenter().getExtent().getName());
                    stmt.setInt(3, plot.getRegion().getCenter().getBlockX());
                    stmt.setInt(4, plot.getRegion().getCenter().getBlockY());
                    stmt.setInt(5, plot.getRegion().getCenter().getBlockZ());
                    stmt.setInt(6, plot.getRegion().getMin().getBlockX());
                    stmt.setInt(7, plot.getRegion().getMin().getBlockZ());
                    stmt.setInt(8, plot.getRegion().getMax().getBlockX());
                    stmt.setInt(9, plot.getRegion().getMax().getBlockZ());
                    stmt.setString(10, plot.getType().name());
                    stmt.setLong(11, plot.getExpireTime());
                    stmt.setBoolean(12, plot.isEntryBlocked());
                }catch(SQLException ex){
                    ex.printStackTrace();
                }
            });
            plot.setDbId(i);


        }else{
            DatabaseUtil.executeUpdate("UPDATE `" + PLOTS_TABLE_NAME + "`" +
                    " SET `uuid` = ?, `expire`= ?,`entry` =?, `type`=? WHERE `id`=?", stmt->{
                try{
                    stmt.setString(1, plot.getOwner().isPresent()? plot.getOwner().get().toString(): "");
                    stmt.setLong(2, plot.getExpireTime());
                    stmt.setBoolean(3, plot.isEntryBlocked());
                    stmt.setString(4, plot.getType().name());
                    stmt.setLong(5, plot.getDbId());
                }catch(SQLException ex){
                    ex.printStackTrace();
                }
            });
        }

    }
    public void register(Plot plot) {
        save(plot);
        plots.add(plot);
    }

    public Stream<Plot> getPlots(UUID uniqueId) {
        return plots.stream().filter(plot -> plot.getOwner().isPresent() && plot.getOwner().get().equals(uniqueId));
    }


    public void remove(Plot plot) {
        remove(plot, true);
    }

    public void remove(Plot plot, boolean blockChange) {
        if (blockChange) {
            plot.getRegion().getCenter().setBlockType(BlockTypes.AIR);
        }
        DatabaseUtil.execute("DELETE FROM `" + PLOT_MEMBERS_TABLE_NAME + "` WHERE `plot_id`='" + plot.getDbId() + "';");
        DatabaseUtil.execute("DELETE FROM `" + PLOTS_TABLE_NAME + "` WHERE `id`='" + plot.getDbId() + "';");
        plots.remove(plot);
    }

    public boolean intersectsAnyRegion(Region region) {
        return plots.stream().anyMatch(p -> p.getRegion().getCenter().getExtent().getName().equals(region.getCenter().getExtent().getName()) &&
                p.getRegion().intersects(region));
    }
    public List<Plot> getIntercectedRegions(Region region) {
        return plots.stream().filter(p -> p.getRegion().getCenter().getExtent().getName().equals(region.getCenter().getExtent().getName()) &&
                p.getRegion().intersects(region)).collect(Collectors.toList());
    }

    public Optional<Plot> getPlot(Location<World> spongeLocation, boolean center) {
        if(center){
            return plots.stream().filter(plot -> plot.getRegion().getCenter().equals(spongeLocation)).findAny();
        }
        return plots.stream().filter(plot ->{
            final Location<World> min = plot.getRegion().getMin();
            final Location<World> max = plot.getRegion().getMax();
            return min.getExtent().getName().equals(spongeLocation.getExtent().getName()) &&
                    min.getBlockX()<=spongeLocation.getBlockX() && max.getBlockX()>=spongeLocation.getBlockX() &&
                    min.getBlockZ()<=spongeLocation.getBlockZ() && max.getBlockZ()>=spongeLocation.getBlockZ();
        }).findAny();
    }

    public boolean hasAccessAtLocation(Location<World> worldLocation, UUID uniqueId) {
        Optional<Plot> plotByBlock = getPlot(worldLocation, false);
        return !plotByBlock.isPresent() || plotByBlock.get().hasAccess(uniqueId);
    }

    public List<Plot> getOutdatedPlots() {
        return plots.stream().filter(Plot::isExpired).collect(Collectors.toList());
    }

    public List<Plot> getAllPlots(Location<World> spongeLocation) {
        return plots.stream().filter(plot ->{
            final Location<World> min = plot.getRegion().getMin();
            final Location<World> max = plot.getRegion().getMax();
            return min.getExtent().getName().equals(spongeLocation.getExtent().getName()) &&
                    min.getBlockX()<=spongeLocation.getBlockX() && max.getBlockX()>=spongeLocation.getBlockX() &&
                    min.getBlockZ()<=spongeLocation.getBlockZ() && max.getBlockZ()>=spongeLocation.getBlockZ();
        }).collect(Collectors.toList());
    }
}
