package pl.raisemon.plots;

import lombok.Getter;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public class Region {
    @Getter
    private Location<World> center;
    @Getter
    private Location<World> min;
    @Getter
    private Location<World> max;

    public Region(){}

    public Region(Location<World> center) {
        this.center = center.copy();
        final int size = PlotsPlugin.getInstance().getSize();
        this.min = new Location<>(center.getExtent(), center.getBlockX() - size, 0, center.getBlockZ() - size);
        this.max = new Location<>(center.getExtent(), center.getBlockX() + size, 255, center.getBlockZ() + size);
    }

    public Region(Location<World> center, int size) {
        this.center = center.copy();
        this.min = new Location<>(center.getExtent(), center.getBlockX() - size, 0, center.getBlockZ() - size);
        this.max = new Location<>(center.getExtent(), center.getBlockX() + size, 255, center.getBlockZ() + size);
    }

    public Region(Location<World> center, Location<World> min, Location<World> max) {
        this.center = center;
        this.min = min;
        this.max = max;
    }

    public boolean intersects(Region anotherRegion){
        return (this.getMax().getBlockX() >= anotherRegion.getMin().getBlockX() &&
                this.getMin().getBlockX() <= anotherRegion.getMax().getBlockX() &&
                this.getMax().getBlockZ() >= anotherRegion.getMin().getBlockZ() &&
                this.getMin().getBlockZ() <= anotherRegion.getMax().getBlockZ());
    }



    public boolean intersects(int minx, int minz, int maxx, int maxz) {
        return (this.getMax().getBlockX() >= minx &&
                this.getMin().getBlockX() <= maxx &&
                this.getMax().getBlockZ() >= minz &&
                this.getMin().getBlockZ() <= maxz);
    }
}
