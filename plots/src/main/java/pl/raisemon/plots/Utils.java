package pl.raisemon.plots;

public class Utils {

    public static String durationToString(long duration) {
        long s = duration % 60;
        duration /= 60;
        long m = duration % 60;
        duration /= 60;
        long h = duration % 24;
        duration /= 24;
        long d = duration;
        return d + "d " + h + "h " + m + "m " + s + "s";
    }


    public static long durationFromString(String lastLine) {
        if (!lastLine.matches("[0-9]{1,3}d [0-9]{1,2}h [0-9]{1,2}m [0-9]{1,2}s")) {
            return 0;
        }
        String[] split = lastLine.split(" ");
        int[] ints = new int[split.length];
        for (int i = 0; i < split.length; i++) {
            ints[i] = Integer.parseInt(split[i].substring(0, split[i].length() - 1));
        }
        return ((ints[0] * 24 + ints[1]) * 60 + ints[2]) * 60 + ints[3];
    }

}
