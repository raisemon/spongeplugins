package pl.raisemon.plots.plots;

import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.utils.DatabaseUtil;
import pl.raisemon.plots.PlotsPlugin;
import pl.raisemon.plots.Region;
import pl.raisemon.plots.managers.WorldPlotManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ConfigSerializable
public class Plot {
    private UUID owner;
    private Region region;
    private PlotType type;
    private long expireTime;
    private List<UUID> members = new ArrayList<>();
    private boolean entryBlocked = false;
    private long dbId = -1;

    //Only for deserialization!
    public Plot(){}

    public Plot(Player player, Region region, PlotType type) {
        this(player, region, type, defaultTime());
    }

    public static long defaultTime() {
        return System.currentTimeMillis() + PlotsPlugin.getInstance().getPlotTime()*1000;
    }

    public Plot(Player player, Region region, PlotType type, long expireTime) {
        this.owner = player.getUniqueId();
        this.region = region;
        this.type = type;
        this.expireTime = expireTime;
    }
    public Plot(UUID owner, Region region, PlotType type) {
        this(owner,region,type,defaultTime());
    }
    public Plot(UUID owner, Region region, PlotType type, long expireTime) {
        this.owner = owner;
        this.region = region;
        this.type = type;
        this.expireTime = expireTime;
    }

    public Region getRegion() {
        return region;
    }

    public PlotType getType() {
        return type;
    }

    public Optional<UUID> getOwner() {
        return Optional.ofNullable(owner);
    }

    public long getExpireTime() {
        return expireTime;
    }

    public boolean isExpired() {
        return getExpireTime() < System.currentTimeMillis();
    }

    public List<UUID> getMembers() {
        return members;
    }

    public boolean hasMember(UUID o) {
        return members.contains(o);
    }

    public boolean addMember(UUID uuid) {
        if(members.contains(uuid)){
            return false;
        }
        if(uuid == owner){
            return false;
        }
        boolean b = members.add(uuid);
        DatabaseUtil.insert("INSERT INTO `" + WorldPlotManager.PLOT_MEMBERS_TABLE_NAME + "`" +
                "(`plot_id`,`uuid`) VALUES" +
                "(?, ?)", stmt->{
            try{
                stmt.setLong(1, Plot.this.getDbId());
                stmt.setString(2, uuid.toString());

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        return b;
    }

    public boolean removeMember(UUID uuid) {
        if (!members.contains(uuid)) {
            return false;
        }

        boolean b = members.remove(uuid);
        DatabaseUtil.execute("DELETE FROM `" + WorldPlotManager.PLOT_MEMBERS_TABLE_NAME + "` WHERE `plot_id`='" + this.getDbId() + "' AND `uuid`=\""+uuid.toString()+"\";");
        return b;
    }

    public boolean hasAccess(UUID uniqueId) {
        return owner != null && (owner.equals(uniqueId) || hasMember(uniqueId));
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
        WorldPlotManager.getInstance().save(this);
    }

    public void showBorder(Player player) {
        Location<World> min = this.region.getMin();
        Location<World> max = this.region.getMax();
        Location<World> center = this.region.getCenter();
        World extent = min.getExtent();
        showBorderBlock(extent, min.getBlockX(), min.getBlockZ(), BlockTypes.GLOWSTONE, player);
        showBorderBlock(extent, min.getBlockX()+1, min.getBlockZ(), BlockTypes.GOLD_BLOCK, player);
        showBorderBlock(extent, min.getBlockX(), min.getBlockZ()+1, BlockTypes.GOLD_BLOCK, player);

        showBorderBlock(extent, max.getBlockX(), max.getBlockZ(), BlockTypes.GLOWSTONE, player);
        showBorderBlock(extent, max.getBlockX()-1, max.getBlockZ(), BlockTypes.GOLD_BLOCK, player);
        showBorderBlock(extent, max.getBlockX(), max.getBlockZ()-1, BlockTypes.GOLD_BLOCK, player);

        showBorderBlock(extent, min.getBlockX(), max.getBlockZ(), BlockTypes.GLOWSTONE, player);
        showBorderBlock(extent, min.getBlockX()+1, max.getBlockZ(), BlockTypes.GOLD_BLOCK, player);
        showBorderBlock(extent, min.getBlockX(), max.getBlockZ()-1, BlockTypes.GOLD_BLOCK, player);

        showBorderBlock(extent, max.getBlockX(), min.getBlockZ(), BlockTypes.GLOWSTONE, player);
        showBorderBlock(extent, max.getBlockX()-1, min.getBlockZ(), BlockTypes.GOLD_BLOCK, player);
        showBorderBlock(extent, max.getBlockX(), min.getBlockZ()+1, BlockTypes.GOLD_BLOCK, player);

        showBorderBlock(extent, min.getBlockX(), center.getBlockZ(), BlockTypes.GLOWSTONE, player);
        showBorderBlock(extent, min.getBlockX(), center.getBlockZ()+1, BlockTypes.GOLD_BLOCK, player);
        showBorderBlock(extent, min.getBlockX(), center.getBlockZ()-1, BlockTypes.GOLD_BLOCK, player);

        showBorderBlock(extent, max.getBlockX(), center.getBlockZ(), BlockTypes.GLOWSTONE, player);
        showBorderBlock(extent, max.getBlockX(), center.getBlockZ()+1, BlockTypes.GOLD_BLOCK, player);
        showBorderBlock(extent, max.getBlockX(), center.getBlockZ()-1, BlockTypes.GOLD_BLOCK, player);

        showBorderBlock(extent, center.getBlockX(), min.getBlockZ(), BlockTypes.GLOWSTONE, player);
        showBorderBlock(extent, center.getBlockX()+1, min.getBlockZ(), BlockTypes.GOLD_BLOCK, player);
        showBorderBlock(extent, center.getBlockX()-1, min.getBlockZ(), BlockTypes.GOLD_BLOCK, player);

        showBorderBlock(extent, center.getBlockX(), max.getBlockZ(), BlockTypes.GLOWSTONE, player);
        showBorderBlock(extent, center.getBlockX()+1, max.getBlockZ(), BlockTypes.GOLD_BLOCK, player);
        showBorderBlock(extent, center.getBlockX()-1, max.getBlockZ(), BlockTypes.GOLD_BLOCK, player);


    }

    private void showBorderBlock(World extent, int blockX, int blockZ, BlockType type, Player player) {
        for(int i = 255; i>0; i--){
            if(extent.getBlockType(blockX, i, blockZ) != BlockTypes.AIR){
                player.sendBlockChange(blockX, i, blockZ, type.getDefaultState());
                return;
            }
        }
    }

    public boolean isEntryBlocked() {
        return entryBlocked;
    }

    public void changeEntry() {
        this.entryBlocked = !this.entryBlocked;
        WorldPlotManager.getInstance().save(this);
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public long getDbId() {
        return dbId;
    }

    public void setEntryBlocked(boolean entryBlocked) {
        this.entryBlocked = entryBlocked;
    }

    public void setType(PlotType type) {
        this.type = type;
        WorldPlotManager.getInstance().save(this);
    }

    public void setOwner(UUID owner) {
        this.owner = owner;
        WorldPlotManager.getInstance().save(this);
    }

    public enum PlotType{
        DEFAULT, PREMIUM
    }



}
