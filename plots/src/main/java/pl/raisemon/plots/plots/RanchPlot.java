package pl.raisemon.plots.plots;

import pl.raisemon.api.utils.DatabaseUtil;
import pl.raisemon.plots.Region;
import pl.raisemon.plots.managers.WorldPlotManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class RanchPlot {
    private final UUID owner;
    private final Region region;
    private long dbId;
    private List<UUID> members = new ArrayList<>();

    public RanchPlot(UUID owner, Region region) {

        this.owner = owner;
        this.region = region;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public long getDbId() {
        return dbId;
    }

    public Region getRegion() {
        return region;
    }
    public Optional<UUID> getOwner() {
        return Optional.ofNullable(owner);
    }

    public List<UUID> getMembers() {
        return members;
    }

    public boolean hasMember(UUID o) {
        return members.contains(o);
    }

    public boolean addMember(UUID uuid) {
        if(members.contains(uuid)){
            return false;
        }
        if(uuid == owner){
            return false;
        }
        boolean b = members.add(uuid);
        DatabaseUtil.insert("INSERT INTO `" + WorldPlotManager.PLOT_MEMBERS_TABLE_NAME + "`" +
                "(`plot_id`,`uuid`) VALUES" +
                "(?, ?)", stmt->{
            try{
                stmt.setLong(1, RanchPlot.this.getDbId());
                stmt.setString(2, uuid.toString());

            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        return b;
    }

    public boolean removeMember(UUID uuid) {
        if (!members.contains(uuid)) {
            return false;
        }

        boolean b = members.remove(uuid);
        DatabaseUtil.execute("DELETE FROM `" + WorldPlotManager.PLOT_MEMBERS_TABLE_NAME + "` WHERE `plot_id`='" + this.getDbId() + "' AND `uuid`=\""+uuid.toString()+"\";");
        return b;
    }

    public boolean hasAccess(UUID uniqueId) {
        return owner != null && (owner.equals(uniqueId) || hasMember(uniqueId));
    }

}
