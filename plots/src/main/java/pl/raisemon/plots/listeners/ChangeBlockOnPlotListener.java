package pl.raisemon.plots.listeners;

import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.monster.EntityCreeper;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.hanging.ItemFrame;
import org.spongepowered.api.entity.hanging.Painting;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.entity.CollideEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.plots.PlotsPlugin;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;

import java.util.Optional;

public class ChangeBlockOnPlotListener {

    @Listener(order = Order.FIRST)
    public void blockChange(ChangeBlockEvent event, @First Player player) {
        event.getTransactions().stream().filter(blockSnapshotTransaction -> {
            Optional<Location<World>> optionalLocation = blockSnapshotTransaction.getFinal().getLocation();
            if(!optionalLocation.isPresent()){
                return false;
            }
            Location<World> worldLocation = optionalLocation.get();
            //true = delete
            if (!WorldPlotManager.getInstance().hasAccessAtLocation(worldLocation, player.getUniqueId())) {
                if (!PlotsPlugin.getInstance().getBypassPlayers().contains(player.getUniqueId())) {
                    return true;
                }
            }

            //player has access on this block
            Optional<Plot> plotOptional = WorldPlotManager.getInstance().getPlot(worldLocation, true);
            if(!plotOptional.isPresent()){
                //this block is not sponge
                return false;
            }
            //block is sponge
            if(plotOptional.get().getOwner().isPresent() && !player.getUniqueId().equals(plotOptional.get().getOwner().get())){
                //plot has owner and it is broken by another player
                return true;
            }
            //if block change is fired by explosion it must be cancelled
            return event.getCause().containsType(EntityTNTPrimed.class) || event.getCause().containsType(EntityCreeper.class);
        }).forEach(blockSnapshotTransaction -> blockSnapshotTransaction.setValid(false));
    }
    @Listener
    public void onEntityCollide(CollideEntityEvent event) {
        for(Entity entity : event.getEntities()) {
            if (entity instanceof Painting) {
                event.setCancelled(true);
                break;
            }
            if (entity instanceof ItemFrame) {
                event.setCancelled(true);
                break;
            }
        }
    }
}
