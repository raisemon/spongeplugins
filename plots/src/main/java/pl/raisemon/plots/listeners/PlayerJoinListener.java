package pl.raisemon.plots.listeners;

import pl.raisemon.plots.PlotsPlugin;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.network.ClientConnectionEvent;

public class PlayerJoinListener {
    @Listener
    public void onJoin(ClientConnectionEvent.Join event){
        PlotsPlugin.getInstance().getBypassPlayers().remove(event.getTargetEntity().getUniqueId());
        WorldPlotManager.getInstance().getPlots(event.getTargetEntity().getUniqueId())
                .filter(plot -> plot.getType() == Plot.PlotType.DEFAULT)
                .forEach(plot -> plot.setExpireTime(Plot.defaultTime()));
    }
}
