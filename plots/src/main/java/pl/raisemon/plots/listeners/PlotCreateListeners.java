package pl.raisemon.plots.listeners;

import net.foxdenstudio.sponge.foxcore.plugin.util.BoundingBox2;
import net.foxdenstudio.sponge.foxcore.plugin.util.BoundingBox3;
import net.foxdenstudio.sponge.foxguard.plugin.FGManager;
import net.foxdenstudio.sponge.foxguard.plugin.region.IRegion;
import net.foxdenstudio.sponge.foxguard.plugin.region.world.CuboidRegion;
import net.foxdenstudio.sponge.foxguard.plugin.region.world.RectangularRegion;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockState;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.Last;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.PlotsPlugin;
import pl.raisemon.plots.Region;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;

import java.util.*;
import java.util.stream.Stream;

public class PlotCreateListeners {
    private final Map<UUID, Long> premiumTimes = new HashMap<>();
    @Listener(order = Order.FIRST)
    public void interact(InteractBlockEvent.Secondary event, @Last Player player) {
        Optional<ItemStack> itemInHand = player.getItemInHand(event.getHandType());
        if(!itemInHand.isPresent() || !itemInHand.get().getType().getId().equals(PlotsPlugin.getInstance().getPremiumPlotType())){
            return;
        }
        premiumTimes.put(player.getUniqueId(), System.currentTimeMillis() + PlotsPlugin.getInstance().getTimeFromItem(itemInHand.get()) * 1000);

    }
    @Listener(order = Order.FIRST)
    public void blockPlace(ChangeBlockEvent.Place event, @Last Player player) {
        if (event.getTransactions().size()==0) {
            return;
        }
        final Transaction<BlockSnapshot> transaction = event.getTransactions().get(0);
        BlockState state = transaction.getFinal().getState();
        Plot.PlotType type = PlotsPlugin.getInstance().getPlotType(state);
        if(type == null){
            return;
        }
        if(type == Plot.PlotType.PREMIUM && !premiumTimes.containsKey(player.getUniqueId())){
            player.sendMessage(Text.of(TextColors.RED, "Nie mozna odczytac czasu dzialki premium, skontaktuj sie z naczelnym szopem programista w celu rozwiazania problemu."));
            return;
        }
        event.setCancelled(true);
        final Optional<Location<World>> location = transaction.getDefault().getLocation();
        if(!location.isPresent()){
            return;
        }
        if (!PlotsPlugin.getInstance().getWorlds().contains(location.get().getExtent().getName())) {
            MessageUtils.sendMessage(player, "Dzialki", "plots.invalidWorld");
            return;
        }

        Region region = new Region(location.get());
        Set<IRegion> regionsAtPos = FGManager.getInstance().getRegionsAtPos(location.get().getExtent(), location.get().getBlockPosition());
        boolean bb = false;
        for (IRegion rgAtPos : regionsAtPos) {
            if(rgAtPos instanceof RectangularRegion){
                BoundingBox2 boundingBox = ((RectangularRegion) rgAtPos).getBoundingBox();
                bb = region.intersects(boundingBox.a.getX(), boundingBox.a.getY(), boundingBox.b.getX(), boundingBox.b.getY());
            }
            if(rgAtPos instanceof CuboidRegion){
                BoundingBox3 boundingBox = ((CuboidRegion) rgAtPos).getBoundingBox();
                bb = region.intersects(boundingBox.a.getX(), boundingBox.a.getZ(), boundingBox.b.getX(), boundingBox.b.getZ());
            }
            if (bb) {
                break;
            }
        }
        if(bb){
            MessageUtils.sendMessage(player, "Dzialki", "plots.nearRegion");
            return;
        }

        if(WorldPlotManager.getInstance().getPlots(player.getUniqueId()).anyMatch(plot -> plot.getType() == Plot.PlotType.DEFAULT) && type == Plot.PlotType.DEFAULT){
            MessageUtils.sendMessage(player, "Dzialki", "plots.haveDefaultPlot");
            return;
        }
        Stream<Plot> plots = WorldPlotManager.getInstance().getPlots(player.getUniqueId());
        int max = PlotsPlugin.getInstance().getMaxPlayerPremiumPlots(player);
        if (plots.filter(plot -> plot.getType() == Plot.PlotType.PREMIUM).count() >= max && type == Plot.PlotType.PREMIUM) {
            MessageUtils.sendMessage(player, "Dzialki", "plots.tooManyPremiumPlots");
            return;
        }


        if (WorldPlotManager.getInstance().intersectsAnyRegion(region)) {
            MessageUtils.sendMessage(player, "Dzialki", "plots.nearAnotherPlot");
            return;
        }
        event.setCancelled(false);
        Plot plot = null;
        if (type == Plot.PlotType.DEFAULT) {
            plot = new Plot(player, region, Plot.PlotType.DEFAULT);
        } else {
            plot = new Plot(player, region, Plot.PlotType.PREMIUM, premiumTimes.get(player.getUniqueId()));
            premiumTimes.remove(player.getUniqueId());
        }

        WorldPlotManager.getInstance().register(plot);
        MessageUtils.sendMessage(player, "Dzialki", "plots.plotCreated");
    }
}
