package pl.raisemon.plots.listeners;

import pl.raisemon.plots.Utils;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.service.user.UserStorageService;
import org.spongepowered.api.text.serializer.TextSerializers;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;
import java.util.stream.Collectors;

public class PlotInfoListener {
    @Listener
    public void handle(InteractBlockEvent.Secondary.MainHand event, @First Player player) throws Exception {
        Optional<Location<World>> location = event.getTargetBlock().getLocation();
        if (!location.isPresent()) {
            return;
        }
        Optional<Plot> plot = WorldPlotManager.getInstance().getPlot(location.get(), true);
        if(!plot.isPresent()){
            return;
        }
        Plot plotBySponge = plot.get();

        if((!plotBySponge.hasAccess(player.getUniqueId()) && !player.hasPermission("pl.raisemon.plotinfo"))){
            return;
        }
        event.setCancelled(true);
        player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&cDzialka " + (plotBySponge.getType() == Plot.PlotType.PREMIUM ? "premium" : "zwykla")));
        if (plotBySponge.getType() == Plot.PlotType.PREMIUM)
            player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&3Wygasa za: &b" + Utils.durationToString((int) ((plotBySponge.getExpireTime() - System.currentTimeMillis()) / 1000))));
        UserStorageService userStorageService = Sponge.getServiceManager().provide(UserStorageService.class).get();
        player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&3Wlasciciel: &b" + (plotBySponge.getOwner().isPresent() ? userStorageService.get(plotBySponge.getOwner().get()).get().getName() : "Brak")));
        String s = plotBySponge.getMembers().stream().map(uuid -> userStorageService.get(uuid).get().getName()).collect(Collectors.joining(", "));
        player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&3Czlonkowie: &b" + (s.length() > 0 ? s : "brak")));
        player.sendMessage(TextSerializers.FORMATTING_CODE.deserialize("&3Wejscie: &b" + (plotBySponge.isEntryBlocked() ? "Zablokowane" : "Odblokowane")));


    }
}
