package pl.raisemon.plots.listeners;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.EntityTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.enchantment.EnchantmentTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.api.utils.MessageUtils;
import pl.raisemon.plots.PlotsPlugin;
import pl.raisemon.plots.Utils;
import pl.raisemon.plots.managers.WorldPlotManager;
import pl.raisemon.plots.plots.Plot;

import java.util.Collections;
import java.util.Optional;

public class PlotBreakListeners {
    @Listener(order = Order.FIRST)
    public void onBreakPlot(ChangeBlockEvent.Break event, @First Player player) {
        for (Transaction<BlockSnapshot> blockSnapshotTransaction : event.getTransactions()) {

            if(!blockSnapshotTransaction.getOriginal().getState().getType().getName().equals(PlotsPlugin.getInstance().getPlotType()) &&
                !blockSnapshotTransaction.getOriginal().getState().getType().getName().equals(PlotsPlugin.getInstance().getPremiumPlotType())){
                continue;
            }

            Optional<Location<World>> locationOptional = blockSnapshotTransaction.getOriginal().getLocation();
            if(!locationOptional.isPresent()){
                continue;
            }
            Location<World> spongeLocation = locationOptional.get();

            Optional<Plot> plotBySponge = WorldPlotManager.getInstance().getPlot(spongeLocation, true);
            if(!plotBySponge.isPresent()){
                continue;
            }
            final Optional<ItemStack> itemInHand = player.getItemInHand(HandTypes.MAIN_HAND);
            if(plotBySponge.get().getType().equals(Plot.PlotType.PREMIUM) && itemInHand.isPresent()
                    && itemInHand.get().get(Keys.ITEM_ENCHANTMENTS).isPresent() &&
                    itemInHand.get().get(Keys.ITEM_ENCHANTMENTS).get().stream().anyMatch(e -> e.getType().equals(EnchantmentTypes.SILK_TOUCH))){
                MessageUtils.sendMessage(player, "Dzialki", "plots.silkTouchPlotBreak");
                blockSnapshotTransaction.setValid(false);
                continue;
            }
            if (!plotBySponge.get().getOwner().filter(uuid -> uuid.equals(player.getUniqueId())).isPresent()) {
                MessageUtils.sendMessage(player, "Dzialki", "plots.notOwner");
                blockSnapshotTransaction.setValid(false);
                continue;
            }
            WorldPlotManager.getInstance().remove(plotBySponge.get(), false);
            MessageUtils.sendMessage(player, "Dzialki", "plots.plotRemoved");

            if(plotBySponge.get().getType().equals(Plot.PlotType.PREMIUM) && !plotBySponge.get().isExpired()){
                final ItemStack.Builder builder = ItemStack.builder();
                final Optional<ItemType> type = Sponge.getRegistry().getType(ItemType.class, PlotsPlugin.getInstance().getPremiumPlotType());
                if(!type.isPresent()){
                    return;
                }
                builder.itemType(type.get());
                builder.add(Keys.ITEM_LORE, Collections.singletonList(Text.of(TextColors.GOLD, "Pozostalo: " + Utils.durationToString((plotBySponge.get().getExpireTime() - System.currentTimeMillis())/1000))));


                Entity entity = spongeLocation.getExtent().createEntity(EntityTypes.ITEM, spongeLocation.getBlockPosition());

                entity.offer(Keys.REPRESENTED_ITEM, builder.build().createSnapshot());
                spongeLocation.getExtent().spawnEntity(entity);

            }
        }
    }
}
