package pl.raisemon.plots.listeners;

import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.hanging.ItemFrame;
import org.spongepowered.api.entity.hanging.Painting;
import org.spongepowered.api.entity.living.ArmorStand;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.vehicle.Boat;
import org.spongepowered.api.entity.vehicle.minecart.Minecart;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.entity.SpawnEntityEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.chat.ChatTypes;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;
import pl.raisemon.plots.PlotsPlugin;

public class RanchListener {
    @Listener
    public void onBlockModify(ChangeBlockEvent event, @First Player player) {
        if (event instanceof ChangeBlockEvent.Break) {
            for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
                if (transaction.getOriginal().getState().getType().getId().equalsIgnoreCase("pixelmon:ranch")) {
                    event.setCancelled(true);
                    player.sendMessage(Text.of(TextColors.RED, "Nie mozna niszczyc ranchblocka!"));
                    return;
                }
            }
        }
        if (event instanceof ChangeBlockEvent.Place) {
            for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
                if (player.getItemInHand(HandTypes.MAIN_HAND).map(i -> i.getType().getId().equalsIgnoreCase("pixelmon:ranch")).orElse(false)) {
                    event.setCancelled(true);
                    if (PlotsPlugin.getInstance().isRanchRegion(transaction.getOriginal().getLocation().get())) {
                        if (transaction.getOriginal().getLocation().get().getBlockRelative(Direction.DOWN).getBlockType().equals(BlockTypes.WOOL)) {
                            if (PlotsPlugin.getInstance().canCreate(player, transaction.getOriginal().getLocation().get())) {
                                event.setCancelled(false);
                                PlotsPlugin.getInstance().createRanchPlot(transaction.getOriginal().getLocation().get(), player);
                                player.sendMessage(Text.of(TextColors.GREEN, "Utworzyles dzialke!"));
                                return;
                            }
                        }
                    }
                }
            }
        }
        for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
            if (transaction.getOriginal().getLocation().isPresent()) {
                if (PlotsPlugin.getInstance().isRanchRegion(transaction.getOriginal().getLocation().get())) {
                    event.setCancelled(true);
                    if (PlotsPlugin.getInstance().haveRanchPermission(player, transaction.getOriginal().getLocation().get())) {
                        event.setCancelled(false);
                    }
                }
            }
        }
    }

    @Listener
    public void interact(InteractBlockEvent event, @First Player player) {
        if (!event.getTargetBlock().getLocation().isPresent()) {
            return;
        }
        if (event instanceof InteractBlockEvent.Secondary.MainHand) {
            if (player.getItemInHand(HandTypes.MAIN_HAND).map(i -> i.getType().getId().equalsIgnoreCase("pixelmon:ranch")).orElse(false)) {
                if (event.getTargetBlock().getState().getType().equals(BlockTypes.WOOL)) {
                    return;
                }
            }
        }
        Location<World> worldLocation = event.getTargetBlock().getLocation().get();
        if (PlotsPlugin.getInstance().isRanchRegion(worldLocation)) {
            if (player.hasPermission("pl.raisemon.admin")) {
                player.sendMessages(ChatTypes.ACTION_BAR, Text.of(TextColors.RED, "Modyfikujesz region rancha"));
                return;
            }
            event.setCancelled(true);
            if (PlotsPlugin.getInstance().haveRanchPermission(player, worldLocation)) {
                event.setCancelled(false);
            }
        }
    }

    @Listener
    public void interact(InteractEntityEvent event, @First Player player) {
        Location<World> worldLocation = event.getTargetEntity().getLocation();
        if (PlotsPlugin.getInstance().isRanchRegion(worldLocation)) {
            event.setCancelled(true);
            if (PlotsPlugin.getInstance().haveRanchPermission(player, worldLocation)) {
                event.setCancelled(false);
            }
        }
    }

    @Listener
    public void interact(SpawnEntityEvent event, @First Player player) {
        if (event.getEntities().size() == 0) {
            return;
        }
        for (Entity entity : event.getEntities()) {
            if (entity instanceof Boat ||
                    entity instanceof Minecart ||
                    entity instanceof ArmorStand ||
                    entity instanceof Painting ||
                    entity instanceof ItemFrame) {
                Location<World> worldLocation = event.getEntities().get(0).getLocation();
                if (PlotsPlugin.getInstance().isRanchRegion(worldLocation)) {
                    event.setCancelled(true);
                    if (PlotsPlugin.getInstance().haveRanchPermission(player, worldLocation)) {
                        event.setCancelled(false);
                    }
                }
            }
        }
    }

    @Listener
    public void onCommand(SendCommandEvent event, @First Player player) {
        if (!player.getWorld().getName().equalsIgnoreCase("world")) {
            if (PlotsPlugin.getInstance().getExtraCommandsBlocked().contains(event.getCommand())) {
                event.setCancelled(true);
                if (PlotsPlugin.getInstance().isRanchRegion(player.getLocation())) {
                    event.setCancelled(false);
                }
            }
        }
    }
}
